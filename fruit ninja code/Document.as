﻿package  
{	
	import flash.display.*;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Matrix;
	import flash.utils.Timer;
	import flash.ui.Mouse;
	import flash.events.TimerEvent;
	
	import Box2D.Dynamics.*;
	import Box2D.Dynamics.Joints.*;
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*; 
	import gs.*;
	import gs.easing.*; 
	import BillNinja;
		
	public class Document extends MovieClip
	{	
		//private var ninjaGame:BillNinja;
		
		public function Document() 
		{
			//stage.addEventListener(MouseEvent.MOUSE_DOWN, mDown);
			//addEventListener(Event.ENTER_FRAME, update);
			createNinjaGame(0,0);
			//createNinjaWorld();
		}
		
		private function createNinjaGame(xPos:Number, yPos:Number)
		{
			var ninjaGame:BillNinja = new BillNinja(xPos, yPos, this);//xPos, yPos, this
			addChild(ninjaGame);
		}		
	}
}
