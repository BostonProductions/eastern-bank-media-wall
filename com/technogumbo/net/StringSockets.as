package com.technogumbo.net
{
	import flash.errors.IOError;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.Socket;
	import flash.utils.Timer;
	
	public class StringSockets extends EventDispatcher
	{
		public static var CONNECTED:String = "com.technogumbo.net.StringSockets_CONNECTED";
		public static var DISCONNECTED:String = "com.technogumbo.net.StringSockets_DISCONNECTED";
		public static var STRINGDATA:String = "com.technogumbo.net.StringSockets_STRINGDATA";
		public static var ERROR:String = "com.technogumbo.net.StringSockets_ERROR";
		
		public var _theSocket:Socket;
		protected var _targetServer:String;
		protected var _targetPort:int;
		protected var _messageDelimeter:String;
		
		protected var _volatileIncomingSocketData:String = "";
		
		protected var _reconnectTimer:Timer;
		
		public function StringSockets(_SocketURL:String ="", _SocketPort:int = 0, _MessageDelimeter:String="/r/n")
		{
			_targetServer = _SocketURL;
			_targetPort = _SocketPort;
			_messageDelimeter = _MessageDelimeter;
		}
		public function getConnectedStatus():Boolean {
			var socketConnected:Boolean = false;
			if(_theSocket != null) {
				socketConnected = _theSocket.connected;
			}
			return socketConnected;
		}
		public function getSocketURL():String {
			return _targetServer;
		}
		public function getSocketPort():int {
			return _targetPort;
		}
		/**
		 * When using this function with a socket server class, this function allows you to pass in
		 * an already created socket connection.
		 * @param _OptionalActiveSocket 
		 */
		public function makeConnection(_OptionalActiveSocket:Socket = null):void {
			createSocket(_OptionalActiveSocket);
		}
		
		public function disconnect():void {
			destroySocket();
			destroyReconnectTimer();
		}
		
		public function sendString(_InputString:String="", _AutoDelimeter:Boolean = true):void {
			if (_theSocket) {
				if (_theSocket.connected) {
					if(_AutoDelimeter) {
						_theSocket.writeUTFBytes(_InputString + _messageDelimeter);
					} else {
						_theSocket.writeUTFBytes(_InputString);
					}
					
					_theSocket.flush();
				}
			}
		}
		
		public function destroyInternals():void {
			destroyReconnectTimer();
			destroySocket();
		}
		
		/**
		 * The socket is for when were working with an AIR2 socket server
		 * as it will recieve already open and active Sockets. We just need to
		 * start listening to them.
		 */
		protected function createSocket(_OptionalActiveSocket:Socket = null):void {
			if (_theSocket == null) {
				if(_OptionalActiveSocket == null) {
					_theSocket = new Socket();
				} else {
					_theSocket = _OptionalActiveSocket;
				}
				_theSocket.addEventListener(Event.CONNECT, handleConnection, false, 0, true);
				_theSocket.addEventListener(Event.CLOSE, handleDisconnect, false, 0, true);
				_theSocket.addEventListener(IOErrorEvent.IO_ERROR, handleSocketIOError, false, 0, true);
				_theSocket.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleSocketError, false, 0, true);
				_theSocket.addEventListener(ProgressEvent.SOCKET_DATA, handleSocketData, false, 0, true);
				if(_OptionalActiveSocket == null) {
					_theSocket.connect(_targetServer, _targetPort);
				}
			} else {
				destroySocket(_OptionalActiveSocket, true);
			}
		}
		
		protected function destroySocket(_OptionalActiveSocket:Socket = null, _OptionalCallback:Boolean = false):void {
			if (_theSocket != null) {
				try {
				 _theSocket.close();
				} catch (e:Error) {
					// Who cares
				}
				
				_theSocket.removeEventListener(Event.CONNECT, handleConnection);
				_theSocket.removeEventListener(Event.CLOSE, handleDisconnect);
				_theSocket.removeEventListener(IOErrorEvent.IO_ERROR, handleSocketIOError);
				_theSocket.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handleSocketError);
				_theSocket.removeEventListener(ProgressEvent.SOCKET_DATA, handleSocketData);
				
				_theSocket = null;
				
				if (_OptionalCallback == true) {
					createSocket(_OptionalActiveSocket);
				}
			}
		}
		
		/* Event Handlers */
		
		protected function handleConnection(e:Event):void {
			dispatchEvent( new Event(StringSockets.CONNECTED, false, true));
		}
		protected function handleDisconnect(e:Event):void {
			dispatchEvent( new Event(StringSockets.DISCONNECTED, false, true));
		}
		protected function handleSocketIOError(e:IOErrorEvent):void {
			// throw(e);
			// No no, no throwing.  What we want is to silently try and
			// reconnect over and over again
			//trace("com.technogumbo.net.StringSockets Socket Error: " + e);
			dispatchEvent( new DataEvent(StringSockets.ERROR, false, true, e.text ) );
			
			createReconnectTimer();
		}
		protected function handleSocketError(e:SecurityErrorEvent):void {
			// throw(e);
			// No no, no throwing.  What we want is to silently try and
			// reconnect over and over again
			//trace("com.technogumbo.net.StringSockets Socket Error: " + e);
			dispatchEvent( new DataEvent(StringSockets.ERROR, false, true, e.text ) );
			
			createReconnectTimer();
		}
		/**
		 * OLD VERSION - THIS WOULD BREAK ON MORE THAN ONE MESSAGE--oops
		protected function handleSocketData(e:ProgressEvent):void {
			
			if (_theSocket != null) {
				var input:String = _theSocket.readUTFBytes(_theSocket.bytesAvailable);
				trace("StringSockets - Raw Input: " + input);
				// Search for the delimeter in the string
				var delimIndex:int = input.search(_messageDelimeter);
				if (delimIndex != -1) {
					var relevantData:String = input.substring(0, delimIndex);
					var leftoverData:String = "";
					if ( (delimIndex + _messageDelimeter.length) < input.length ) {
						leftoverData = input.substring(delimIndex + _messageDelimeter.length, input.length);
					}
					
					var FinalMessage:String = _volatileIncomingSocketData.concat(relevantData);
					// Dispatch
					dispatchEvent(new DataEvent(StringSockets.STRINGDATA, false, true, FinalMessage));
					// Reset the incoming socket string
					_volatileIncomingSocketData = leftoverData;
				} else {
					_volatileIncomingSocketData = _volatileIncomingSocketData.concat(input);
				}
			}
		}
		 */
		protected function handleSocketData(e:ProgressEvent):void {
			
			if (_theSocket != null) {
				var input:String = _theSocket.readUTFBytes(_theSocket.bytesAvailable);
				
				if(_volatileIncomingSocketData != "") {
					input = _volatileIncomingSocketData + input;
					_volatileIncomingSocketData = "";
				}
				
				//trace("StringSockets - Raw Input: " + input);
				// Search for the delimeter in the string
				var delimIndex:int = input.search(_messageDelimeter);
				if (delimIndex != -1) {
					
					var dataArr:Array = input.split(_messageDelimeter);
					for(var i:int = 0; i < dataArr.length; i++) {
						// LAST ELEMENT IS SPECIAL
						if( i == dataArr.length - 1 ) {
							// WAS THIS A COMPLETE MESSAGE?
							if(dataArr[i] == "") {
								// YES - THIS IS THE END - QUIT
								break;
							}
							
							var subDelimCount:Array = dataArr[i].split('"');
							if(subDelimCount.length == 3) {
								// This is a final full message
								//trace( "FINAL FULL MSG Position: " + i + " Data: " + dataArr[i] );
								dispatchEvent(new DataEvent(StringSockets.STRINGDATA, false, true, dataArr[i]));
							} else {
								// Add whats left for the next time the socket gets data
								_volatileIncomingSocketData = _volatileIncomingSocketData + dataArr[i];
							}
							//trace("Number of items on end string: " + subDelimCount.length + " finalString: " + dataArr[i]);
						} else {
							// NORMAL FULL MESSAGE NO NEED TO WORRY
							//trace( "Position: " + i + " Data: " + dataArr[i] );
							dispatchEvent(new DataEvent(StringSockets.STRINGDATA, false, true, dataArr[i]));
						}
					}
					
					// Reset the incoming socket string
					//_volatileIncomingSocketData = leftoverData;
				} else {
					_volatileIncomingSocketData = input;
				}
				
			}
		}
		
		private function createReconnectTimer():void {
			if(_reconnectTimer == null) {
				_reconnectTimer = new Timer(3000);
				_reconnectTimer.addEventListener(TimerEvent.TIMER, handleReconnectTimerUp, false, 0, true);
				_reconnectTimer.start();
			} else {
				destroyReconnectTimer(true);
			}
		}
		private function destroyReconnectTimer(_OptionalCallback:Boolean = false):void {
			if(_reconnectTimer != null) {
				_reconnectTimer.stop();
				_reconnectTimer.removeEventListener(TimerEvent.TIMER, handleReconnectTimerUp);
				_reconnectTimer = null;
				if(_OptionalCallback == true) {
					createReconnectTimer();
				}
			}
		}
		private function handleReconnectTimerUp(e:TimerEvent):void {
			destroyReconnectTimer();
			
			destroySocket(null,true);
		}
	}
}