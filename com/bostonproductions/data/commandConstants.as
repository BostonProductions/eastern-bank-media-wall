﻿package com.bostonproductions.data
{
	public class commandConstants
	{
		public static const INIT_COMPLETE:String = "INIT_COMPLETE";
		
		public static const PROMOTIONS_PHOTOS:String = "GET_PROMOTIONS_PHOTOS";
		public static const PROMOTIONS_TEXT:String = "GET_PROMOTIONS_TEXT";
		public static const PROMOTIONS_VIDEOS:String = "GET_PROMOTIONS_VIDEOS";
		public static const PROMOTIONS_ALL:String = "GET_PROMOTIONS_ALL";
		
		public static const PROMOTIONS_EVENT:String = "EVENT_PROMOTIONS_ALL";
		
		public static const MEDIA_WALL_HOME:String = "GET_MEDIAWALL_HOME";
		public static const MEDIA_WALL_BUSINESS:String = "GET_MEDIAWALL_BUSINESS";
		public static const MEDIA_WALL_DREAMS:String = "GET_MEDIAWALL_DREAMS";
		public static const MEDIA_WALL_LIFE:String = "GET_MEDIAWALL_LIFE";
		public static const MEDIA_WALL_GAMES:String = "GET_MEDIAWALLGAME_ALL";
		public static const MEDIA_WALL_ALL:String = "GET_MEDIAWALL_ALL";
		
		public static const MEDIA_WALL_EVENT:String = "EVENT_MEDIAWALL_ALL";
		
		public static const MEDIA_WALL_HOME_EVENT:String = "EVENT_MEDIAWALL_HOME";
		public static const MEDIA_WALL_BUSINESS_EVENT:String = "EVENT_MEDIAWALL_BUSINESS";
		public static const MEDIA_WALL_DREAMS_EVENT:String = "EVENT_MEDIAWALL_DREAMS";
		public static const MEDIA_WALL_LIFE_EVENT:String = "EVENT_MEDIAWALL_LIFE";
		public static const MEDIA_WALL_GAMES_EVENT:String = "EVENT_MEDIAWALLGAME_ALL";
		
		public static const VESTIBULE_ALL:String = "GET_VESTIBULE_ALL";
		public static const VESTIBULE_PHOTOS:String = "GET_VESTIBULE_PHOTOS";
		public static const VESTIBULE_TEXT:String = "GET_VESTIBULE_TEXT";
		
		public static const VESTIBULE_EVENT:String = "EVENT_VESTIBULE_ALL";
		
		public static const COMMUNITY_ALL:String = "GET_COMMUNITY_ALL";
		public static const COMMUNITY_PHOTOS:String = "GET_COMMUNITY_PHOTOS";
		public static const COMMUNITY_VIDEOS:String = "GET_COMMUNITY_VIDEOS";
		public static const COMMUNITY_TEXT:String = "GET_COMMUNITY_TEXT";
		
		public static const COMMUNITY_EVENT:String = "EVENT_COMMUNITY_ALL";
	}
	
}