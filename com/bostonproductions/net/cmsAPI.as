package com.bostonproductions.net
{
	import com.bostonproductions.events.CMSEvent;
	import flash.errors.IOError;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.net.Socket;
	import flash.utils.Timer;
	
	import com.bostonproductions.data.commandConstants;
	
	public class cmsAPI extends EventDispatcher
	{
		public static const INITCOMPLETE:String = "com.bostonproductions.net.cmsAPI.INITCOMPLETE";
		public static const CMS_DATA:String = "com.bostonproductions.net.cmsAPI.CMS_DATA";
		public static const NEW_CMS_DATA:String = "com.bostonproductions.net.cmsAPI.NEW_CMS_DATA";
		
		private var _initialized:Boolean = false;
		
		private var _link:cmsLink;
		
		public function cmsAPI()
		{

		}
		
		/**
		 * Call this to initiate the connection to the
		 * caching application after you have added
		 * the relevant event listeners.
		 */
		public function init():void {
			createCMSLink();
		}
		/**
		 * During execution this class should be active
		 * at all times, but if for some reason you want
		 * to ensure it is completley gotten rid of,
		 * call this function right before nulling the
		 * instance value of the class. It will ensure
		 * all internal assets are cleaned up.
		 */
		public function destroyInternals():void {
			destroyCMSLink();
			
		}
		/**
		 * Will retrieve the most recent cached
		 * promotions photos xml. Be aware that events
		 * may happen during execution with updated
		 * results as this function.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval
		 * of the data.
		 */
		public function get_promotions_photos():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			
			_link.sendSocketString(commandConstants.PROMOTIONS_PHOTOS);
		}
		
		/**
		 * Will retrieve the most recent cached
		 * promotions videos xml. Be aware that events
		 * may happen during execution with updated
		 * results as this function.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval
		 * of the data.
		 */
		public function get_promotions_videos():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.PROMOTIONS_VIDEOS);
		}
		
		/**
		 * Will retrieve the most recent cached
		 * promotions text xml. Be aware that events
		 * may happen during execution with updated
		 * results as this function.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval
		 * of the data.
		 */
		public function get_promotions_text():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.PROMOTIONS_TEXT);
		}
		
		/**
		 * Will retrieve the most recent cached
		 * xml for all promotions content. This returns the
		 * results from get_promotions_text, get_promotions_videos, and
		 * get_promotions_photos concatenated as a single xml document.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval
		 * of the data.
		 */
		public function get_promotions_all():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.PROMOTIONS_ALL);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the media wall life section.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_media_wall_life():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			
			_link.sendSocketString(commandConstants.MEDIA_WALL_LIFE);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the media wall business section.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_media_wall_business():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.MEDIA_WALL_BUSINESS);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the media wall dreams section.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_media_wall_dreams():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.MEDIA_WALL_DREAMS);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the media wall home section.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_media_wall_home():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.MEDIA_WALL_HOME);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the media wall games section. Be careful as the games
		 * have a very different xml structure than the rest of the
		 * media wall home.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_media_wall_games():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.MEDIA_WALL_GAMES);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the media wall. This combines all the other data together in one
		 * large xml document. Be careful as the game nodes will have a slightly
		 * different structure.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_media_wall_all():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.MEDIA_WALL_ALL);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the vestibule photos.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_vestibule_photos():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.VESTIBULE_PHOTOS);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the vestibule text.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_vestibule_text():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.VESTIBULE_TEXT);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the vestibule.  Combines all data from photos and text
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_vestibule_all():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.VESTIBULE_ALL);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the community wall photos.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_community_photos():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.COMMUNITY_PHOTOS);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the community wall videos.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_community_videos():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.COMMUNITY_VIDEOS);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the community wall text.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_community_text():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.COMMUNITY_TEXT);
		}
		
		/**
		 * Will retrieve the most recent cached xml
		 * for the community board.  This combines all the photo, video, and text data
		 * into one xml return.
		 * 
		 * The CMS_DATA event will be dispatched on retrieval of
		 * the data.
		 */
		public function get_community_all():void {
			if (! _initialized ) {
				// Throw an error
				throw( new Error("You must wait for an initial connection to take place indicated by the INITCOMPLETE event before calling API methods."));
				// End
				return;
			}
			_link.sendSocketString(commandConstants.COMMUNITY_ALL);
		}
		
		private function createCMSLink():void {
			if (_link == null) {
				_link = new cmsLink();
				_link.addEventListener(cmsLink.DATA, handleCMSData, false, 0, true);
				_link.init();
			} else {
				destroyCMSLink( true );
			}
		}
		
		private function destroyCMSLink(optionalCallback:Boolean = false):void {
			if (_link != null) {
				_link.removeEventListener(cmsLink.DATA, handleCMSData);
				_link.destroyInternals();
				_link = null;
				
				if (optionalCallback == true) createCMSLink();
			}
		}
		
		private function handleCMSData(e:CMSEvent):void {
			// We care about the INITIALIZATION EVENT. We also care about
			// commands that are prefixed with "EVENT" because we want to
			// send those as different event types
			
			switch(e.Data.command) {
				case commandConstants.INIT_COMPLETE:
					_initialized = true;
					dispatchEvent(new Event(cmsAPI.INITCOMPLETE, false, true));
				break;
				case commandConstants.PROMOTIONS_EVENT:
				case commandConstants.MEDIA_WALL_EVENT:
				case commandConstants.MEDIA_WALL_GAMES_EVENT:
				case commandConstants.VESTIBULE_EVENT:
				case commandConstants.COMMUNITY_EVENT:
					dispatchEvent(new CMSEvent(cmsAPI.NEW_CMS_DATA, false, true, e.Data));
				break;
				default:
					dispatchEvent(new CMSEvent(cmsAPI.CMS_DATA, false, true, e.Data));
				break;
			}
		}
	}
}