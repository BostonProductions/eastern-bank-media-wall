﻿package code
{

	import flash.display.MovieClip;

	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;

	import flash.events.TimerEvent;
	import flash.utils.Timer;

	import com.greensock.*;
	import com.greensock.easing.*;

	import com.treefortress.sound.SoundAS;
	import com.treefortress.sound.SoundInstance;
	import com.treefortress.sound.SoundManager;


	public class LoGm extends MovieClip
	{

		public var playerNumber:Number;

		private var s:String = "";
		private var sa:String = "";
		private var sy:String = "";
		private var sm:String = "";

		public var score:Number = 0;
		public var monthlySavings:Number = 0;
		public var gArray:Array = new Array();
		public var gAYMArray:Array;

		public var whichG:Number = 0;
		public var whichField:Number = 0;

		public var playerGo:Boolean = false;

		public var ct:Number = 0;

		private var s1:String = "";
		private var s2:String = "";
		private var s3:String = "";
		private var s4:String = "";

		//public var countdownTimer:Timer;
		public var time:Number = 10;
		public var gameTimer:Timer;

		public var timeoutTimer:Timer;
		public var timeout:Number = 30;

		public var owner:SavingsGame;

		public var saGm:SaGm;
		public var drGm:DrGm;

		public var icon1:Number = 0;
		public var icon2:Number = 0;
		public var icon3:Number = 0;
		public var icon4:Number = 0;
		public var icon5:Number = 0;
		public var icon6:Number = 0;
		public var icon7:Number = 0;

		public var whichi:Number = 0;

		public var iconArray:Array;

		public static var SBC:String = "sbc";
		public static var SCA:String = "sca";
		public static var SCPI:String = "scpi";
		public static var SCO:String = "sco";
		public static var SG:String = "sg";
		public static var SGG:String = "sgg";
		public static var SH:String = "sh";
		public static var SMM:String = "smm";
		public static var SR:String = "sr";
		public static var ST:String = "st";
		public static var SV:String = "sv";
		public static var SWA:String = "swa";
		public static var SWE:String = "swe";
		public static var SGM:String = "sgm";
		public static var MR:String = "mr";
		public static var SSV:String = "ssv";

		public var howManyDollars:Number = 0;

		public var exitBtn:CloseBlue;

		public var howMuchMoney:Number = 0;

		public var timeoutPopup:StillThere;

		public var numberSpinner:TenMillionSpinner;
		public var l:Number = 0;

		public function LoGm(myOwner:SavingsGame)
		{
			owner = myOwner;

			if (owner.whatLang == "english")
			{
				l = 0;
			}
			else
			{
				l = 1;
			}
			timeout = owner.owner.loadXML.saverTimeout;

			timeoutTimer = new Timer(timeout,1);
			timeoutTimer.start();
			timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, allTimedOut);
			addEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimer);

			SoundAS.loadSound(owner.owner.loadXML.saverButtonClick, SBC);
			SoundAS.loadSound(owner.owner.loadXML.saverCar, SCA);
			SoundAS.loadSound(owner.owner.loadXML.saverCloudPopIn, SCPI);
			SoundAS.loadSound(owner.owner.loadXML.saverCollege, SCO);
			SoundAS.loadSound(owner.owner.loadXML.saverGambler, SG);
			SoundAS.loadSound(owner.owner.loadXML.saverGoGetter, SGG);
			SoundAS.loadSound(owner.owner.loadXML.saverHome, SH);
			SoundAS.loadSound(owner.owner.loadXML.saverMoneyMeter, SMM);
			SoundAS.loadSound(owner.owner.loadXML.saverRetirement, SR);
			SoundAS.loadSound(owner.owner.loadXML.saverTortoise, ST);
			SoundAS.loadSound(owner.owner.loadXML.saverVacation, SV);
			SoundAS.loadSound(owner.owner.loadXML.saverWardrobe, SWA);
			SoundAS.loadSound(owner.owner.loadXML.saverWedding, SWE);
			SoundAS.loadSound(owner.owner.loadXML.saverQuizMusic, SGM);
			SoundAS.loadSound(owner.owner.loadXML.saverMoneyRolling, MR);
			SoundAS.loadSound(owner.owner.loadXML.saverScaleShortVersion, SSV);

			iconArray = new Array(icon1,icon2,icon3,icon4,icon5,icon6,icon7);

			exitBtn = new CloseBlue();
			addChild(exitBtn);
			setChildIndex(exitBtn, numChildren - 1);
			exitBtn.x = 1080 - exitBtn.width;
			exitBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, exitGame);



			numberSpinner = new TenMillionSpinner();
			//numberSpinner.x = 1080 - numberSpinner.width;
			//numberSpinner.y = 1920 - numberSpinner.height;
			//addChild(numberSpinner);
			numberSpinner.x = 650;
			numberSpinner.y = 1822;
			numberSpinner.visible = false;
			//numberSpinner.init(score);
			//numberSpinner.stopAnimation();
		}

		public function resetTimer(e:TuioTouchEvent = null):void
		{
			//trace("resetTimer");
			if (timeoutTimer != null)
			{
				timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, allTimedOut);
				timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, reallyTimedOut);
				removeEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimer);
				timeoutTimer.stop();
			}
			timeoutTimer = new Timer(timeout,1);
			//trace("seconds for timeout = " + timeout/1000);
			timeoutTimer.start();
			timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, allTimedOut);
			addEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimer);
		}

		public function allTimedOut(e:TimerEvent):void
		{
			//trace("allTimedOut");
			//exitGame();
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, allTimedOut);
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, reallyTimedOut);
			removeEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimer);
			timeoutTimer.stop();

			timeoutTimer = new Timer(timeout/2, 1);
			timeoutTimer.start();
			timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, reallyTimedOut);

			timeoutPopup = new StillThere();
			addChild(timeoutPopup);
			timeoutPopup.yesBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, stillPlaying);
			timeoutPopup.quitBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, exitGame);
			timeoutPopup.quitBtn.t.text = owner.owner.loadXML.dreamQuit[l];;
			timeoutPopup.yesBtn.t.text = owner.owner.loadXML.dreamYes[l];;
			timeoutPopup.t.text = owner.owner.loadXML.dreamStillThere[l];;
		}

		public function reallyTimedOut(e:TimerEvent):void
		{
			//trace("reallyTimedOut");
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, allTimedOut);
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, reallyTimedOut);
			removeEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimer);
			timeoutTimer.stop();

			exitGame();
		}


		public function stillPlaying(e:TuioTouchEvent):void
		{
			//trace("stillPlaying");
			if (timeoutPopup != null)
			{
				removeChild(timeoutPopup);
				timeoutPopup = null;
				resetTimer();
			}
		}

		public function playSound(n:Number):void
		{
			switch (n)
			{
				case 1 :
					SoundAS.playFx(SBC);
					break;
				case 2 :
					SoundAS.play(SCA);
					break;
				case 3 :
					SoundAS.playFx(SCPI);
					break;
				case 4 :
					SoundAS.play(SCO);
					break;
				case 5 :
					SoundAS.play(SG);
					break;
				case 6 :
					SoundAS.play(SGG);
					break;
				case 7 :
					SoundAS.play(SH);
					break;
				case 8 :
					SoundAS.playFx(SMM);
					break;
				case 9 :
					SoundAS.play(SR);
					break;
				case 10 :
					SoundAS.play(ST);
					break;
				case 11 :
					SoundAS.play(SV);
					break;
				case 12 :
					SoundAS.play(SWA);
					break;
				case 13 :
					SoundAS.play(SWE);
					break;
				case 14 :
					SoundAS.play(SGM);
					break;
				case 15 :
					SoundAS.play(MR);
					break;
				case 16 :
					SoundAS.play(SSV);
					break;
			}
		}
		
		public function stopSound():void
		{
			SoundAS.stopAll();
		}

		public function zeroOrOne():Number
		{
			var num:Number = Math.floor(Math.random() * 2);
			return num;
		}


		public function startGame():void
		{
			ct = 1;
			playerGo = true;
			//countdownTimer.stop();
			//countdownTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, countdown2);
			owner.isNowPlaying();
			this.nextFrame();
			countdown();
			/*gameTimer = new Timer(3000);
			gameTimer.stop();
			gameTimer.start();
			gameTimer.addEventListener(TimerEvent.TIMER_COMPLETE, countdown);*/
		}

		public function goToSecondPart(e:TuioTouchEvent):void
		{
			countdown();
		}

		public function countdown(e:TimerEvent = null):void
		{
			if (gameTimer != null)
			{
				gameTimer.stop();
				gameTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, countdown);
			}
			switch (ct)
			{
				case 1 :
					this.gotoAndStop(2);
					drGm = new DrGm(this, owner.whatLang);
					addChild(drGm);
					drGm.x = 0;
					drGm.y = 0;
					setChildIndex(exitBtn, numChildren - 1);
					ct++;
					break;
				case 2 :
					if (drGm != null)
					{
						removeChild(drGm);
						drGm = null;
					}
					this.gotoAndStop(3);
					//trace("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& currentFrame = " + this.currentFrame);
					var tempArray:Array = new Array(d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16);
					for (var i:int = 0; i < tempArray.length; i++)
					{
						if (i < howManyDollars)
						{
							TweenMax.to(tempArray[i], 1, {delay:0.25 * i, frame:30, ease:Strong.easeOut});
						}
					}

					nextPart.addEventListener(TuioTouchEvent.TOUCH_DOWN, goToSecondPart);
					//cannedAnimation.play();
					if (howMuchMoney <= 400000)
					{
						if (l == 0)
						{
							resultsT.gotoAndStop(3);
						}
						else
						{
							resultsT.gotoAndStop(4);
						}
					}
					else
					{
						if (l == 0)
						{
							resultsT.gotoAndStop(1);
						}
						else
						{
							resultsT.gotoAndStop(2);
						}
					}
					if (l == 0)
					{
						dTitle.gotoAndStop(1);
					}
					else
					{
						dTitle.gotoAndStop(2);
					}

					nextPart.t.text = owner.owner.loadXML.dreamSHClickHere[l];

					scales.addChild(numberSpinner);
					scales.dollaStack.alpha = 0;
					scales.spendText.gotoAndStop(l + 1);
					scales.saveText.gotoAndStop(l + 1);
					TweenMax.to(scales.dollaStack, 1, {alpha:1, ease:Linear.easeNone});
					TweenMax.fromTo(scales.dollaStack, 2, {frame:2}, {frame:90, ease:Linear.easeNone});
					TweenMax.fromTo(scales, 2, {frame:2}, {frame:90, ease:Linear.easeNone});
					numberSpinner.visible = true;
					numberSpinner.init(howMuchMoney);
					numberSpinner.stopAnimation();
					playSound(15);
					TweenMax.delayedCall(1.2, playSound, [16]);

					ct++;
					break;

				case 3 :
					//numberSpinner.visible = false;
					this.gotoAndStop(4);
					saGm = new SaGm(this);
					addChild(saGm);
					playSound(14);
					setChildIndex(exitBtn, numChildren - 1);
					ct++;
					break;

				case 4 :
					this.gotoAndStop(5);
					creditStairs.play();
					creditStairs.ft.t.text = owner.owner.loadXML.dreamCheckOutOther[l];
					creditStairs.t1.text = owner.owner.loadXML.dreamAutomaticTransfers[l];
					creditStairs.t2.text = owner.owner.loadXML.dreamMoneyMarketAccount[l];
					creditStairs.t3.text = owner.owner.loadXML.dreamCD[l];
					creditStairs.t4.text = owner.owner.loadXML.dreamRetirementPlanning[l];
					creditStairs.t5.text = owner.owner.loadXML.dreamInvestments[l];
					gameTimer = new Timer(10000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER_COMPLETE, countdown);
					ct++;
					setChildIndex(exitBtn, numChildren - 1);

					break;

				case 5 :
					//trace("----------------------------------------- done");
					exitGame();
					break;
			}
		}

		public function exitGame(e:TuioTouchEvent = null):void
		{
			if (drGm != null)
			{
				drGm.allTimedOut();
			}
			playerGo = false;
			this.visible = false;
			SoundAS.stopAll();
			SoundAS.removeAll();
			timeoutTimer.stop();
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, allTimedOut);
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, reallyTimedOut);
			removeEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimer);
			owner.removeThis();
		}

		public function goToThirdPart(e:TuioTouchEvent):void
		{
			//saGm.tipsBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goToThirdPart);
			//saGm.nextFrame();

			//saGm.exitBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, closeEverything);
		}

		private function countPlayers(e:TuioTouchEvent):void
		{
			//trace("countPlayers");
			//trace(e.currentTarget.name);

			playerGo = true;
			//countdownTimer.stop();
			//countdownTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, countdown2);
			nextFrame();
			startGame();
			owner.isNowPlaying();
		}

		public function stopEverything():void
		{
			SoundAS.stopAll();
			timeoutTimer.stop();
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, allTimedOut);
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, reallyTimedOut);
			removeEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimer);
			stopCounting();
		}

		public function startCountdown():void
		{
			owner.owner.fadeGamesOut();

			//countdownTimer = new Timer(1000);
			//countdownTimer.stop();
			//countdownTimer.start();
			//countdownTimer.addEventListener(TimerEvent.TIMER_COMPLETE, countdown2);

			jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
		}

		public function stopCounting():void
		{
			//countdownTimer.stop();
			//countdownTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, countdown2);
		}

		private function countdown2(e:TimerEvent):void
		{
			time--;
			t.text = time.toString();
			if (time == 0)
			{
				//countdownTimer.stop();
				//countdownTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, countdown2);
				//nextFrame();
				//if (playerGo == false)
				//{
				//visible = false;
				SoundAS.stopAll();
				owner.removeThis();
				//}
				//else
				//{
				//startGame();
				//}
			}
		}

		public function closeEventything(e:TuioTouchEvent):void
		{
			playerGo = false;
			this.visible = false;
			SoundAS.stopAll();
			owner.removeThis();
		}

		public function allDone():void
		{
			SoundAS.stopAll();
			owner.allDone();
		}

	}

}