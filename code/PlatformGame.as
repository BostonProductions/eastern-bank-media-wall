﻿package code
{
	import flash.display.*;				// import display class
	import flash.events.*;				// import events class
	import flash.text.*;					// import text class
	import flash.utils.getTimer;		// import timer class
	//import flash.events.TuioTouchEvent;
	import flash.ui.Mouse;
	import flash.geom.Point;
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	import code.Document;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	public class PlatformGame extends MovieClip {
		
		// variables that will remain constant throughout the game
		private var gravity:Number = .004;				// variable to move the hero back down to the ground
		private var edgeDistance:Number = 300;		// variable to control when to start scrolling the background
		private var heroY:Number;
		private var heroX:Number;
		
		private var isMoving:Boolean = false;

		// object arrays
		private var fixedObjects:Array;   	// hold all objects the hero can stand on or be stopped by
		private var otherObjects:Array;		// holds all objects that the hero can interact with - door, key, etc.
		
		// hero and enemies
		private var hero:Object;				// hero is already a movie clip on the Stage inside the gameLevel movie clip
		private var enemies:Array;			// holds all the enemies within the game level
		
		// game state
		private var playerObjects:Array;					// store the objects that the user picks up
		private var gameScore:int;						// create a variable to update the user's score
		private var gameMode:String = "start";		// game mode that convey the various functions to the hero
		private var playerLives:int;						// create a variable to store the nubmer of lives the hero has
		private var lastTime:Number = 0;				// used to drive the time-based animation used by the game elements
		
		private var owner:Document;
		public var whichPong:Number;
		public var whichScreen:Number;
		private var closeLarge:CloseLarge;
		
		private var curY:Number;
		private var curX:Number;
		
		private var heroStartX:Number;
		private var heroStartY:Number;
		
		private var closeoutTimer:Timer;
		
		public function PlatformGame(xPos:Number, yPos:Number, myOwner:Document, w:Number, s:Number) 
		{
			//trace("_________________________________ PlatformGame");
			owner = myOwner;
			
			whichPong = w;
			whichScreen = s;
			this.x = xPos; // + (owner.loadXML.screenWidth - this.width)/2;
			this.y = 500;
			
			
			
			closeLarge = new CloseLarge();
			addChild(closeLarge);
			closeLarge.x = 1080 - closeLarge.width;
			closeLarge.y = -500;
			//closeLarge.rotation = 90;
			closeLarge.endTxt.text == " "
			closeLarge.addEventListener(TuioTouchEvent.TOUCH_DOWN, endClickX);
			
			closeoutTimer = new Timer(180000, 1);
			closeoutTimer.start();
			closeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			
			
			// add an event listener to the startButton
			//startButton.addEventListener(TuioTouchEvent.CLICK, clickStart);
		}
		
		

		private function timeout(e:TimerEvent):void
		{
			closeoutTimer.reset();
			closeoutTimer.stop();
			closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			owner.removeNewPlatform(whichPong, whichScreen);
		
		}

		
		private function endClickX(e:TuioTouchEvent = null):void
		{
			
			
			//reset all assets and play again!
			//isEnding = false;
			
			if (closeoutTimer != null)
			{
				closeoutTimer.reset();
				closeoutTimer.stop();
				closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			}
				
			owner.removeNewPlatform(whichPong, whichScreen);
		}
		
		// add an event handler to respond to the listener
		/*function clickStart(e:TuioTouchEvent) {
			//startButton.removeEventListener(TuioTouchEvent.CLICK, clickStart);
			gotoAndStop("play");
			startPlatformGame();
			startGameLevel();
		}*/

		// start game by setting intital values for some of the declared variables
		// this is called on frame 2 of the FLA file
		public function startPlatformGame() {
			playerObjects = new Array();
			gameScore = 0;
			gameMode = "play";
			playerLives = 3;
			setChildIndex(closeLarge, numChildren - 1);
		}
		
		// start level for the game is called on a frame where there is a "gamelevel" move clip
		public function startGameLevel() {
			
			// call functions below to create characters in the game level
			createHero();
			
			addEnemies();
			
			// examine level and record all the objects (movie clips - walls, floor, key, etc.)
			examineLevel();
			
			// add listeners
			this.addEventListener(Event.ENTER_FRAME,gameLoop);
			//stage.addEventListener(KeyboardEvent.KEY_DOWN,keyDownFunction);
			//stage.addEventListener(KeyboardEvent.KEY_UP,keyUpFunction);
			stage.addEventListener(TuioTouchEvent.TOUCH_MOVE, mDown);
			stage.addEventListener(TuioTouchEvent.TOUCH_DOWN, mDown);
			stage.addEventListener(TuioTouchEvent.TOUCH_UP, mUp);
			
			// set game state
			gameMode = "play";
			addScore(0);
			showLives();
			setChildIndex(closeLarge, numChildren - 1);
		}
		
		// creates the hero object and sets all properties
		// the hero movie clip is already in the "gamelevel" movie clip on the Stage
		public function createHero() {
			hero = new Object();					// create a new "Object" to set many properties
			
			hero.mc = gamelevel.hero;			// set a "mc" property = the actual movie clip in the gamelevel movie clip
			hero.dx = 0.0;							// describe the horizontal velocity of the hero
			hero.dy = 0.0;							// describe the vertical velocity of the hero
			hero.inAir = false;						// will be set to "true" when the hero is not resting on the ground
			
			hero.direction = 1;						// determine which direction the hero is facing
			hero.animstate = "stand";				// determines which frame of the hero movie clip to display
			hero.walkAnimation = new Array(2,3,4,5,6,7,8);		// stores the frames of the walk sequence
			hero.animstep = 0;						// used to keep track of which step in the animation is currently showing
			
			hero.jump = false;						// this property is set to true when the user presses the SPACEBAR
			hero.moveLeft = false;					// toggles to true when the user pressed the LEFT arrow key on the keyboard
			hero.moveRight = false;				// toggles to true when the user pressed the RIGHT arrow key on the keyboard
			
			hero.jumpSpeed = .8;					// determines how high the hero jumps
			hero.walkSpeed = .15;					// determines how fast the hero walks
			
			hero.width = 33.0;						// used to determine collisions
			hero.height = 40.0;
			
			hero.startx = hero.mc.x;				// if the user hits an enemy he is reset to original coordinates
			hero.starty = hero.mc.y;				// so, need to record the starting position for the hero movie clip
			
			
			heroStartX = hero.mc.x;
			heroStartY = hero.mc.y;
		}
		
		// finds all enemies in the level and creates an object for each
		public function addEnemies() {
			enemies = new Array();				// store all the enemy movie clips in "game level" movie clip
			var i:int = 1;								// set a temp variable to start looping through enemy movie clips
			while (true) {								// repeat while the following is true
				if (gamelevel["enemy"+i] == null) break;		// if there is an enemy with an instance name of "enemy1", "enemy2", etc.
				
				var enemy = new Object();							// create an "Object" to set properties
				
				enemy.mc = gamelevel["enemy"+i];				// record the actual movie clip in the gamelevel movie clip
				enemy.dx = 0.0;						// set the initial horizontal velocity
				enemy.dy = 0.0;						// set the initial vertical velocity
				enemy.inAir = false;					// used to let the enemies jump
				
				enemy.direction = 1;				// determine which direction the enemy is facing
				enemy.animstate = "stand"		// determines which frame of the enemy movie clip to display
				enemy.walkAnimation = new Array(2,3,4,5);  // stores the frames of the walk sequence
				enemy.animstep = 0;				// used to keep track of which step in the animation is currently showing
				
				enemy.jump = false;				// Boolean to allow the enemy to jump
				enemy.moveRight = true;			// the enemy starts moving to the right when the game starts
				enemy.moveLeft = false;			// toggles to true when enemy needs to change direction
				
				enemy.jumpSpeed = 1.0;			// determine how high the enemy jumps
				enemy.walkSpeed = .08;			// determine how fast the enemy walks
				
				enemy.width = 50.0;				// used for collision detection
				enemy.height = 30.0;
				
				enemies.push(enemy);				// add the enemy to the enemies array
				i++;										// increment the value of "i" by one and loop again
			}
		}
		
		// after the hero and enemies have been found,
		// look at all level children and record walls, floors and items
		public function examineLevel() {
			fixedObjects = new Array();			// create a new array to store the floor and walls
			otherObjects = new Array();			// create a new array to store the key, door, etc.
			
			// loop through the number of children contained within the gamelevel movie clip
			for(var i:int=0; i < this.gamelevel.numChildren; i++) {
				var mc = this.gamelevel.getChildAt(i);   // create a temp variable to hold the current child in the loop
				// If the object is a Floor or Wall, add object to fixedObjects
				if ((mc is Floor) || (mc is Wall)) {
					var floorObject:Object = new Object();   		// create a new object to store properties
					floorObject.mc = mc;									// record the reference to the actual movie clip
					floorObject.leftside = mc.x;							// store the left side of the movie clip
					floorObject.rightside = mc.x+mc.width;			// store the right side of the movie clip
					floorObject.topside = mc.y;							// store the top of the movie clip
					floorObject.bottomside = mc.y+mc.height;		// store the bottom of the movie clip
					fixedObjects.push(floorObject);						// add the current object to the array
					
				// add treasure, key and door to otherOjects
				} else if ((mc is Treasure) || (mc is Key) || (mc is Door) || (mc is Chest) || (mc is Coin)) {
					otherObjects.push(mc);  // add the other objects to the array
				}
			}
		}
		
		// create event handler called by event listener
		// this function determines which key is pressed, and sets hero properties
		public function keyDownFunction(e:KeyboardEvent) {
			if (gameMode != "play") return;     	// don't move the hero until in game mode value is set to "play"
			if (e.keyCode == 37) {       				// if the user is pressing the LEFT arrow key
				hero.moveLeft = true;					// set the hero boolean variable to true
			} else if (e.keyCode == 39) {			// if the user is pressing the RIGHT arrow key
				hero.moveRight = true;				// set the hero boolean variable to true
			} else if (e.keyCode == 32) {			// if the user presses the SPACEBAR
				if (!hero.inAir) {							// if the hero is not already jumping in the air,
					hero.jump = true;					// set the boolean variable for jumping to true
				}
			}
		}
		public function keyUpFunction(e:KeyboardEvent) {
			if (e.keyCode == 37) {						// if the user releases the LEFT arrow key
				hero.moveLeft = false;					// set the hero boolean varaible to false
			} else if (e.keyCode == 39) {			// if the user releases the RIGHT arrow key
				hero.moveRight = false;				// set the hero boolean varaible to false
			}
		}
		
		function mDown(e:TuioTouchEvent):void 
		{
			closeoutTimer.reset();
			closeoutTimer.start();
			if (e.stageX > this.x && e.stageX < this.x + 1080)
			{
				curY = e.stageY - 500;
				curX = e.stageX;
				isMoving = true;
			}
		}
		
		function mUp(e:TuioTouchEvent):void 
		{
			closeoutTimer.reset();
			closeoutTimer.start();
			if (e.stageX > this.x && e.stageX < this.x + 1080)
			{
				isMoving = false;
			}
		}
		
		function DistanceTwoPoints(x1:Number, x2:Number,  y1:Number, y2:Number): Number {
			var dx:Number = x1-x2;
			var dy:Number = y1-y2;
			return Math.sqrt(dx * dx + dy * dy);
		}
		
		// perform all game tasks through ENTER_FRAME listener
		// this function is called once per frame
		public function gameLoop(e:Event) {
			// get time difference in milliseconds the last time this function was called
			if (lastTime == 0) lastTime = getTimer();
			var timeDiff:int = getTimer() - lastTime;
			lastTime += timeDiff;
			
			heroX = gamelevel.localToGlobal(new Point(hero.mc.x, 0)).x;
			heroY = (hero.mc.y*2)-hero.mc.height*2;
			
			var diff:Number = curX-heroX;
						
			if(isMoving == true)
			{
				if (curY<heroY && diff > 0) {
					hero.moveRight = true;	// if the user presses the SPACEBAR
					hero.moveLeft = false;
					if (!hero.inAir) {							// if the hero is not already jumping in the air,
						hero.jump = true;					// set the boolean variable for jumping to true
					}
				}
				if (curY<heroY && diff < 0) {
					hero.moveLeft = true;	// if the user presses the SPACEBAR
					hero.moveRight = false;
					if (!hero.inAir) {							// if the hero is not already jumping in the air,
						hero.jump = true;					// set the boolean variable for jumping to true
					}
				}
				if (diff < 0) {       				// if the user is pressing the LEFT arrow key
					hero.moveLeft = true;					// set the hero boolean variable to true
				} 
				else if (diff > 0) {			// if the user is pressing the RIGHT arrow key
					hero.moveRight = true;				// set the hero boolean variable to true
				}
			}
			if(isMoving == false)
			{
				hero.moveLeft = false;					// set the hero boolean variable to true
				hero.moveRight = false;				// set the hero boolean variable to true
			}
			
			// only perform tasks if in play mode
			if (gameMode == "play") {
				moveEnemies(timeDiff);				// call the function that moves the enemy
				moveCharacter(hero, timeDiff);		// call the function that moves the character passed in first parameter = hero
				checkCollisions();							// call the function that looks for collisions
				scrollWithHero();							// call the function that scrolls the background based on hero's position
			}
		}
		
		// loop through all enemies and move them
		public function moveEnemies(timeDiff:int) {
			// loop through all the enemy object stored in the enemies array
			for(var i:int=0;i<enemies.length;i++) {
				moveCharacter(enemies[i],timeDiff);  	// call the moveCharacter function and pass the current enemy and the time
				
				if (enemies[i].hitWallRight) {					// if the current enemy hits a right wall
					enemies[i].moveLeft = true;				
					enemies[i].mc.eBody.eLabel.scaleX = -1;		// the enemy should start moving the opposite direction (left)
					enemies[i].moveRight = false;				// and stop moving to the right
				} else if (enemies[i].hitWallLeft) {			// if the current enemy hits a left wall
					enemies[i].moveRight = true;		
					enemies[i].mc.eBody.eLabel.scaleX = 1;		// the enemy should start moving the opposite direction (right)
					enemies[i].moveLeft = false;				// and stop moving to the left
				}
			}
		}
		
		// primary function for character movement fir hero and enemies
		public function moveCharacter(char:Object, timeDiff:Number) {
			if (timeDiff < 1) return;  // if time has passed

			// assume character pulled down by gravity
			var verticalChange:Number = char.dy * timeDiff + timeDiff * gravity;   // determine how high vertically the character is
			if (verticalChange > 15.0) verticalChange = 15.0;			// limit the vertical change to 15 pixels
			char.dy += timeDiff * gravity;									// set the vertical change	
			
			// react to changes from key presses in the "keyDownFunction"
			var horizontalChange = 0;								// when game starts, there is no change in horizontal movement
			var newAnimState:String = "stand";				// when game starts, set the animate state for character to "stand"
			var newDirection:int = char.direction;				// retrieve the intial character direction
			
			if (char.moveLeft) {													// if the character needs to walk left
				horizontalChange = -char.walkSpeed * timeDiff;		// set the horizontal change to negative value of walk speed
				newAnimState = "walk";										// set the animation state to "walk"
				newDirection = -1;												// set the direction to -1 (for scaleX flip horizontally)
				
			} else if (char.moveRight) {										// if the character needs to walk right
				horizontalChange = char.walkSpeed * timeDiff;		// set the horizontal change based on value of walk speed
				newAnimState = "walk";										// set the animation state to "walk"
				newDirection = 1;												// set the direction back to 1 (for scaleX flip horizontally)
			}
			if (char.jump) {														// if the character needs to jump
				char.jump = false;												// turn off boolean varaible set true when user pressed SPACEBAR
				char.dy = -char.jumpSpeed;									// set the change in the vertical direction based on jumpSpeed value
				verticalChange = -char.jumpSpeed;						// set the vertical change
				newAnimState = "jump";										// set the animation state to "jump"
			}
			
			// assume no wall hit, and hanging in air
			char.hitWallRight = false;
			char.hitWallLeft = false;
			char.inAir = true;
					
			// find new vertical position
			var newY:Number = char.mc.y + verticalChange;			// calculate the new vertical position of the character
		
			// loop through all fixed objects in the "fixedObjects" array to see if character has landed
			// registration point for characters are at the feet
			// registration point for walls and floors are at the top
			for(var i:int=0; i<fixedObjects.length; i++) {
				if ((char.mc.x + char.width/2 > fixedObjects[i].leftside) && (char.mc.x - char.width/2 < fixedObjects[i].rightside)) {
					if ((char.mc.y <= fixedObjects[i].topside) && (newY > fixedObjects[i].topside)) {
						newY = fixedObjects[i].topside;  			// stop the character on the object's top surface
						char.dy = 0;										// set its change in vertical direction to 0
						char.inAir = false;								// set the boolean variable for airborn to false because character has landed
						break;												// break from the loop so that it doesn't have to loop any more objects
					}
				}
			}
			
			// find new horizontal position
			var newX:Number = char.mc.x + horizontalChange;		// calculate the new horizontal position of the character
		
			// loop through all objects to see if character has bumped into a wall
			for(i=0; i<fixedObjects.length; i++) {
				if ((newY > fixedObjects[i].topside) && (newY-char.height < fixedObjects[i].bottomside)) {
					if ((char.mc.x-char.width/2 >= fixedObjects[i].rightside) && (newX-char.width/2 <= fixedObjects[i].rightside)) {
						newX = fixedObjects[i].rightside + char.width/2;
						char.hitWallLeft = true;
						break;
					}
					if ((char.mc.x+char.width/2 <= fixedObjects[i].leftside) && (newX+char.width/2 >= fixedObjects[i].leftside)) {
						newX = fixedObjects[i].leftside - char.width/2;
						char.hitWallRight = true;
						break;
					}
				}
			}
			
			// set the horizontal and vertical position of character
			char.mc.x = newX;
			char.mc.y = newY;
			
			// set animation state (appearance of the character)
			if (char.inAir) {
				newAnimState = "jump";
			}
			if (newAnimState != null)
			{
				char.animstate = newAnimState;
			}
			else
			{
				char.animstate = "walk";
			}
			// if the character animation state is set to "walk", move along walk cycle frame-by-frame animation
			if (char.animstate == "walk") {
				char.animstep += timeDiff/60;								// calculate the index number to show walk cycle frame
				if (char.animstep > char.walkAnimation.length) {		// if the animation step is greater than the number of items in the array
					char.animstep = 2;											// set the index number back to the beginning
				}
				if (char.walkAnimation != null)
				{
					if (char.animstep != null)
					{
						if (char.mc != null)
						{
							char.mc.gotoAndStop(char.walkAnimation[Math.floor(char.animstep)]);
						}
					}
				}
						// show the walk cysle frame
				//trace(char.animstep);
				//trace(char.walkAnimation);
				//trace(char.mc);
				
				
			// if the character is not walking, show stand or jump state
			} else if (char.animstate != null) {
				//trace("char.animstate = " + char.animstate);
				char.mc.gotoAndStop(char.animstate);
			}
			
			// changed directions for the character
			if (newDirection != char.direction) {
				char.direction = newDirection;
				char.mc.scaleX = char.direction;		// flip the character horizontally
			}
		}
		
		// scroll background to the right or left if needed
		// function constantly checks the position of the hero relative to the Stage
		public function scrollWithHero() {
			var stagePosition:Number = gamelevel.x + (hero.mc.x*2);		// calculate the stagePosition based on game level and hero
			var rightEdge:Number = 1080 - edgeDistance;	// determine when to scroll based on the right edge
			var leftEdge:Number = edgeDistance;									// determine when to scroll based on left edge
						
			if (stagePosition > rightEdge) {										// if the hero moves past the right edge
				gamelevel.x -= (stagePosition - rightEdge);					// physcially move the entire "gamelevel" movie clip
				if (gamelevel.x < -(gamelevel.width - 1080)) gamelevel.x = -(gamelevel.width - 1080);  // stop moving if goes to far to the right
			}
			if (stagePosition < leftEdge) {										// if the hero moves to far to the left
				gamelevel.x += (leftEdge - stagePosition);					// move the entire "gamelevel" movie clip
				if (gamelevel.x > 0) gamelevel.x = 0;							// stop the scroll at the left edge of the "gamelevel" movie clip
			}
		}
		
		// check collisions with enemies, items
		public function checkCollisions() {
			// loop through all the enemies
			for(var i:int=enemies.length-1; i>=0; i--) {
				if (hero.mc.hitTestObject(enemies[i].mc)) {
					// is the hero jumping down onto the enemy?
					if (hero.inAir && (hero.dy > 0)) {
						enemyDie(i);
					} else {
						heroDie();
						enemies[i].mc.gotoAndPlay("win");
					}
				}
			}
			
			// items
			for(i=otherObjects.length-1; i>=0; i--) {
				if (hero.mc.hitTestObject(otherObjects[i])) {
					//trace("hitting");
					getObject(i);
				}
			}
		}
		
		// remove enemy if hero jumps on it
		public function enemyDie(enemyNum:int) {
			// communicate to PointBust.as file to creates a text field that states "Got Em!" where the enemy's location is
			var pb:PointBurst = new PointBurst(gamelevel,"Bill Paid!", enemies[enemyNum].mc.x, enemies[enemyNum].mc.y-20);
			var tempArray:Array = new Array();
			tempArray.push(enemies[enemyNum].mc);
			enemies.splice(enemyNum,1);								// remove enemy from array
			
			tempArray[0].gotoAndPlay("die");
			if(tempArray[0].currentFrame == 25)
			{
				gamelevel.removeChild(tempArray[0]);			// remove enemy from Stage
				tempArray = [];
			}
		}	
		
		// enemy got player
		public function heroDie() {
			// show dialog box
			//trace("heroDie");
			var dialog:Dialog = new Dialog();
			dialog.x = 350;
			dialog.y = 200;
			addChild(dialog);
		
			if (playerLives == 0) {
				gameMode = "gameover";
				
				dialog.okButton.endTxt.text = "Close";
				dialog.message.text = "Game Over!";
			} else {
				gameMode = "dead";
				dialog.message.text = "Too Much Debt!";
				playerLives--;
			}
			hero.mc.gotoAndPlay("die");
		}
		
		// player collides with objects
		public function getObject(objectNum:int) {
			// award points for treasure
			if (otherObjects[objectNum] is Treasure) {
				var pb:PointBurst = new PointBurst(gamelevel, 100, otherObjects[objectNum].x, otherObjects[objectNum].y);
				gamelevel.removeChild(otherObjects[objectNum]);
				otherObjects.splice(objectNum,1);
				addScore(100);
				
			// got the key, add to inventory
			} else if (otherObjects[objectNum] is Coin) {
				pb = new PointBurst(gamelevel, 1000, otherObjects[objectNum].x, otherObjects[objectNum].y);
				pb = new PointBurst(gamelevel,"Secret Stash!", otherObjects[objectNum].x, otherObjects[objectNum].y+30);
				gamelevel.removeChild(otherObjects[objectNum]);
				otherObjects.splice(objectNum,1);
				addScore(1000);
				
			// got the key, add to inventory
			} else if (otherObjects[objectNum] is Key) {
				pb = new PointBurst(gamelevel, "Vault Key!", otherObjects[objectNum].x, otherObjects[objectNum].y);
				playerObjects.push("Key");
				gamelevel.removeChild(otherObjects[objectNum]);
				otherObjects.splice(objectNum,1);
				
			// hit the door, end level if hero has the key
			} else if (otherObjects[objectNum] is Door) {
				if (playerObjects.indexOf("Key") == -1) return;
				if (otherObjects[objectNum].currentFrame == 1) {
					otherObjects[objectNum].gotoAndPlay("open");
					levelComplete();
				}
				
			// got the chest, game won
			} else if (otherObjects[objectNum] is Chest) {
				otherObjects[objectNum].gotoAndStop("open");
				gameComplete();
			}
					
		}
		
		// add points to score
		public function addScore(numPoints:int) {
			gameScore += numPoints;
			scoreDisplay.text = String( gameScore);
		}
		
		// update player lives
		public function showLives() {
			livesDisplay.text = String(playerLives);
		}
		
		// level over, bring up dialog
		public function levelComplete() {
			gameMode = "done";
			var dialog:Dialog = new Dialog();
			dialog.x = 350;
			dialog.y = 200;
			addChild(dialog);
			dialog.message.text = "Level Complete!";
			dialog.okButton.endTxt.text = "Next Level";
		}
		
		// game over, bring up dialog
		public function gameComplete() {
			gameMode = "gameover";
			var dialog:Dialog = new Dialog();
			dialog.x = 350;
			dialog.y = 200;
			addChild(dialog);
			dialog.message.text = "You Banked Your Savings!";
			dialog.okButton.endTxt.text = "Close";
		}
		
		// dialog button clicked
		public function clickDialogButton(e:TuioTouchEvent) {
			closeoutTimer.reset();
			closeoutTimer.start();
			removeChild(MovieClip(e.currentTarget.parent));
			
			// new life, restart, or go to next level
			if (gameMode == "dead") {
				// reset hero
				showLives();
				hero.mc.x = hero.startx;
				hero.mc.y = hero.starty;
				gameMode = "play";
			} else if (gameMode == "gameover") {
				cleanUp();
				gotoAndStop("start");
				hero.mc.x = heroStartX;
				hero.mc.y = heroStartY;
				//hero.mc.x = hero.startx;
				//hero.mc.y = hero.starty;
				cleanUp();
				endClickX();
			} else if (gameMode == "done") {
				cleanUp();
				nextFrame();
				//hero.mc.x = hero.startx;
				//hero.mc.y = hero.starty;
				//hero.mc.x = hero.startx;
				//hero.mc.y = hero.starty;
				//cleanUp();
			}
			
			// give stage back the keyboard focus
			//stage.focus = stage;
		}			
		
		// clean up game
		public function cleanUp() {
			//removeChild(gamelevel);
			
			stage.removeEventListener(TuioTouchEvent.TOUCH_MOVE, mDown);
			stage.removeEventListener(TuioTouchEvent.TOUCH_DOWN, mDown);
			stage.removeEventListener(TuioTouchEvent.TOUCH_UP, mUp);
			this.removeEventListener(Event.ENTER_FRAME,gameLoop);
			//stage.removeEventListener(KeyboardEvent.KEY_DOWN,keyDownFunction);
			//stage.removeEventListener(KeyboardEvent.KEY_UP,keyUpFunction);
		}
		
	}
	
}