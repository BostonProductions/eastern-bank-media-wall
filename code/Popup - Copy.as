﻿package code {
	
	import flash.display.MovieClip;
	
	import flash.filters.DropShadowFilter;
	import flash.filters.BitmapFilterQuality;
	
	import flash.events.TransformGestureEvent;
	import flash.events.GesturePhase;
	//import flash.ui.Multitouch.inputMode;
	import flash.events.Event;
	import org.tuio.TuioTouchEvent;
	import flash.media.Video;
	
	import flash.net.NetConnection;
	import flash.net.NetStream;
    import flash.net.NetStreamAppendBytesAction;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.display.Loader;
	import flash.events.NetStatusEvent;
	import code.drawingApp;
	import code.MortgageCalculator;
	import code.SliderPointer;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import code.Document;
	import code.Close;
	import code.Tab;
	
	import code.fonts.Gotham;
	import code.fonts.GothamBold;
    import fl.text.TLFTextField;
    import flash.text.TextFieldType;
    import flash.text.TextFormat;
    import flash.text.TextFormatAlign;
	import flashx.textLayout.formats.TextAlign;
	import flashx.textLayout.formats.TextLayoutFormat;
	import flashx.textLayout.elements.TextFlow;
    import flash.text.engine.TextBaseline;
	import flash.text.Font;
	
	public class Popup extends MovieClip {
		
		private var shadowAlpha:Number = 1;
		private var shadowAngle:Number = 45;
		private var shadowBlurX:Number = 30;
		private var shadowBlurY:Number = 30;
		private var shadowColor:uint = 0x003366;
		private var shadowDistance:Number = 0;
		private var shadowHideObject:Boolean = false;
		private var shadowInner:Boolean = false;
		private var shadowKnockout:Boolean = false;
		private var shadowQuality:int = BitmapFilterQuality.MEDIUM;
		private var shadowStrength:Number = .75;
		private var sf:DropShadowFilter;
		
		/*public var pSlider:SliderPointer;
		public var tSlider:SliderPointer;
		public var iSlider:SliderPointer;*/
		
		public var whichPopup:Number;
		
		public var whichMenu:Number;
		
		private var whichButtonOpen:Number = 1;
		
		private var curID:int = -1;
		
		public var video:Video = new Video();
		public var nc:NetConnection = new NetConnection();
		public var ns:NetStream;
		
		private const videoURL:String = "FirstThingFirst.mp4";
		
		public var owner:Document;
		private var drawApp:drawingApp;
		
		public var timeoutTimer:Timer;
		
		public var mortgageCalc:MortgageCalculator;
		
		private var arrNum:Number;
		
		
		private var t1:TLFTextField = new TLFTextField();
		private var t2:TLFTextField = new TLFTextField();
		private var t3:TLFTextField = new TLFTextField();
		private var t4:TLFTextField = new TLFTextField();
		private var t5:TLFTextField = new TLFTextField();
		private var t6:TLFTextField = new TLFTextField();
		private var tFormat:TextLayoutFormat = new TextLayoutFormat();
		private var tTextFlow:TextFlow;
		private var tArray:Array;
		
		private var gotham:Font = new Gotham();
		private var gothamBold:Font = new GothamBold();
		
		public var close_btn:Close;
		private var b1:Tab;
		private var b2:Tab;
		private var b3:Tab;
		private var b4:Tab;
		private var b5:Tab;
		private var b6:Tab;
		private var bArray:Array;
		
		private var bgLoader:Loader = new Loader();
		private var bgFile:URLRequest;
		
		private var tab1:MovieClip = new MovieClip();
		private var tab2:MovieClip = new MovieClip();
		private var tab3:MovieClip = new MovieClip();
		private var tab4:MovieClip = new MovieClip();
		private var tab5:MovieClip = new MovieClip();
		private var tab6:MovieClip = new MovieClip();
		private var tabArray:Array;
		
		private var tab1Alt:Boolean = false;
		private var tab2Alt:Boolean = false;
		private var tab3Alt:Boolean = false;
		private var tab4Alt:Boolean = false;
		private var tab5Alt:Boolean = false;
		private var tab6Alt:Boolean = false;
		private var tabAltArray:Array;
		
		private var tab1Text:Boolean = false;
		private var tab2Text:Boolean = false;
		private var tab3Text:Boolean = false;
		private var tab4Text:Boolean = false;
		private var tab5Text:Boolean = false;
		private var tab6Text:Boolean = false;
		private var tabTextArray:Array;
		
		private var text1:TLFTextField = new TLFTextField();
		private var text2:TLFTextField = new TLFTextField();
		private var text3:TLFTextField = new TLFTextField();
		private var text4:TLFTextField = new TLFTextField();
		private var text5:TLFTextField = new TLFTextField();
		private var text6:TLFTextField = new TLFTextField();
		private var textFormat:TextLayoutFormat = new TextLayoutFormat();
		private var textTextFlow:TextFlow;
		private var textArray:Array;
		
		private var tab1Image:Boolean = false;
		private var tab2Image:Boolean = false;
		private var tab3Image:Boolean = false;
		private var tab4Image:Boolean = false;
		private var tab5Image:Boolean = false;
		private var tab6Image:Boolean = false;
		private var tabImageArray:Array;
		
		private var tab1Video:Boolean = false;
		private var tab2Video:Boolean = false;
		private var tab3Video:Boolean = false;
		private var tab4Video:Boolean = false;
		private var tab5Video:Boolean = false;
		private var tab6Video:Boolean = false;
		private var tabVideoArray:Array;
		
		private var tab1Active:Boolean = true;
		private var tab2Active:Boolean = true;
		private var tab3Active:Boolean = true;
		private var tab4Active:Boolean = true;
		private var tab5Active:Boolean = true;
		private var tab6Active:Boolean = true;
		private var tabActiveArray:Array = new Array();
		
		private var tab1Img:Loader = new Loader();
		private var tab2Img:Loader = new Loader();
		private var tab3Img:Loader = new Loader();
		private var tab4Img:Loader = new Loader();
		private var tab5Img:Loader = new Loader();
		private var tab6Img:Loader = new Loader();
		private var tabImgArray:Array;
		private var tab1ImgFile:URLRequest;
		private var tab2ImgFile:URLRequest;
		private var tab3ImgFile:URLRequest;
		private var tab4ImgFile:URLRequest;
		private var tab5ImgFile:URLRequest;
		private var tab6ImgFile:URLRequest;
		private var tabImgFileArray:Array;
		
		private var tab1Str:String;
		private var tab2Str:String;
		private var tab3Str:String;
		private var tab4Str:String;
		private var tab5Str:String;
		private var tab6Str:String;
		private var tabStrArray:Array;
		
		public var v1:Video = new Video();
		public var nc1:NetConnection = new NetConnection();
		public var ns1:NetStream;
		private var vU1:String = "";
		
		public var v2:Video = new Video();
		public var nc2:NetConnection = new NetConnection();
		public var ns2:NetStream;
		private var vU2:String = "";
		
		public var v3:Video = new Video();
		public var nc3:NetConnection = new NetConnection();
		public var ns3:NetStream;
		private var vU3:String = "";
		
		public var v4:Video = new Video();
		public var nc4:NetConnection = new NetConnection();
		public var ns4:NetStream;
		private var vU4:String = "";
		
		public var v5:Video = new Video();
		public var nc5:NetConnection = new NetConnection();
		public var ns5:NetStream;
		private var vU5:String = "";
		
		public var v6:Video = new Video();
		public var nc6:NetConnection = new NetConnection();
		public var ns6:NetStream;
		private var vU6:String = "";
		
		private var nsArray:Array = new Array();
		
		private var bgWidth:Number = 488;
		private var bgHeight:Number = 678;
		
		private var mortgageRateXML:XML;
		private var mortgageRateLoader:URLLoader;
		private var whichMRT:Number;
		private var mortgageRateArray:Array;
		
		private var MRTitle:TLFTextField;
		private var MRTitleFormat:TextLayoutFormat = new TextLayoutFormat();
		private var MRTitleTextFlow:TextFlow;
		
		private var MRGroup:TLFTextField;
		private var MRGroupFormat:TextLayoutFormat = new TextLayoutFormat();
		private var MRGroupTextFlow:TextFlow;
		
		private var MRProduct:TLFTextField;
		private var MRProductFormat:TextLayoutFormat = new TextLayoutFormat();
		private var MRProductTextFlow:TextFlow;
		
		private var MRHeading:TLFTextField;
		private var MRHeadingFormat:TextLayoutFormat = new TextLayoutFormat();
		private var MRHeadingTextFlow:TextFlow;
		
		private var MRRate:TLFTextField;
		private var MRRateFormat:TextLayoutFormat = new TextLayoutFormat();
		private var MRRateTextFlow:TextFlow;
		
		private var MRTextArray:Array = new Array();
		private var mortgageRateTable:MovieClip;
		
		private var savedWidth:Number;
		private var savedHeight:Number;
		
		public function Popup(myOwner:Document, w:Number) 
		{
			owner = myOwner;
			
			whichMenu = w;
			arrNum = whichMenu - 1;
			
			sf = new DropShadowFilter(shadowDistance, shadowAngle, shadowColor, shadowAlpha, shadowBlurX, shadowBlurY, shadowStrength, shadowQuality, shadowInner, shadowKnockout, shadowHideObject);
			
			this.addEventListener(TransformGestureEvent.GESTURE_PAN, handleDrag);
			this.addEventListener(TransformGestureEvent.GESTURE_ZOOM, handleScale);
			//this.addEventListener(TransformGestureEvent.GESTURE_ROTATE, handleRotate);
			this.addEventListener(TuioTouchEvent.TOUCH_DOWN, handleDown);
			
			bgFile = new URLRequest(owner.loadXML.fileArray[arrNum]);
			bgLoader.load(bgFile);
			bgLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, bgLoaded);
			addChild(bgLoader);
			
			addChild(tab1);
			addChild(tab2);
			addChild(tab3);
			addChild(tab4);
			addChild(tab5);
			addChild(tab6);
			tab2.visible = false;
			tab3.visible = false;
			tab4.visible = false;
			tab5.visible = false;
			tab6.visible = false;
			
			tabArray = new Array(tab1, tab2, tab3, tab4, tab5, tab6);
			
			close_btn = new Close();
			addChild(close_btn);
			close_btn.x = 462;
			close_btn.y = 0;
			
			b1 = new Tab();
			addChild(b1);
			b1.x = 0;
			b1.y = 604;
			
			b2 = new Tab();
			addChild(b2);
			b2.x = 163;
			b2.y = b1.y;
			
			b3 = new Tab();
			addChild(b3);
			b3.x = 327;
			b3.y = b1.y;
			
			b4 = new Tab();
			addChild(b4);
			b4.x = b1.x;
			b4.y = 642;
			
			b5 = new Tab();
			addChild(b5);
			b5.x = b2.x;
			b5.y = b4.y;
			
			b6 = new Tab();
			addChild(b6);
			b6.x = b3.x;
			b6.y = b4.y;
			
			//introText.visible = true;
			b1.gotoAndStop(whichMenu);
			b2.gotoAndStop(whichMenu);
			b3.gotoAndStop(whichMenu);
			b4.gotoAndStop(whichMenu);
			b5.gotoAndStop(whichMenu);
			b6.gotoAndStop(whichMenu);
			
			reloadAll();
			
			//make this smarter to react to the xml
			/*if (whichMenu == 5)
			{
				b2.y = b3.y = b1.y = b4.y;
				b4.visible = false;
				b5.visible = false;
				b6.visible = false;
				t4.visible = false;
				t5.visible = false;
				t6.visible = false;
			}*/
		}
		
		public function unloadAll():void
		{
			removeListeners();
			
			tArray = new Array();
			removeChild(t1);
			removeChild(t2);
			removeChild(t3);
			removeChild(t4);
			removeChild(t5);
			removeChild(t6);
				
			tab1.removeChild(tab1Img);
			tab1.removeChild(text1);
					
			tab1Active = false;
			b1.alpha = .3;
			tab2Active = false;
			b2.alpha = .3;
			tab3Active = false;
			b3.alpha = .3;
			tab4Active = false;
			b4.alpha = .3;
			tab5Active = false;
			b5.alpha = .3;
			tab6Active = false;
			b6.alpha = .3;
				
			bArray = new Array();
			tabActiveArray = new Array();
				
				
			tab1Video = false;
			tab2Video = false;
			tab3Video = false;
			tab4Video = false;
			tab5Video = false;
			tab6Video = false;
				
			tabVideoArray = new Array();
				
			if (tab1.contains(tab1Img) && tab1Img != null)
			{
				tab1.removeChild(tab1Img);
				tab1Image = false;
			}
			if (tab2.contains(tab2Img) && tab2Img != null)
			{
				tab2.removeChild(tab2Img);
				tab2Image = false;
			}
			if (tab3.contains(tab3Img) && tab3Img != null)
			{
				tab3.removeChild(tab3Img);
				tab3Image = false;
			}
			if (tab4.contains(tab4Img) && tab4Img != null)
			{
				tab4.removeChild(tab4Img);
				tab4Image = false;
			}
			if (tab5.contains(tab5Img) && tab5Img != null)
			{
				tab5.removeChild(tab5Img);
				tab5Image = false;
			}
			if (tab6.contains(tab6Img) && tab6Img != null)
			{
				tab6.removeChild(tab6Img);
				tab6Image = false;
			}
			
			tabImgArray = new Array();
				
			//tab1.removeChild(text1);
			//tab2.removeChild(text2);
			//tab3.removeChild(text3);
			//tab4.removeChild(text4);
			//tab5.removeChild(text5);
			//tab6.removeChild(text6);
					
			textArray = new Array();
			
			
			if (arrNum == 2)
			{
				for each (var t:TLFTextField in MRTextArray)
				{
					mortgageRateTable.removeChild(t);
					t = null;
				}
				tabArray[whichMRT].removeChild(mortgageRateTable)
				mortgageRateTable = null;
				MRTextArray = new Array();
			}
			
			reloadAll();
		}
		
		private function reloadAll():void
		{
			trace("reloadAll");
			
			this.scaleX = 1;
			this.scaleY = 1;
			
			setTextFormat(textFormat, 12.5, 0x000000, 1, 20, true);
			
			//get play info separately
			if (arrNum == 4)
			{
				
				setTextFormat(tFormat, 10, 0xffffff, 2, 0, true, 2);
				setText(t1, b1, 0xffffff, owner.loadXML.popupArray[arrNum][0], tFormat, tTextFlow);
				setText(t2, b2, 0xffffff, owner.loadXML.popupArray[arrNum][1], tFormat, tTextFlow);
				setText(t3, b3, 0xffffff, owner.loadXML.popupArray[arrNum][2], tFormat, tTextFlow);
				setText(t4, b4, 0xffffff, owner.loadXML.popupArray[arrNum][3], tFormat, tTextFlow);
				setText(t5, b5, 0xffffff, owner.loadXML.popupArray[arrNum][4], tFormat, tTextFlow);
				setText(t6, b6, 0xffffff, owner.loadXML.popupArray[arrNum][5], tFormat, tTextFlow);
				addChild(t1);
				addChild(t2);
				addChild(t3);
				addChild(t4);
				addChild(t5);
				addChild(t6);
				
				tArray = new Array(t1, t2, t3, t4, t5, t6);
				
				tab1Active = false;
					b1.alpha = .3;
				tab2Active = false;
					b2.alpha = .3;
				tab3Active = false;
					b3.alpha = .3;
				tab4Active = false;
					b4.alpha = .3;
				tab5Active = false;
					b5.alpha = .3;
				tab6Active = false;
					b6.alpha = .3;
					
					if (owner.loadXML.playArray[0] != "")
				{
					tab1Active = true;
					b1.alpha = 1;
				}
				if (owner.loadXML.playArray.length > 1)
				{
					tab2Active = true;
					b2.alpha = 1;
				}
				if (owner.loadXML.playArray.length > 2)
				{
					tab2Active = true;
					b2.alpha = 1;
					tab3Active = true;
					b3.alpha = 1;
				}
				if (owner.loadXML.playArray.length > 3)
				{
					tab2Active = true;
					b2.alpha = 1;
					tab3Active = true;
					b3.alpha = 1;
					tab4Active = true;
					b4.alpha = 1;
				}
				if (owner.loadXML.playArray.length > 4)
				{
					tab2Active = true;
					b2.alpha = 1;
					tab3Active = true;
					b3.alpha = 1;
					tab4Active = true;
					b4.alpha = 1;
					tab5Active = true;
					b5.alpha = 1;
				}
				if (owner.loadXML.playArray.length > 5)
				{
					tab2Active = true;
					b2.alpha = 1;
					tab3Active = true;
					b3.alpha = 1;
					tab4Active = true;
					b4.alpha = 1;
					tab5Active = true;
					b5.alpha = 1;
					tab6Active = true;
					b6.alpha = 1;
				}
				
				
				bArray = new Array(b1, b2, b3, b4, b5, b6);
				tabActiveArray = new Array(tab1Active, tab2Active, tab3Active, tab4Active, tab5Active, tab6Active);
				
				if (owner.loadXML.playImage != "")
				{
					trace(owner.loadXML.playImage);
					tab1ImgFile = new URLRequest(owner.loadXML.playImage);
					tab1Img.load(tab1ImgFile);
					tab1Img.contentLoaderInfo.addEventListener(Event.COMPLETE, Image1Loaded);
					tab1.addChild(tab1Img);
					tab1Image = true;
					tab1Img.y = 100;
				}
				
				tab1Str = owner.loadXML.playText;
				
				if (tab1Str != "")
				{
					tab1Text = true;
					setText(text1, null, 0x000000, tab1Str, textFormat, textTextFlow);
					tab1.addChild(text1);
					text1.width = 400;
					text1.height = 500;
					text1.x = 150;
					text1.y = 300;
				}
			}
			if (arrNum < 4)
			{
				/*trace("###############################");
				trace(owner.loadXML.popupArray[arrNum]);
				trace("###############################");
				
				trace("%%%%%%%%%%%%%%%%%%%%%%%%%%");
				trace(owner.loadXML.popupArray[arrNum][0]);
				trace("%%%%%%%%%%%%%%%%%%%%%%%%%%");
				
				
				
				trace(arrNum);*/
				
				
				tab1Active = false;
					b1.alpha = .3;
				tab2Active = false;
					b2.alpha = .3;
				tab3Active = false;
					b3.alpha = .3;
				tab4Active = false;
					b4.alpha = .3;
				tab5Active = false;
					b5.alpha = .3;
				tab6Active = false;
					b6.alpha = .3;
					
					
				if (owner.loadXML.popupArray[arrNum][0][7] == "false" && owner.loadXML.popupArray[arrNum][0][1] != "")
				{
					tab1Active = true;
					b1.alpha = 1;
				}
				if (owner.loadXML.popupArray[arrNum][1][7] == "false" && owner.loadXML.popupArray[arrNum][1][1] != "")
				{
					tab2Active = true;
					b2.alpha = 1;
				}
				if (owner.loadXML.popupArray[arrNum][2][7] == "false" && owner.loadXML.popupArray[arrNum][2][1] != "")
				{
					tab3Active = true;
					b3.alpha = 1;
				}
				if (owner.loadXML.popupArray[arrNum][3][7] == "false" && owner.loadXML.popupArray[arrNum][3][1] != "")
				{
					tab4Active = true;
					b4.alpha = 1;
				}
				if (owner.loadXML.popupArray[arrNum][4][7] == "false" && owner.loadXML.popupArray[arrNum][4][1] != "")
				{
					tab5Active = true;
					b5.alpha = 1;
				}
				if (owner.loadXML.popupArray[arrNum][5][7] == "false" && owner.loadXML.popupArray[arrNum][5][1] != "")
				{
					tab6Active = true;
					b6.alpha = 1;
				}
				trace(arrNum + ", " + owner.loadXML.popupArray[arrNum][5][7]);
				
				bArray = new Array(b1, b2, b3, b4, b5, b6);
				tabActiveArray = new Array(tab1Active, tab2Active, tab3Active, tab4Active, tab5Active, tab6Active);
				
				trace("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ " + owner.loadXML.popupArray[arrNum][0][1]);
				setTextFormat(tFormat, 10, 0xffffff, 2, 0, true, 2);
				setText(t1, b1, 0xffffff, owner.loadXML.popupArray[arrNum][0][1], tFormat, tTextFlow);
				setText(t2, b2, 0xffffff, owner.loadXML.popupArray[arrNum][1][1], tFormat, tTextFlow);
				setText(t3, b3, 0xffffff, owner.loadXML.popupArray[arrNum][2][1], tFormat, tTextFlow);
				setText(t4, b4, 0xffffff, owner.loadXML.popupArray[arrNum][3][1], tFormat, tTextFlow);
				setText(t5, b5, 0xffffff, owner.loadXML.popupArray[arrNum][4][1], tFormat, tTextFlow);
				setText(t6, b6, 0xffffff, owner.loadXML.popupArray[arrNum][5][1], tFormat, tTextFlow);
				addChild(t1);
				addChild(t2);
				addChild(t3);
				addChild(t4);
				addChild(t5);
				addChild(t6);
				
				tArray = new Array(t1, t2, t3, t4, t5, t6);
				
				if (owner.loadXML.popupArray[arrNum][0][5] != "")
				{
					vU1 = owner.loadXML.popupArray[arrNum][0][5];
					startVideo(nc1, ns1, v1, vU1, tab1);
					tab1Video = true;
				}
				else
				{
					startVideo(nc1, ns1, v1, vU1, tab1);
				}
				if (owner.loadXML.popupArray[arrNum][1][5] != "")
				{
					vU2 = owner.loadXML.popupArray[arrNum][1][5];
					startVideo(nc2, ns2, v2, vU2, tab2);
					tab2Video = true;
				}
				else
				{
					startVideo(nc2, ns2, v2, vU2, tab2);
				}
				if (owner.loadXML.popupArray[arrNum][2][5] != "")
				{
					vU3 = owner.loadXML.popupArray[arrNum][2][5];
					startVideo(nc3, ns3, v3, vU3, tab3);
					tab3Video = true;
				}
				else
				{
					startVideo(nc3, ns3, v3, vU3, tab3);
				}
				if (owner.loadXML.popupArray[arrNum][3][5] != "")
				{
					vU4 = owner.loadXML.popupArray[arrNum][3][5];
					startVideo(nc4, ns4, v4, vU4, tab4);
					tab4Video = true;
				}
				else
				{
					startVideo(nc4, ns4, v4, vU4, tab4);
				}
				if (owner.loadXML.popupArray[arrNum][4][5] != "")
				{
					vU5 = owner.loadXML.popupArray[arrNum][4][5];
					startVideo(nc5, ns5, v5, vU5, tab5);
					tab5Video = true;
				}
				else
				{
					startVideo(nc5, ns5, v5, vU5, tab5);
				}
				if (owner.loadXML.popupArray[arrNum][5][5] != "")
				{
					vU6 = owner.loadXML.popupArray[arrNum][5][5];
					startVideo(nc6, ns6, v6, vU6, tab6);
					tab6Video = true;
				}
				else
				{
					startVideo(nc6, ns6, v6, vU6, tab6);
				}
				tabVideoArray = new Array(tab1Video, tab2Video, tab3Video, tab4Video, tab5Video, tab6Video);
				
				if (owner.loadXML.popupArray[arrNum][0][3] != "")
				{
					tab1ImgFile = new URLRequest(owner.loadXML.popupArray[arrNum][0][3]);
					tab1Img.load(tab1ImgFile);
					tab1Img.contentLoaderInfo.addEventListener(Event.COMPLETE, Image1Loaded);
					tab1.addChild(tab1Img);
					tab1Image = true;
					tab1Img.y = 100;
				}
				if (owner.loadXML.popupArray[arrNum][1][3] != "")
				{
					tab2ImgFile = new URLRequest(owner.loadXML.popupArray[arrNum][1][3]);
					tab2Img.load(tab2ImgFile);
					tab2Img.contentLoaderInfo.addEventListener(Event.COMPLETE, Image2Loaded);
					tab2.addChild(tab2Img);
					tab2Image = true;
					tab2Img.y = 100;
					tab2Img.x = (488 - tab2Img.width)/2 - 244;
				}
				if (owner.loadXML.popupArray[arrNum][2][3] != "")
				{
					tab3ImgFile = new URLRequest(owner.loadXML.popupArray[arrNum][2][3]);
					tab3Img.load(tab3ImgFile);
					tab3Img.contentLoaderInfo.addEventListener(Event.COMPLETE, Image3Loaded);
					tab3.addChild(tab3Img);
					tab3Image = true;
					tab3Img.y = 100;
					tab3Img.x = (488 - tab3Img.width)/2 - 244;
				}
				if (owner.loadXML.popupArray[arrNum][3][3] != "")
				{
					tab4ImgFile = new URLRequest(owner.loadXML.popupArray[arrNum][3][3]);
					tab4Img.load(tab4ImgFile);
					tab4Img.contentLoaderInfo.addEventListener(Event.COMPLETE, Image4Loaded);
					tab4.addChild(tab4Img);
					tab4Image = true;
					tab4Img.y = 100;
					tab4Img.x = (488 - tab4Img.width)/2 - 244;
				}
				if (owner.loadXML.popupArray[arrNum][4][3] != "")
				{
					tab5ImgFile = new URLRequest(owner.loadXML.popupArray[arrNum][4][3]);
					tab5Img.load(tab5ImgFile);
					tab5Img.contentLoaderInfo.addEventListener(Event.COMPLETE, Image5Loaded);
					tab5.addChild(tab5Img);
					tab5Image = true;
					tab5Img.y = 100;
					tab5Img.x = (488 - tab5Img.width)/2 - 244;
					trace(tab5Img.x);
				}
				if (owner.loadXML.popupArray[arrNum][5][3] != "")
				{
					tab6ImgFile = new URLRequest(owner.loadXML.popupArray[arrNum][5][3]);
					tab6Img.load(tab6ImgFile);
					tab6Img.contentLoaderInfo.addEventListener(Event.COMPLETE, Image6Loaded);
					tab6.addChild(tab6Img);
					tab6Image = true;
					tab6Img.y = 100;
					tab6Img.x = (488 - tab6Img.width)/2 - 244;
				}
				
				tabImgArray = new Array(tab1Img, tab2Img, tab3Img, tab4Img, tab5Img, tab6Img);
				
				tab1Str = owner.loadXML.popupArray[arrNum][0][2];
				tab2Str = owner.loadXML.popupArray[arrNum][1][2];
				tab3Str = owner.loadXML.popupArray[arrNum][2][2];
				tab4Str = owner.loadXML.popupArray[arrNum][3][2];
				tab5Str = owner.loadXML.popupArray[arrNum][4][2];
				tab6Str = owner.loadXML.popupArray[arrNum][5][2];
				if (tab1Str != "")
				{
					tab1Text = true;
					setText(text1, null, 0x000000, tab1Str, textFormat, textTextFlow);
					tab1.addChild(text1);
					text1.width = 400;
					text1.height = 500;
					text1.x = 150;
					text1.y = 300;
				}
				if (tab2Str != "")
				{
					tab2Text = true;
					setText(text2, null, 0x000000, tab1Str, textFormat, textTextFlow);
					tab2.addChild(text2);
					text2.width = 200;
					text2.height = 500;
					text2.x = 200;
					text2.y = 400;
				}
				if (tab3Str != "")
				{
					tab3Text = true;
					setText(text3, null, 0x000000, tab1Str, textFormat, textTextFlow);
					tab3.addChild(text3);
					text3.width = 200;
					text3.height = 500;
					text3.x = 200;
					text3.y = 400;
				}
				if (tab4Str != "")
				{
					tab4Text = true;
					setText(text4, null, 0x000000, tab1Str, textFormat, textTextFlow);
					tab4.addChild(text4);
					text4.width = 200;
					text4.height = 500;
					text4.x = 200;
					text4.y = 400;
				}
				if (tab5Str != "")
				{
					tab5Text = true;
					setText(text5, null, 0x000000, tab1Str, textFormat, textTextFlow);
					tab5.addChild(text5);
					text5.width = 200;
					text5.height = 500;
					text5.x = 200;
					text5.y = 400;
				}
				if (tab6Str != "")
				{
					tab6Text = true;
					setText(text6, null, 0x000000, tab1Str, textFormat, textTextFlow);
					tab6.addChild(text6);
					text6.width = 200;
					text6.height = 500;
					text6.x = 200;
					text6.y = 400;
				}
				
				textArray = new Array(text1, text2, text3, text4, text5, text6);
				
				//t1.htmlText = owner.loadXML.popupArray[arrNum][0][1];
				/*trace(owner.loadXML.popupArray[arrNum][0][1]);
				trace(t1.text);
				//t2.htmlText = owner.loadXML.popupArray[arrNum][1][1];
				trace(owner.loadXML.popupArray[arrNum][1][1]);
				trace(t2.text);
				//t3.htmlText = owner.loadXML.popupArray[arrNum][2][1];
				trace(owner.loadXML.popupArray[arrNum][2][1]);
				trace(t3.text);
				//t4.htmlText = owner.loadXML.popupArray[arrNum][3][1];
				trace(owner.loadXML.popupArray[arrNum][3][1]);
				trace(t4.text);
				//t5.htmlText = owner.loadXML.popupArray[arrNum][4][1];
				trace(owner.loadXML.popupArray[arrNum][4][1]);
				trace(t5.text);
				//t6.htmlText = owner.loadXML.popupArray[arrNum][5][1];
				trace(owner.loadXML.popupArray[arrNum][5][1]);
				trace(t6.text);*/
				
				//t1.htmlText = "BLAH";
				
				if (arrNum == 2)
				{
					
					for each (var ta:TLFTextField in tArray)
					{
						if (ta.text == owner.loadXML.mortgageRateTableString)
						{
							whichMRT = tArray.indexOf(ta);
							trace("whichMRT = " + whichMRT);
							mortgageRateTable = new MovieClip();
							
						}
					}
					mortgageRateLoader = new URLLoader();
					mortgageRateLoader.load(new URLRequest(owner.loadXML.mortgageRateURL));
					mortgageRateLoader.addEventListener(Event.COMPLETE, processMortgageRateXML);
					
				}
			}
				
			addListeners();
		}
		
		private function processMortgageRateXML(e:Event):void
		{
			trace("processMortgageRateXML");
			setTextFormat(MRTitleFormat, 24, 0x003466, 2, 0, false, 2);
			setTextFormat(MRGroupFormat, 20, 0x000000, 2, 0, false, 2);
			setTextFormat(MRProductFormat, 18, 0x000000, 1, 0, true, 2);
			setTextFormat(MRHeadingFormat, 16, 0xffffff, 1, 0, false, 2);
			setTextFormat(MRRateFormat, 16, 0x000000, 1, 0, true, 2);
			
			
			MRTitle = new TLFTextField();
			var tempString:String;
			setText(MRTitle, null, 0x003466, owner.loadXML.mortgageRateToday, MRTitleFormat, MRTitleTextFlow);
			MRTitle.width = 448;
			MRTitle.height = 400;
			MRTitle.y = 100;
			MRTitle.x = 20;
			tabArray[whichMRT].addChild(MRTitle);
			trace("()()()()()()()()()()(() " + owner.loadXML.mortgageRateToday);
			//MRTextArray.push(MRTitle);
			
			mortgageRateXML = new XML(e.target.data);
			mortgageRateArray = new Array();
			
			var offset:Number = 50;
			for (var n:int = 0; n < mortgageRateXML.GROUPS.GROUP.length(); n++)
			{
				trace("mortgageRateXML.GROUPS.GROUP[n].NAME = " + mortgageRateXML.GROUPS.GROUP[n].NAME);
				MRGroup = new TLFTextField();
				setText(MRGroup, null, 0x000000, mortgageRateXML.GROUPS.GROUP[n].NAME, MRGroupFormat, MRGroupTextFlow);
				MRGroup.width = 448;
				MRGroup.height = 40;
				MRGroup.x = 20;
				MRGroup.y = MRTitle.y + offset;
				mortgageRateTable.addChild(MRGroup);
				MRTextArray.push(MRGroup);
				offset += 35;
				//cycles through the two groups
				var groupArray = new Array(mortgageRateXML.GROUPS.GROUP[n].NAME)
				var inc:Number = 0;
				for (var m:int = 0; m < mortgageRateXML.GROUPS.GROUP[n].PRODUCT.length(); m++)
				{
					
					//cycles through each group
					var prodArray = new Array(mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].DESCR, mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].RATE,
											  mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].APR, mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].POINTS)
					groupArray.push(prodArray);
					
					if (inc == 0)
					{
						MRProduct = new TLFTextField();
						setText(MRProduct, null, 0x000000, mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].DESCR, MRProductFormat, MRProductTextFlow);
						MRProduct.width = 448;
						MRProduct.height = 40;
						MRProduct.x = 20;
						MRProduct.y = MRTitle.y + offset;
						mortgageRateTable.addChild(MRProduct);
						offset += 20;
						MRTextArray.push(MRProduct);	
						
						MRHeading = new TLFTextField();
						setText(MRHeading, null, 0xffffff, owner.loadXML.mortgageRateRate, MRHeadingFormat, MRHeadingTextFlow);
						MRHeading.width = 248;
						MRHeading.height = 20;
						MRHeading.x = 20;
						MRHeading.y = MRTitle.y + offset;
						MRHeading.background = true;
						MRHeading.backgroundColor = 0x003466;
						mortgageRateTable.addChild(MRHeading);
						MRTextArray.push(MRHeading);	
						
						MRHeading = new TLFTextField();
						setText(MRHeading, null, 0xffffff, owner.loadXML.mortgageRateAPR, MRHeadingFormat, MRHeadingTextFlow);
						MRHeading.width = 248;
						MRHeading.height = 20;
						MRHeading.x = 180;
						MRHeading.y = MRTitle.y + offset;
						MRHeading.background = true;
						MRHeading.backgroundColor = 0x003466;
						mortgageRateTable.addChild(MRHeading);
						MRTextArray.push(MRHeading);	
						
						MRHeading = new TLFTextField();
						setText(MRHeading, null, 0xffffff, owner.loadXML.mortgageRatePoints, MRHeadingFormat, MRHeadingTextFlow);
						MRHeading.width = 128;
						MRHeading.height = 20;
						MRHeading.x = 340;
						MRHeading.y = MRTitle.y + offset;
						MRHeading.background = true;
						MRHeading.backgroundColor = 0x003466;
						mortgageRateTable.addChild(MRHeading);
						MRTextArray.push(MRHeading);	
						offset += 20;
						
						tempString = String(Number(mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].RATE).toFixed(3)) + "%";
						MRRate = new TLFTextField();
						setText(MRRate, null, 0x000000, tempString, MRRateFormat, MRRateTextFlow);
						MRRate.width = 248;
						MRRate.height = 40;
						MRRate.x = 20;
						MRRate.y = MRTitle.y + offset;
						mortgageRateTable.addChild(MRRate);
						MRTextArray.push(MRRate);	
						
						tempString = String(Number(mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].APR).toFixed(3)) + "%";
						MRRate = new TLFTextField();
						setText(MRRate, null, 0x000000, tempString, MRRateFormat, MRRateTextFlow);
						MRRate.width = 248;
						MRRate.height = 40;
						MRRate.x = 180;
						MRRate.y = MRTitle.y + offset;
						mortgageRateTable.addChild(MRRate);
						MRTextArray.push(MRRate);	
						
						tempString = String(Number(mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].POINTS).toFixed(3)) + "%";
						MRRate = new TLFTextField();
						setText(MRRate, null, 0x000000, tempString, MRRateFormat, MRRateTextFlow);
						MRRate.width = 120;
						MRRate.height = 40;
						MRRate.x = 340;
						MRRate.y = MRTitle.y + offset;
						mortgageRateTable.addChild(MRRate);
						MRTextArray.push(MRRate);	
					}
					if (inc == 1)
					{
						MRRate = new TLFTextField();
						tempString = String(Number(mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].RATE).toFixed(3)) + "%";
						setText(MRRate, null, 0x000000, tempString, MRRateFormat, MRRateTextFlow);
						MRRate.width = 248;
						MRRate.height = 40;
						MRRate.x = 20;
						MRRate.y = MRTitle.y + offset;
						mortgageRateTable.addChild(MRRate);
						MRTextArray.push(MRRate);	
						
						MRRate = new TLFTextField();
						tempString = String(Number(mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].APR).toFixed(3)) + "%";
						setText(MRRate, null, 0x000000, tempString, MRRateFormat, MRRateTextFlow);
						MRRate.width = 248;
						MRRate.height = 40;
						MRRate.x = 180;
						MRRate.y = MRTitle.y + offset;
						mortgageRateTable.addChild(MRRate);
						MRTextArray.push(MRRate);	
						
						MRRate = new TLFTextField();
						tempString = String(Number(mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].POINTS).toFixed(3)) + "%";
						setText(MRRate, null, 0x000000, tempString, MRRateFormat, MRRateTextFlow);
						MRRate.width = 128;
						MRRate.height = 40;
						MRRate.x = 340;
						MRRate.y = MRTitle.y + offset;
						mortgageRateTable.addChild(MRRate);
						MRTextArray.push(MRRate);	
					}
					if (m < mortgageRateXML.GROUPS.GROUP[n].PRODUCT.length() - 1)
					{
						if (mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m].DESCR == mortgageRateXML.GROUPS.GROUP[n].PRODUCT[m + 1].DESCR)
						{
							inc++;
							offset += 20;
						}
						else
						{
							inc = 0;
							offset += 35;
						}	
					}
					
				}
				mortgageRateArray.push(groupArray);
			
					
			offset += 50;
			}
			tabArray[whichMRT].addChild(mortgageRateTable);
			tabArray[whichMRT].visible = true;
			//addChild(mortgageRateTable);
			trace(tabArray[whichMRT].contains(mortgageRateTable));
			trace(this.contains(tabArray[whichMRT]));
			trace(mortgageRateArray);
			trace("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			trace(whichMRT);
			trace("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		}
		
		private function bgLoaded(e:Event):void
		{
			bgWidth = bgLoader.width;
			bgHeight = bgLoader.height;
			//bgLoader.x -= bgWidth/2;
			//bgLoader.y -= bgHeight/2;
		}
		
		private function Image1Loaded(e:Event):void
		{
			var tempScale:Number = 1;
			while(tab1Img.width > bgWidth || tab1Img.height > bgHeight - 200)
			{
				tab1Img.scaleX = tempScale;
				tab1Img.scaleY = tempScale;
				tempScale -= .005;
			}
			tab1Img.x = (488 - tab1Img.width)/2; // - bgWidth/2;
		}
		
		private function Image2Loaded(e:Event):void
		{
			var tempScale:Number = 1;
			while(tab2Img.width > bgWidth || tab2Img.height > bgHeight - 200)
			{
				tab2Img.scaleX = tempScale;
				tab2Img.scaleY = tempScale;
				tempScale -= .005;
			}
			tab2Img.x = (488 - tab2Img.width)/2;
		}
		
		private function Image3Loaded(e:Event):void
		{
			var tempScale:Number = 1;
			while(tab3Img.width > bgWidth || tab3Img.height > bgHeight - 200)
			{
				tab3Img.scaleX = tempScale;
				tab3Img.scaleY = tempScale;
				tempScale -= .005;
			}
			tab3Img.x = (488 - tab3Img.width)/2;
		}
		
		private function Image4Loaded(e:Event):void
		{
			var tempScale:Number = 1;
			while(tab4Img.width > bgWidth || tab4Img.height > bgHeight - 200)
			{
				tab4Img.scaleX = tempScale;
				tab4Img.scaleY = tempScale;
				tempScale -= .005;
			}
			tab4Img.x = (488 - tab4Img.width)/2;
		}
		
		private function Image5Loaded(e:Event):void
		{
			var tempScale:Number = 1;
			while(tab5Img.width > bgWidth || tab5Img.height > bgHeight - 200)
			{
				tab5Img.scaleX = tempScale;
				tab5Img.scaleY = tempScale;
				tempScale -= .005;
			}
			tab5Img.x = (488 - tab5Img.width)/2;
		}
		
		private function Image6Loaded(e:Event):void
		{
			var tempScale:Number = 1;
			while(tab6Img.width > bgWidth || tab6Img.height > bgHeight - 200)
			{
				tab6Img.scaleX = tempScale;
				tab6Img.scaleY = tempScale;
				tempScale -= .005;
			}
			tab6Img.x = (488 - tab6Img.width)/2;
		}
		
		private function startVideo(nc:NetConnection, ns:NetStream, video:Video, videoURL:String, t:MovieClip, w:Number = 488, h:Number = 274)
		{
			
			//addText("***************************** Video Added");
			
			//trace("adding video");
			//Add the video to the stage
			nc.connect(null);
			var ns:NetStream = new NetStream(nc);
			//var customClient:Object = new Object();
			
			if (videoURL != "")
			{
				ns.client = t;
				ns.addEventListener(NetStatusEvent.NET_STATUS, ns_onPlayStatus);
				t.addChild(video);
				video.attachNetStream(ns);
				ns.play(videoURL);
					
				video.smoothing = true;
				video.width = w;
				video.height = h;
				video.x = 0;
				video.y = 100;
				
				ns.seek(0);
				ns.pause();
			}
			
			nsArray.push(ns);
		}
		
		//--------------------------------------
		//  NetStream Event Handlers
		//--------------------------------------
 
		//the NetStream will look for these two methods
		//on whatever object is identified as the NetStream's client
		//if they do not exist, you will receive a run time error
		//when the NetStream encounters metadata or cuepoints in the video
		public function onMetaData(info:Object):void
		{ 
		
		}
		public function onCuePoint(info:Object):void
		{
			
		}
		
		private function ns_onMetaData(_data:Object)
		{
			
		}	
		
		private function ns_onCuePoint(_data:Object)
		{
			
		}
		
		private function onPlayStatus(info:Object)
		{
			 
		}
		
		private function ns_onPlayStatus(e:NetStatusEvent)
		{
			var tempString:String = e.info.code;
			/*if (e.info.code == "NetStream.Buffer.Empty") 
			{
				//addText(tempString);
				e.target.seek(0);
			} */
		}
		
		public function addListeners():void
		{
			if (tab2Active == true)
			{
				b2.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (tab3Active == true)
			{
				b3.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (tab4Active == true)
			{
				b4.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (tab5Active == true)
			{
				b5.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (tab6Active == true)
			{
				b6.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			
			/*if (whichMenu == 1)
			{
				b6.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (whichMenu == 2)
			{
				b2.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (whichMenu == 3)
			{
				b2.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (whichMenu == 4)
			{
				b5.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
				b6.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (whichMenu == 5)
			{
				//drawApp = new drawingApp(this);
				//addChild(drawApp);
				//drawApp.x = 24;
				//drawApp.y = 50;
				b2.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
				b3.addEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
				b2.y = b3.y = b1.y = b4.y;
			}*/
		}
		
		public function removeListeners():void
		{
			if (tab1Active == true)
			{
				b1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (tab2Active == true)
			{
				b2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (tab3Active == true)
			{
				b3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (tab4Active == true)
			{
				b4.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (tab5Active == true)
			{
				b5.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (tab6Active == true)
			{
				b6.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			
			
			/*if (whichMenu == 1)
			{
				b6.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (whichMenu == 2)
			{
				b2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (whichMenu == 3)
			{
				b2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			}
			if (whichMenu == 4)
			{
				b5.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
				b6.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeM90e);
			}
			if (whichMenu == 5)
			{
				//drawApp = new drawingApp(this);
				//addChild(drawApp);
				//drawApp.x = 24;
				//drawApp.y = 50;
				b2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
				b3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
				b2.y = b3.y = b1.y = b4.y;
			}*/
		}
		
		public function startTimeout():void
		{
			//owner.addText("popup startTimeout " + whichPopup);
			timeoutTimer = new Timer(120000, 1);
			timeoutTimer.stop();
			timeoutTimer.start();
			timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			addEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimeout);
		}
		
		private function resetTimeout(e:TuioTouchEvent):void
		{
			if (whichMenu != 5)
			{
				//owner.addText("resetting timer " + whichPopup);
				timeoutTimer.stop();
				timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
				timeoutTimer = new Timer(120000, 1);
				timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timeout);
				timeoutTimer.reset();
				timeoutTimer.start();
			}
			if (whichMenu == 5)
			{
				if (whichButtonOpen == 1)
				{
					//owner.addText("resetting timer " + whichPopup);
					timeoutTimer.stop();
					timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
					timeoutTimer = new Timer(120000, 1);
					timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timeout);
					timeoutTimer.reset();
					timeoutTimer.start();
				}
			}
		}
		
		private function timeout(e:TimerEvent):void
		{
			owner.addText("popup timeout " + whichPopup);
			timeoutTimer.stop();
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			owner.closePopup(whichPopup);
		}
		
		public function stopTimeout():void
		{
			//owner.addText("popup stopTimeout " + whichPopup);
			if (timeoutTimer != null)
			{
				timeoutTimer.stop();
				timeoutTimer.reset();
				timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
				
				removeEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimeout);
			}
		}
		
		public function restartTimeout():void
		{
			//owner.addText("popup restartTimeout " + whichPopup);
			timeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			timeoutTimer.stop();
			timeoutTimer.reset();
			timeoutTimer.start();
			timeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timeout);
		}
		
		private function changeMe(e:TuioTouchEvent):void
		{
			owner.addText("changeMe " + whichButtonOpen);
			//e.target.removeEventListener(TuioTouchEvent.TOUCH_DOWN, changeMe);
			//e.target.visible = false;
			
			if (e.target == b1)
			{
				whichButtonOpen = 1;
			}
			
			if (e.target == b2)
			{
				whichButtonOpen = 2;
			}
			if (e.target == b3)
			{
				whichButtonOpen = 3;
			}
			if (e.target == b4)
			{
				whichButtonOpen = 4;
			}
			if (e.target == b5)
			{
				whichButtonOpen = 5;
			}
			if (e.target == b6)
			{
				whichButtonOpen = 6;
			}
			
			for each (var j:MovieClip in tabArray)
			{
				j.visible = false;
				if (tabArray.indexOf(j) == whichButtonOpen - 1)
				{
					j.visible = true;
				}
			}
			
			for each (var ns:NetStream in nsArray)
			{
				ns.seek(0);
				ns.pause();
			}
			
			for each (var i:Boolean in tabVideoArray)
			{
				if (i == true)
				{
					nsArray[tabVideoArray.indexOf(i)].seek(0);
					nsArray[tabVideoArray.indexOf(i)].pause();
				}
				if (tabVideoArray.indexOf(i) == whichButtonOpen - 1)
				{
					nsArray[tabVideoArray.indexOf(i)].resume();
				}
			}
			
			//introText.visible = false;
			b1.addEventListener(TuioTouchEvent.TOUCH_DOWN, goHome);
			
			trace("arrNum = " + arrNum);
			if (arrNum == 2)
			{
				trace("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& " + tArray[whichButtonOpen - 1].text);
				if (mortgageCalc == null && tArray[whichButtonOpen - 1].text == owner.loadXML.mortgageCalculatorString)
				{
					trace("add mortgage calculator");
					mortgageCalc = new MortgageCalculator(this);
					mortgageCalc.x = 140;
					mortgageCalc.y = 177;
					addChild(mortgageCalc);
					/*pSlider = new SliderPointer();
					pSlider.x = mortgageCalc.x;
					pSlider.y = mortgageCalc.y + 61; //i 136 t 211
					addChild(pSlider);
					
					iSlider = new SliderPointer();
					iSlider.x = mortgageCalc.x;
					iSlider.y = mortgageCalc.y + 136; //i 136 t 211
					addChild(iSlider);
					
					tSlider = new SliderPointer();
					tSlider.x = mortgageCalc.x;
					tSlider.y = mortgageCalc.y + 211; //i 136 t 211
					addChild(tSlider);
					
					pSlider.addEventListener(TuioTouchEvent.TOUCH_DOWN,sliderDown);
					iSlider.addEventListener(TuioTouchEvent.TOUCH_DOWN,sliderDown);
					tSlider.addEventListener(TuioTouchEvent.TOUCH_DOWN,sliderDown);
					
					pSlider.addEventListener(TransformGestureEvent.GESTURE_PAN,sliderMove);
					iSlider.addEventListener(TransformGestureEvent.GESTURE_PAN,sliderMove);
					tSlider.addEventListener(TransformGestureEvent.GESTURE_PAN,sliderMove);
			
					addEventListener(TuioTouchEvent.TOUCH_UP,sliderUp);*/
				}
				else 
				{
					if (mortgageCalc != null && tArray[whichButtonOpen - 1].text != owner.loadXML.mortgageCalculatorString)
					{
						mortgageCalc.removeListeners();
						removeChild(mortgageCalc);
						mortgageCalc = null;
					}
				}
			}
			
			if (arrNum == 4)
			{
				stopTimeout();
				removeEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimeout);
				if (tArray[whichButtonOpen - 1].text == owner.loadXML.doodleCanvasString)
				{
					if (drawApp == null)
					{
						drawApp = new drawingApp(this);
						addChild(drawApp);
						drawApp.x = 24;
						drawApp.y = 75;
					}
				}
				if (tArray[whichButtonOpen - 1].text == owner.loadXML.billBlasterString)
				{
					if (drawApp != null)
					{
						//restartTimeout();
						removeChild(drawApp);
						drawApp = null;
					}
					owner.addNewBillNinja(whichPopup);
				}
				if (tArray[whichButtonOpen - 1].text == owner.loadXML.checkBounceString)
				{
					if (drawApp != null)
					{
						//restartTimeout();
						removeChild(drawApp);
						drawApp = null;
					}
					owner.addNewPong(whichPopup);
				}
				if (tArray[whichButtonOpen - 1].text == owner.loadXML.platformerString)
				{
					if (drawApp != null)
					{
						//restartTimeout();
						removeChild(drawApp);
						drawApp = null;
					}
					owner.addNewPlatform(whichPopup);
				}
				if (tArray[whichButtonOpen - 1].text == owner.loadXML.buckBreakerString)
				{
					if (drawApp != null)
					{
						//restartTimeout();
						removeChild(drawApp);
						drawApp = null;
					}
					owner.addNewBreaker(whichPopup);
				}
				
			}
		}
		
		/*private function sliderDown(e:TuioTouchEvent) 
		{
			if (mortgageCalc != null)
			{
				//owner.addText("sliderDown");
				e.target.gotoAndStop(2);
				mortgageCalc.sliderArray.push(e.target);
				mortgageCalc.isReadyToMove = true;
			}
		}*/
		
		/*private function sliderUp(e:TuioTouchEvent) 
		{
			if (mortgageCalc != null)
			{
				//owner.addText("sliderUp");
				mortgageCalc.isSliding = false;
				pSlider.gotoAndStop(1);
				tSlider.gotoAndStop(1);
				iSlider.gotoAndStop(1);
				mortgageCalc.sliderArray = [];
				mortgageCalc.isReadyToMove = false;
			}
		}*/
		
		/*private function sliderMove(e:TransformGestureEvent) 
		{
			if (mortgageCalc != null)
			{
				//curX = e.stageX;
				//owner.addText("sliderMove " + curX);
				//owner.addText("init e.target.x = " + e.target.x);
				//sliderArray[0].x = Math.floor(e.stageX - owner.x - this.x - 12.5);
				e.target.x += e.offsetX;
				//owner.addText("post e.target.x = " + e.target.x);
				//owner.addText("sliderArray[0].x = " + sliderArray[0].x + ", sliderArray[0].parent.x = " + sliderArray[0].parent.x + ", this.x = " + this.x + ", owner.x = " + owner.x + ", e.stageX = " + e.stageX);
				if(e.target.x < mortgageCalc.x)
				{
					e.target.x = mortgageCalc.x;
				}
				if(e.target.x > 365 + mortgageCalc.x)
				{
					e.target.x = 365 + mortgageCalc.x;
				}
			}
		}*/
		
		public function resetPopup():void
		{
			owner.addText("resetPopup");
			b1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goHome);
			tab1.visible = true;
			tab2.visible = false;
			tab3.visible = false;
			tab4.visible = false;
			tab5.visible = false;
			tab6.visible = false;
			
			if (tab1Video == true)
			{
				nsArray[nsArray.length - 1].seek(0);
				nsArray[nsArray.length - 1].pause();
			}
			if (tab2Video == true)
			{
				nsArray[nsArray.length - 1].seek(0);
				nsArray[nsArray.length - 1].pause();
			}
			if (tab3Video == true)
			{
				nsArray[nsArray.length - 1].seek(0);
				nsArray[nsArray.length - 1].pause();
			}
			if (tab4Video == true)
			{
				nsArray[nsArray.length - 1].seek(0);
				nsArray[nsArray.length - 1].pause();
			}
			if (tab5Video == true)
			{
				nsArray[nsArray.length - 1].seek(0);
				nsArray[nsArray.length - 1].pause();
			}
			if (tab6Video == true)
			{
				nsArray[nsArray.length - 1].seek(0);
				nsArray[nsArray.length - 1].pause();
			}
			
			for each(var ns:NetStream in nsArray)
			{
				ns.seek(0);
				ns.pause();
			}
				
			if (whichMenu == 3)
			{
				if (mortgageCalc != null)
				{
					mortgageCalc.removeListeners();
					removeChild(mortgageCalc);
					mortgageCalc = null;
				}
				/*if (pSlider!= null)
				{
					pSlider.removeEventListener(TuioTouchEvent.TOUCH_DOWN,sliderDown);
					pSlider.removeEventListener(TransformGestureEvent.GESTURE_PAN,sliderMove);
					removeChild(pSlider);
					pSlider = null;
				}
				if (iSlider != null)
				{
					iSlider.removeEventListener(TuioTouchEvent.TOUCH_DOWN,sliderDown);
					iSlider.removeEventListener(TransformGestureEvent.GESTURE_PAN,sliderMove);
					removeChild(iSlider);
					iSlider = null;
				}
				if (tSlider != null)
				{
					tSlider.removeEventListener(TuioTouchEvent.TOUCH_DOWN,sliderDown);
					tSlider.removeEventListener(TransformGestureEvent.GESTURE_PAN,sliderMove);
					removeChild(tSlider);
					tSlider = null;
				}*/
			}
			
			if (whichMenu == 5)
			{
				//restartTimeout();
				addEventListener(TuioTouchEvent.TOUCH_DOWN, resetTimeout);
				if (drawApp != null)
				{
					removeChild(drawApp);
					drawApp = null;
				}
				if (whichButtonOpen == 1)
				{
					//restartTimeout();
				}
				//owner.removeNewBillNinja(whichPopup);
			}
			whichButtonOpen = 1;
		}
		
		private function goHome(e:TuioTouchEvent):void
		{
			resetPopup();
			restartTimeout();
		}
		
		/*public function resetAll():void
		{
			stopTimeout();
			if (whichMenu == 3)
			{
				if (mortgageCalc != null)
				{
					mortgageCalc.resetCalc();
				}
			}
			if (whichMenu == 5)
			{
				if (drawApp != null)
				{
					drawApp.clearAllLines();
				}
			}
			
		}*/
		
		public function setFilters(b:Boolean):void
		{
			if (b)
			{
				this.filters = [sf];
			}
			else
			{
				this.filters = [];
			}
		}
		
		private function handleScale(e:TransformGestureEvent):void {
			/*owner.addText(e.scaleX + ", " + e.scaleY);
			var factor:Number = 1;
			if (e.scaleX < this.scaleX)
			{
				factor = -1;
			}
			else
			{
				factor = 1;
			}
			if (this.scaleX <= 2 && this.scaleY <= 2 && this.scaleX >= 1 && this.scaleY >= 1)
			{
				this.scaleX += .1 * factor;
				this.scaleY += .1 * factor;
			}*/
			var factor:Number = 1;
			if (e.scaleX < this.scaleX)
			{
				factor = -1;
			}
			else
			{
				factor = 1;
			}
			this.scaleX *=  e.scaleX;
			this.scaleY *=  e.scaleY;
			
			//this.x -= (this.width - bgWidth)/2 * factor;
			//this.y -= (this.height - bgHeight)/2 * factor;
			
			if (e.phase==GesturePhase.END) 
			{
				e.currentTarget.stopDrag();
			}
		}
		
		private function handleRotate(e:TransformGestureEvent):void {
			this.rotation += e.rotation;
		}
		
		private function handleDrag(e:TransformGestureEvent):void {
			//owner.addText("handleDrag");
			if (whichMenu == 5)
			{
				if (drawApp != null)
				{
					if (e.stageX < this.x + 24 || e.stageX > this.x + 24 + drawApp.width)
					{
						this.x += e.offsetX;
						this.y += e.offsetY;
					}
				}
				else
				{
					this.x += e.offsetX;
					this.y += e.offsetY;
				}
			}
			/*else if (whichMenu == 3)
			{
				if (mortgageCalc != null)
				{
					//owner.addText("mortgageCalc exists");
					if (e.stageX < this.x + 70 || e.stageX > this.x + this.width - 70)
					{
						//owner.addText("touch is outside mortgageCalc");
						var tempX:Number = this.x;
						var tempX2:Number = this.x + this.width - 70;
						//owner.addText("this x 1 = " + tempX + ", this x 2 = " + tempX2 + ", stageX = " + e.stageX);
						//this.x += e.offsetX;
						//this.y += e.offsetY;
					}
				}
				else
				{
					this.x += e.offsetX;
					this.y += e.offsetY;
				}
			}*/
			else if (whichMenu == 1 || whichMenu == 2 ||whichMenu == 3 || whichMenu == 4)
			{
				this.x += e.offsetX;
				this.y += e.offsetY;
			}
		}
		
		private function handleDown(e:TuioTouchEvent):void 
		{
			/*if (curID == -1) {
				//setChildIndex(this, numChildren - 1);
				this.curID = e.tuioContainer.sessionID;
				addEventListener(TuioTouchEvent.TOUCH_UP, handleUp);
			}*/
			
			owner.setPopupDepth(whichPopup);
		}
		
		private function handleUp(e:TuioTouchEvent):void {
			if(e.tuioContainer.sessionID == this.curID){
				this.curID = -1;
				removeEventListener(TuioTouchEvent.TOUCH_UP, handleUp);
			}
		}
		
		public function addText(s:String):void
		{
			owner.addText(s);
		}
		
		private function setTextFormat(f:TextLayoutFormat, s:Number, c:uint, a:Number = 0, lh:Number = 0, r:Boolean = true, su:Number = 0):void
		{
			f.alignmentBaseline = TextBaseline.ASCENT;
			f.dominantBaseline = TextBaseline.ASCENT;
			f.firstBaselineOffset = "ascent";
			
			if (lh != 0)
			{
				/*if (owner.leftToRight == false)
				{
					lh -= lh * .3;
				}*/
				f.lineHeight = lh;
			}
			
			//f.locale = owner.locale;
			if (r)
			{
				f.fontFamily = gotham.fontName;
			}
			else
			{
				f.fontFamily = gothamBold.fontName;
			}
				
			//if (owner.leftToRight == false)
			//{
				f.textAlign = TextAlign.RIGHT;
				//f.fontFamily = arabicRiyadh.fontName;
				f.fontSize = s + su;
				f.color = c;
				//textDirection = "flashx.textLayout.formats.Direction.RTL";
				//f.paddingTop = 20;
				//f.paragraphSpaceBefore = 100;
			//}
			
			/*else
			{
				f.textAlign = TextAlign.LEFT;
				f.fontSize = s;
				f.color = c;
				textDirection = "flashx.textLayout.formats.Direction.LTR";
			}*/
			
			
			switch (a)
			{
				case 0:
				break;
				case 1:
				f.textAlign = TextAlign.LEFT;
				break;
				case 2:
				f.textAlign = TextAlign.CENTER;
				break;
				case 3:
				f.textAlign = TextAlign.RIGHT;
				break;
			}
		}
		
		private function setText(t:TLFTextField, o:MovieClip, c:uint, s:String, f:TextLayoutFormat, tf:TextFlow, offset:Number = 0, textBelow:Boolean = false, textBelowBuffer:Number = 0, replacementY:Number = 0, tHeight:Number = 0):void
		{
			t.selectable = false;
			//t.direction = textDirection;
			t.wordWrap = true;
			t.multiline = true;
			t.textColor = c;
			t.mouseChildren = false;
			t.mouseEnabled = false;
			
			if (o != null)
			{
				t.width = o.width;
				t.height = o.height; 
				t.x = o.x;
				if (!textBelow)
				{
					t.y = o.y - offset;
					t.verticalAlign = flashx.textLayout.formats.VerticalAlign.MIDDLE;
				}
				else
				{
					t.y = o.y + o.height + textBelowBuffer;
					t.verticalAlign = flashx.textLayout.formats.VerticalAlign.TOP;
				}
			}
			else
			{
				t.verticalAlign = flashx.textLayout.formats.VerticalAlign.TOP;
				t.width = 1680;
				t.height = tHeight;
				t.x = 0;
				t.y = replacementY;
				
				//if (owner.leftToRight == false)
				//{
					//t.y += 10;
				//}
			}
			
			
			
			t.htmlText = s;
			
			tf = t.textFlow;
			tf.hostFormat = f;
	 		tf.flowComposer.updateAllControllers();
		}
		
		private function setTextFlow(tf:TextFlow, t:TLFTextField, f:TextLayoutFormat):void
		{
			tf = t.textFlow;
			tf.hostFormat = f;
	 		tf.flowComposer.updateAllControllers();
		}
	}
	
}
