﻿//Classes must always be wrapped in a package
package code
{
	//importing display elements that we can use in this class
	import flash.display.*;
	import flash.events.*;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	//defining the class name and saying that it
	//extends the MovieClip class, meaning that it has the same
	//properties as a movieclip
	public class Brick extends MovieClip 
	{
		//The main timeline!
		private var owner:BrickGame;

		//all classes must have a function that runs every time
		//an instance of the class is put on stage
		public function Brick(manager:BrickGame)
		{
			owner = manager;

			//Code that will be run when the brick is added to the stage
			addEventListener(Event.ADDED, beginClass);
			//Enter frame code
			addEventListener(Event.ENTER_FRAME, enterFrameEvents);
		}
		//private function are just functions that you can't access
		//from the main timeline, but only within the class itself
		private function beginClass(event:Event):void
		{
			//incrementing how many bricks are on the stage
			removeEventListener(Event.ADDED, beginClass);
			owner.brickAmt++;
		}
		private function enterFrameEvents(event:Event):void
		{
			//checking if the player has lost
			if(owner.gameOver){
				//destroy this brick
				this.parent.removeChild(this);
				//stop running this code
				removeEventListener(Event.ENTER_FRAME, enterFrameEvents);
			}
			//hit testing with the ball
			if(this.hitTestObject(owner.mcBall)){
				//making the ball bounce off vertically
				owner.ballYSpeed *= -1;
				//destroying this brick
				this.parent.removeChild(this);
				//stop running this code
				removeEventListener(Event.ENTER_FRAME, enterFrameEvents);
				//decrementing the amount of bricks on stage
				owner.brickAmt--;
				//increasing the score
				if(this.currentFrame == 1)
				{
					owner.score += 1;
					explosion();
					explodeText(1);
				}
				if(this.currentFrame == 2)
				{
					owner.score += 10;
					explosion();
					explodeText(10);
				}
				if(this.currentFrame == 3)
				{
					owner.score += 20;
					explosion();
					explodeText(20);
				}
				if(this.currentFrame == 4)
				{
					owner.score += 50;
					explosion();
					explodeText(50);
				}
				if(this.currentFrame == 5)
				{
					owner.score += 100;
					explosion();
					explodeText(100);
				}
				owner.updateTextFields(null);
			}
		}
		
		private function explosion()
		{
			//find the point halfway between the created slash
			var exploder:Exploder = new Exploder();
			
			//create an explosion at the halfway point
			exploder.scaleX = exploder.scaleY = 0;
			exploder.x = this.x + this.width/2;
			exploder.y = this.y + this.height/2;
			owner.addChild(exploder);
			
			TweenMax.to(exploder,.5,{alpha:0, scaleX:1, scaleY:1, ease:Linear.easeNone, onComplete:explodeFinish, onCompleteParams:[exploder]});
		}
		
		private function explodeText(amount:int)
		{
			//create the explosion text at the halfway point on slash
			var plusText:PlusText = new PlusText();
			plusText.alpha = 0;
			plusText.x = this.x + this.width/2;
			plusText.y = this.y + this.height/2;
			owner.addChild(plusText);
			
			if(amount == 1)
			{
				plusText.scaleX = plusText.scaleY = .75;
			}
			if(amount == 10)
			{
				plusText.scaleX = plusText.scaleY = 1;
			}
			if(amount == 20)
			{
				plusText.scaleX = plusText.scaleY = 1.5;
			}
			if(amount == 50)
			{
				plusText.scaleX = plusText.scaleY = 2;
			}
			if(amount == 100)
			{
				plusText.scaleX = plusText.scaleY = 2.5;
			}
			
			//change amount = to the type slashed
			plusText.scoreText.text = "+ " + amount + "!"
			
			TweenMax.to(plusText,1,{alpha:1, y:plusText.y+50, ease:Strong.easeOut, onComplete:explodeTextFinish, onCompleteParams:[plusText]});
		}
		
		private function explodeFinish(explosion:Exploder)
		{
			//remove explosion
			owner.removeChild(explosion);
		}
		
		private function explodeTextFinish(plusText:PlusText)
		{
			//remove explosion text
			owner.removeChild(plusText);
		}
	}
}