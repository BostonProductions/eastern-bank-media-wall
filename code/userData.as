﻿package code
{
	import Box2D.Common.Math.b2Vec2;
	import flash.display.Sprite;
	import flash.display.BitmapData;
	import flash.geom.Matrix;
	import com.greensock.*;
	import com.greensock.easing.*;

	public class userData extends Sprite
	{
		public var id:int;
		public var texture:BitmapData;
		public var objectName:String;
		public var timesSliced:int;
		public var isBuck:Boolean;
		public var exploder:Exploder;

		public function userData(id:int, verticesVec:Vector.<b2Vec2>, texture:BitmapData, oName:String, buckBoolean:Boolean)
		{
			this.id = id;
			this.texture = texture;
			this.objectName = oName;
			timesSliced = 0;
			isBuck = buckBoolean;

			// I use the matrix so that I can have the center of the shape I'm drawing match the center of the BitmapData image - I "move" the BitmapData projection left by half its width and up by half its height.
			var m:Matrix = new Matrix();
			m.tx = -texture.width*0.5;
			m.ty = -texture.height*0.5;
			
			// I then draw lines from each vertex to the next, in clockwise order and use the beginBitmapFill() method to add the texture.
			this.graphics.lineStyle(2);
			this.graphics.beginBitmapFill(texture, m, true, true);
			this.graphics.moveTo(verticesVec[0].x*30, verticesVec[0].y*30);
			for (var i:int=1; i<verticesVec.length; i++) this.graphics.lineTo(verticesVec[i].x*30, verticesVec[i].y*30);
			this.graphics.lineTo(verticesVec[0].x*30, verticesVec[0].y*30);
			this.graphics.endFill();
		}
		
		private function explosion(expX:Number, expY:Number)//position:b2Vec2
		{
			//If a body falls below a certain yPosition, delete it.
			exploder = new Exploder();
			addChild(exploder);
			exploder.scaleX = exploder.scaleY = 0;
			exploder.x = expX*30;
			exploder.y = expY*30;
			TweenMax.to(exploder,.2,{alpha:0, scaleX:1, scaleY:1, ease:Linear.easeNone, onComplete:explodeFinish, onCompleteParams:[exploder]});
		}
		
		private function explodeFinish(explosion:Exploder)
		{
			//If a body falls below a certain yPosition, delete it.
			removeChild(explosion);
		}
	}
}