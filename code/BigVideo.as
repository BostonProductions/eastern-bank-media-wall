﻿package code
{
	
	import flash.display.MovieClip;

	import flash.media.Video;

	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.NetStreamAppendBytesAction;
	import flash.events.NetStatusEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.display.Loader;
	
	//import flash.events.MouseEvent;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	public class BigVideo extends MovieClip 
	{
		//public var whichTab = 4;
		public var whichOwnerTab:Number;

		public var v:Video = new Video();
		public var nc:NetConnection = new NetConnection();
		public var ns:NetStream;
		
		public var vidPath:String;
		
		//private var animMask:VidMask;
		//private var animTitle:MteTitle;
		private var vidBar:VidBar;
		private var playPauseBtn:PlayBtn;
		private var playBar:PlayBar;
		private var scrub:Scrub;
		
		private var duration:Number;
		
		private var playBarOffset:Number = 15;
		
		private var owner:Document;
		
		private var closeBtn:CloseBlue;
		
		public function BigVideo(myOwner:Document, p:String) 
		{
			owner = myOwner;
			vidPath = p;
			//trace("vidPath = " + vidPath);
			
			v = new Video();
			//startVideo(nc, ns, v, p);
			
			nc.connect(null);
			ns = new NetStream(nc);

			ns.client = this;
			ns.addEventListener(NetStatusEvent.NET_STATUS, ns_onPlayStatus);
			addChild(v);
			v.attachNetStream(ns);
			ns.play(vidPath);

			v.width = 1080;
			v.height = 607;
				
			v.x = 0;
			v.y = 0;


			v.smoothing = true;
			//v.cacheAsBitmap = true;
				
			//v.x = 0;
			//v.alpha = 0;
			
			
			
			vidBar = new VidBar();
			addChild(vidBar);
			//vidBar.gotoAndStop(whichOwnerTab);
			//animMask = new VidMask();
			//addChild(animMask);
			playBar = new PlayBar();
			addChild(playBar);
			scrub = new Scrub();
			addChild(scrub);
			playPauseBtn = new PlayBtn();
			addChild(playPauseBtn);
			//animTitle = new MteTitle();
			//addChild(animTitle);
			//animTitle.gotoAndStop(whichOwnerTab);
			
			vidBar.x = 0;
			vidBar.y = 607;
			
			//animTitle.x = 23;
			//animTitle.y = 181;
			
			//animMask.x = -1;
			//animMask.y = 152;
			
			playBar.x = 55;
			playBar.y = 607 + (vidBar.height/2);
			
			scrub.x = playBar.x;
			scrub.y = playBar.y;
			
			playPauseBtn.x = 10;
			playPauseBtn.y = 617;
			
			vidBar.timeText.selectable = false;
			
			
			playPauseBtn.gotoAndStop(1);
			//playPauseBtn.addEventListener(MouseEvent.MOUSE_DOWN, playAnim);
			
			
			stopVideo();
			//scrub.addEventListener(TuioTouchEvent.TOUCH_DOWN, videoScrubberDown);
			playPauseBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, restartVideo);
			
			
			closeBtn = new CloseBlue();
			addChild(closeBtn);
			closeBtn.x = 1080 - closeBtn.width;
			closeBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, closeVid);
			
			restartVideo();
			owner.somethingOpen = true;
			
		}
		
		private function closeVid(e:TuioTouchEvent = null):void
		{
			//trace("trying to close the video");
			stopVideo();
			removeAllListeners();
			owner.removeThis(this);
		}

		/*private function startVideo(nc1:NetConnection, ns1:NetStream, video1:Video, videoURL1:String, w:Number = 1200, h:Number = 850)
		{
			nc.connect(null);
			ns = new NetStream(nc);

			ns.client = this;
			ns.addEventListener(NetStatusEvent.NET_STATUS, ns_onPlayStatus);
			addChild(v);
			v.attachNetStream(ns);
			ns.play(videoURL1);

			v.width = w;
			v.height = h;
				
			v.x = 0;
			v.y = 0;

			stopVideo()

			v.smoothing = true;
			//v.cacheAsBitmap = true;
				
			v.x = 0;
			v.y = 120;
			v.alpha = 0;
			
			//scrub.addEventListener(TuioTouchEvent.MOUSE_OVER, btnOver);
			//scrub.addEventListener(TuioTouchEvent.MOUSE_OUT, btnOut);

			
		}*/
		
		public function stopVideo(e:TuioTouchEvent = null):void
		{
			//trace("stopVideo");
			playPauseBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, pauseVideo);
			ns.seek(0);
			ns.pause();
			playPauseBtn.gotoAndStop(1);
			playPauseBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, restartVideo);
			removeEventListener(Event.ENTER_FRAME, setTime);
			
			//checkTime();
		}
		public function restartVideo(e:TuioTouchEvent = null):void
		{
			//trace("restartVideo");
			playPauseBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, restartVideo);
			ns.resume();
			playPauseBtn.gotoAndStop(2);
			playPauseBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, pauseVideo);
			addEventListener(Event.ENTER_FRAME, setTime);
			
		}
		public function pauseVideo(e:TuioTouchEvent = null):void
		{
			//trace("pauseVideo");
			playPauseBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, pauseVideo);
			ns.pause();
			playPauseBtn.gotoAndStop(1);
			playPauseBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, restartVideo);
			removeEventListener(Event.ENTER_FRAME, setTime);
		}
		
		public function onMetaData(_data:Object):void
		{
			//trace("onMetaData");
			duration = _data.duration;
			//trace("duration = " + duration);
		}
		public function onXMPData(info:Object):void
		{

		}
		public function onCuePoint(info:Object):void
		{

		}

		private function ns_onMetaData(_data:Object)
		{
			//trace("ns_onMetaData");
			duration = _data.duration;
		}

		private function ns_onCuePoint(_data:Object)
		{

		}

		public function onPlayStatus(info:Object)
		{

		}

		private function ns_onPlayStatus(e:NetStatusEvent)
		{
			//trace("ns_onPlayStatus");
			var tempString:String = e.info.code;
			//trace("ns_onPlayStatus = " + tempString);
			if (e.info.code == "NetStream.Buffer.Empty" || e.info.code == "NetStream.Seek.Complete") 
			{
				resetVideo();
				closeVid();
			} 
		}
		
		////// TIMELINE SCRUBBER //////
		/*function videoStatus():void
		{
			amountLoaded = ns.bytesLoaded / ns.bytesTotal;
			videoTrackDownload.width = amountLoaded * 340;
			videoThumb.x = ns.time / duration * 340;
			videoTrackProgress.width = videoThumb.x;
		}*/
		private function videoScrubberDown(e:TuioTouchEvent):void
		{
		//	trace("videoScrubberDown");
			pauseVideo();
			var bounds:Rectangle = new Rectangle(playBar.x,scrub.y,playBar.width - playBarOffset,0);
			//clearInterval(videoInterval);
			//scrubInterval = setInterval(scrubTimeline, 10);
			scrub.startDrag(false, bounds);
			//stage.addEventListener(TuioTouchEvent.TOUCH_UP, stopScrubbingVideo);
			//stage.addEventListener(TuioTouchEvent.TOUCH_MOVE, scrubTimeline);
		}
		private function scrubTimeline(e:TuioTouchEvent):void
		{
			//trace("scrubTimeline");
			ns.seek(((scrub.x - playBar.x)/(playBar.width - playBarOffset)) * duration);
			//trace("((scrub.x - playBar.x)/(playBar.width - playBarOffset)) * duration = " + ((scrub.x - playBar.x)/(playBar.width - playBarOffset)) * duration);
			//trace("ns.time = " + ns.time);
			//trace("scrub.x = " + scrub.x);
			//ns.resume();
			ns.pause();
			checkTime();
			//trace(Math.floor((scrub.x / (playBar.width - playBarOffset)) * duration));
			//trace(ns.time);
		}
		private function stopScrubbingVideo(e:TuioTouchEvent):void
		{
			//trace("stopScrubbingVideo");
			//stage.removeEventListener(TuioTouchEvent.TOUCH_MOVE, scrubTimeline);
			//stage.removeEventListener(TuioTouchEvent.TOUCH_UP, stopScrubbingVideo);
			//clearInterval(scrubInterval);
			//videoInterval = setInterval(videoStatus, 100);
			stopDrag();
			checkTime();
			scrub.x = playBar.x + ((ns.time/duration) * (playBar.width - playBarOffset));
			//trace("ns.time = " + ns.time);
			//trace("setTime scrub.x = " + scrub.x);
		}
		
		public function resetVideo():void
		{
			//trace("resetVideo");
			stopVideo();
			removeEventListener(Event.ENTER_FRAME, setTime);
			scrub.x = playBar.x;
			vidBar.timeText.text = "00:00";
		}
		
		private function setTime(e:Event = null):void
		{
			//trace("setTime " + ns.time);
			//trace("setTime " + duration);
			checkTime();
			scrub.x = playBar.x + ((ns.time/duration) * (playBar.width - playBarOffset));
			//trace("setTime scrub.x = " + scrub.x);
			if (Number(ns.time.toFixed(1)) >= Number(duration.toFixed(1)))
			{
				//trace("*****************************");
				//trace(ns.time.toFixed(1));
				//trace(duration.toFixed(1));
				resetVideo();
			}
		}
		
		private function checkTime():void
		{
			//trace("checkTime ns.time = " + ns.time);
			var vidTime:String = "";
			var secs:Number = Math.round(ns.time);
			if (secs >= 60)
			{
				var min:Number = Math.floor(secs/60);
				var rem:Number = (secs/60) - min;
				secs = Math.round(rem * 60);
				
				if (min < 10)
				{
					vidTime += "0";
				}
				vidTime += min;
				vidTime += ":";
				if (secs < 10)
				{
					vidTime += "0"
				}
				vidTime += secs;
			}
			else
			{
				vidTime = "00:";
				if (secs < 10)
				{
					vidTime += "0"
				}
				vidTime += secs;
			}
			
			vidBar.timeText.text = vidTime;
		}
		
		public function removeAllListeners():void
		{
			scrub.removeEventListener(TuioTouchEvent.TOUCH_DOWN, videoScrubberDown);
			playPauseBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, restartVideo);
			playPauseBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, pauseVideo);
			removeEventListener(Event.ENTER_FRAME, setTime);
			
			closeBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, closeVid);
			ns.removeEventListener(NetStatusEvent.NET_STATUS, ns_onPlayStatus);
			//removeEventListener(TuioTouchEvent.TOUCH_UP, stopScrubbingVideo);
			//removeEventListener(TuioTouchEvent.TOUCH_MOVE, scrubTimeline);
		}

	}
	
}
