﻿package code {
	
	import flash.display.MovieClip;
	
	import org.tuio.TuioTouchEvent;
	import flash.events.TransformGestureEvent;
	
	public class SliderPointer extends MovieClip {
		
		private var leftSide:Number = 0;
		private var rightSide:Number = 365;
		
		public function SliderPointer() 
		{
			//this.addEventListener(TransformGestureEvent.GESTURE_PAN, handleDrag);
		}
		
		public function setConstraints(a:Number, b:Number):void
		{
			leftSide = a;
			rightSide = b;
		}
		
		private function handleDrag(e:TransformGestureEvent):void 
		{
			this.x += e.offsetX;
			
			if (this.x < leftSide)
			{
				this.x = leftSide;
			}
			if (this.x > rightSide)
			{
				this.x = rightSide;
			}
		}
	}
	
}
