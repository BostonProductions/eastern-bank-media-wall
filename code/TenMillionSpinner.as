﻿package code
{
	import flash.display.Sprite;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import code.NumberAnimation;
	import flash.geom.Point;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	
	
	public class TenMillionSpinner extends Sprite {
		private var numBackground:ScrollingAmount = new ScrollingAmount();
		private var numberAnim7:NumberAnimation = new NumberAnimation();// ten million
		private var numberAnim6:NumberAnimation = new NumberAnimation();// one million
		
		private var numberAnim5:NumberAnimation = new NumberAnimation();// hundred thousands
		private var numberAnim4:NumberAnimation = new NumberAnimation();// ten thousands
		private var numberAnim3:NumberAnimation = new NumberAnimation();// one thousands
		
		private var numberAnim2:NumberAnimation = new NumberAnimation();// hundreds
		private var numberAnim1:NumberAnimation = new NumberAnimation();// tens
		private var numberAnim0:NumberAnimation = new NumberAnimation();// ones*/
		
		public var OKToInit:Boolean;
		public var finalNumber;Number;
		public var ItsDoneCounter:Number;
		
		private var xLocAryArray:Array = [80,123,176,218,261,313,356,398];// starting x location of numbers
		private var numberArray:Array = [];
		
		public static const SPINNER_DONE:String = "spinnerdone";
		
		public function TenMillionSpinner() {
			// constructor code
			addChild(numBackground);
			OKToInit = true;
			
		}

		public function init(_finalNumber:Number):void{
			
			
			
			OKToInit = false;
			final_Number = _finalNumber;
			
			// makes array to distribute to numbers
			for(var i:Number = 0; i < 8;i++){
				
				this["numberAnim" + i].x = xLocAryArray[i];
				this["numberAnim" + i].y = 15;
				
				this["numberAnim" + i].whichDigit = i; 
				
				addChild(this["numberAnim" + i]);
				
				var str:String = String(finalNumber);
				var startIndex:Number = 8 - (str.length);
				
				this["numberAnim" + i].hideFinalNumber = false;
				
				if(str.length < 8){
					//trace("startIndex = " + startIndex + " finalNumber " + finalNumber);
					if(i<startIndex){
						
						this["numberAnim" + i].final_Number = 0;
						this["numberAnim" + i].hideFinalNumber = true;
						//trace(" i < startIndex digit= " + i  +" my Number= " + 0);
						
						}else
						{
							var ndx:Number = (i - startIndex);
							
							this["numberAnim" + i].final_Number = numberArray[ndx];
							//trace(" i => startIndex digit= " + i  +" my Number= " + numberArray[ndx] + " my Index= " + ndx)
						}
					}else{// string has 8 digits
						this["numberAnim" + i].final_Number = numberArray[i];
						this["numberAnim" + i].hideFinalNumber = false;
						//trace(" str has 8 digit= " + i  +" my Number= " + numberArray[i])
						
						}
				this["numberAnim" + i].init();
				
				}
				OKToInit = true;
			}
			
		public function get oKToInit():Boolean{
			return OKToInit;
			}
			
		public function stopAnimation():void{
			//trace("tenMillion stopani");
			if(!this.hasEventListener(Event.ENTER_FRAME)){
			addEventListener(Event.ENTER_FRAME, doStopAni,false,0,true);
			}
			}
		private function doStopAni(e:Event):void{
				if(OKToInit){
					removeEventListener(Event.ENTER_FRAME, doStopAni);
					ItsDoneCounter = 0;
					
					for(var i:Number = 0; i < 8;i++){
						this["numberAnim" + i].stopAtNumber();
						this["numberAnim" + i].addEventListener(NumberAnimation.ANIM_DONE, incrementCounter,false,0,true);
						}
				}
			}
		public function incrementCounter(e:Event):void{
			ItsDoneCounter += 1;
			e.currentTarget.removeEventListener(NumberAnimation.ANIM_DONE, incrementCounter);
			
			//trace(e.currentTarget.whichDigit + "ItsDoneCounter = " + ItsDoneCounter);
			
			if(ItsDoneCounter > 7){
				//trace("ItsDoneCounter = " + ItsDoneCounter);
				sendSpinnerDone();
				}
			
			}
		public function sendSpinnerDone():void{
			
			dispatchEvent(new Event(SPINNER_DONE, true));
			
			}

		public function get final_Number():Number{
			return finalNumber;
			}
			
		public function set final_Number(num:Number):void{
			
			finalNumber = num;
			
			var str:String = String(finalNumber);
			var len:Number = str.length;
			
			numberArray = [];
			//trace("tenMillionspinner num " + num + " str= " + str + " len= " + len);
			for(var i:Number = 0;i<len;i++){
				numberArray.push(str.charAt(i));
				}
				//trace("numberArray= " + numberArray);
				//trace("final number = " + finalNumber);
			
			}

	}
	
}
