﻿package code 
{
	
	import flash.display.MovieClip;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	
	public class DrGm extends MovieClip 
	{
		
		public var playerNumber:Number;
		
		private var s:String = "";
		private var sa:String = "";
		private var sy:String = "";
		private var sm:String = "";
		
		public var score:Number = 0;
		public var monthlySavings:Number = 0;
		public var gArray:Array = new Array();
		public var gAYMArray:Array;
		
		public var whichG:Number = 0;
		public var whichField:Number = 0;
		
		public var playerGo:Boolean = false;
		
		public var ct:Number = 0;
		
		private var s1:String = "";
		private var s2:String = "";
		private var s3:String = "";
		private var s4:String = "";
		
		public var countdownTimer:Timer;
		public var time:Number = 10;
		public var gameTimer:Timer;
		
		public var owner:LoGm;
		
		public var whichSet:Number = 1;
		
		public function DrGm(myOwner:LoGm) 
		{
			owner = myOwner;
			/*for (var i:int = 0; i < 12; i++)
			{
				gAYMArray = new Array(0, 0, 0);
				gArray.push(gAYMArray);
			}*/
			b1.addEventListener(TuioTouchEvent.TOUCH_DOWN, select1);
			b2.addEventListener(TuioTouchEvent.TOUCH_DOWN, select2);
			b1.gotoAndStop(1);
			b2.gotoAndStop(2);
			dreamNext.addEventListener(TuioTouchEvent.TOUCH_DOWN, goNext);
		}
		
		public function select1(e:TuioTouchEvent):void
		{
			b1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, select1);
			b2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, select2);
			b1.visible = false;
			b2.visible = false;
			bigB.visible = true;
			
		}
		
		public function select2(e:TuioTouchEvent):void
		{
			b1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, select1);
			b2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, select2);
			b1.visible = false;
			b2.visible = false;
			bigB.visible = true;
			
		}
		
		public function goNext(e:TuioTouchEvent):void
		{
			bigB.visible = false;
			b1.visible = true;
			b2.visible = true;
			b1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, select1);
			b2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, select2);
			b1.addEventListener(TuioTouchEvent.TOUCH_DOWN, select1);
			b2.addEventListener(TuioTouchEvent.TOUCH_DOWN, select2);
			if (whichSet == 1)
			{
				b1.gotoAndStop(3);
				b2.gotoAndStop(4);
			}
			if (whichSet == 2)
			{
				b1.gotoAndStop(5);
				b2.gotoAndStop(6);
			}
			if (whichSet == 3)
			{
				b1.gotoAndStop(7);
				b2.gotoAndStop(8);
			}
			
			whichSet++;
		}
		
		/*public function zeroOrOne():Number
		{
			var num:Number = Math.floor(Math.random() * 2);
			return num;
		}
		
		
		public function startGame():void
		{
			ct = 1;
			gameTimer = new Timer(3000);
			gameTimer.stop();
			gameTimer.start();
			gameTimer.addEventListener(TimerEvent.TIMER, countdown);
		}
		
		private function countdown(e:TimerEvent = null):void
		{
			gameTimer.stop();
			gameTimer.removeEventListener(TimerEvent.TIMER, countdown);
			switch (ct)
			{
				case 1:
					
					if (zeroOrOne() == 0)
					{
						gotoAndStop(3);
					}
					else
					{
						gotoAndStop(4);
					}
					
					resetQuestions();
					
					addQuestListeners();
					
					ct = 2;
				break;
				
				case 2:
					setScore(currentFrame);
					if (zeroOrOne() == 0)
					{
						gotoAndStop(5);
					}
					else
					{
						gotoAndStop(6);
					}
					
					ct = 3;
				break;
				
				case 3:
					setScore(currentFrame);
					
					if (zeroOrOne() == 0)
					{
						gotoAndStop(7);
					}
					else
					{
						gotoAndStop(8);
					}
					
					ct = 4;
				break;
				
				case 4:
					setScore(currentFrame);
					
					if (zeroOrOne() == 0)
					{
						gotoAndStop(9);
					}
					else
					{
						gotoAndStop(10);
					}
					
					ct = 5;
				break;
				
				case 5:
					setScore(currentFrame);
					
					if (zeroOrOne() == 0)
					{
						gotoAndStop(11);
					}
					else
					{
						gotoAndStop(12);
					}
					
					ct = 6;
				break;
				
				case 6:
					setScore(currentFrame);
					
					removeQuestListeners();
					
					gotoAndStop(13);
					
					showScore();
					
					moveOn.addEventListener(TuioTouchEvent.TOUCH_DOWN, movingOn);
					//timerGo(7);
				break;
				
				case 8:
					gotoAndStop(15);
					ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, goOk);
					addCalcListeners();
				break;
				
				case 17:
					gotoAndStop(18);
					timerGo(18);
				break;
				
				case 18:
					gotoAndStop(19);
					timerGo(19);
				break;
				
				case 19:
					trace("----------------------------------------- done");
					playerGo = false;
					this.visible = false;
					owner.removeThis();
				break;
			}
		}
		
		private function countPlayers(e:TuioTouchEvent):void
		{
			trace("countPlayers");
			trace(e.currentTarget.name);
			
			playerGo = true;
			
		}
		
		public function startCountdown():void
		{
			owner.owner.fadeGamesOut();
			
			countdownTimer = new Timer(1000);
			countdownTimer.stop();
			countdownTimer.start();
			countdownTimer.addEventListener(TimerEvent.TIMER, countdown2);
			
			jg.addEventListener(TuioTouchEvent.TOUCH_DOWN, countPlayers);
		}
		
		private function countdown2(e:TimerEvent):void
		{
			time--;
			t.text = time.toString();
			if (time == 0)
			{
				countdownTimer.stop();
				countdownTimer.removeEventListener(TimerEvent.TIMER, countdown2);
				nextFrame();
				if (playerGo == false)
				{
					visible = false;
					owner.removeThis();
				}
				else
				{
					startGame();
				}
			}
		}
		
		
		
		private function goOk(e:TuioTouchEvent):void
		{
			ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, goOk);
			removeCalLiseners();
			gotoAndStop(16);
			resetGs();
			addGListeners();
			ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
		}
		
		private function allDone(e:TuioTouchEvent):void
		{
			resetGs();
			removeGListeners();
			
			ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, allDone);
			
			gotoAndStop(17);
			
			calc();
			
			timerGo(17);
		}
		
		
		
		private function movingOn(e:TuioTouchEvent):void
		{
			moveOn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, movingOn);
			gotoAndStop(14);
			timerGo(8);
		}
		
		private function timerGo(n:Number):void
		{
			ct = n;
			switch (n)
			{
				case 2:
					gameTimer = new Timer(5000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 3:
					gameTimer = new Timer(5000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 4:
					gameTimer = new Timer(5000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 5:
					gameTimer = new Timer(5000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 6:
					gameTimer = new Timer(5000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 7:
					
				break;
				case 8:
					gameTimer = new Timer(10000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 17:
					gameTimer = new Timer(10000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 18:
					gameTimer = new Timer(10000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
				case 19:
					gameTimer = new Timer(10000);
					gameTimer.stop();
					gameTimer.start();
					gameTimer.addEventListener(TimerEvent.TIMER, countdown);
				break;
			}
		}
		
		
		
		public function calc():void
		{
			
		}
		
		public function setScore(n:Number):void
		{
			switch (n)
			{
				case 3:
					if (q1.currentFrame == 2)
					{
						score += 0;
					}
					if (q2.currentFrame == 2)
					{
						score += 1;
					}
					if (q3.currentFrame == 2)
					{
						score += 2;
					}
				break;
				case 4:
					if (q1.currentFrame == 2)
					{
						score += 2;
					}
					if (q2.currentFrame == 2)
					{
						score += 1;
					}
					if (q3.currentFrame == 2)
					{
						score += 0;
					}
				break;
				case 5:
					if (q1.currentFrame == 2)
					{
						score += 2;
					}
					if (q2.currentFrame == 2)
					{
						score += 0;
					}
					if (q3.currentFrame == 2)
					{
						score += 1;
					}
				break;
				case 6:
					if (q1.currentFrame == 2)
					{
						score += 0;
					}
					if (q2.currentFrame == 2)
					{
						score += 1;
					}
					if (q3.currentFrame == 2)
					{
						score += 2;
					}
				break;
				case 7:
					if (q1.currentFrame == 2)
					{
						score += 0;
					}
					if (q2.currentFrame == 2)
					{
						score += 1;
					}
					if (q3.currentFrame == 2)
					{
						score += 2;
					}
				break;
				case 8:
					if (q1.currentFrame == 2)
					{
						score += 1;
					}
					if (q2.currentFrame == 2)
					{
						score += 0;
					}
					if (q3.currentFrame == 2)
					{
						score += 2;
					}
				break;
				case 9:
					if (q1.currentFrame == 2)
					{
						score += 0;
					}
					if (q2.currentFrame == 2)
					{
						score += 2;
					}
					if (q3.currentFrame == 2)
					{
						score += 1;
					}
				break;
				case 10:
					if (q1.currentFrame == 2)
					{
						score += 1;
					}
					if (q2.currentFrame == 2)
					{
						score += 0;
					}
					if (q3.currentFrame == 2)
					{
						score += 2;
					}
				break;
				case 11:
					if (q1.currentFrame == 2)
					{
						score += 1;
					}
					if (q2.currentFrame == 2)
					{
						score += 0;
					}
					if (q3.currentFrame == 2)
					{
						score += 2;
					}
				break;
				case 12:
					if (q1.currentFrame == 2)
					{
						score += 2;
					}
					if (q2.currentFrame == 2)
					{
						score += 0;
					}
					if (q3.currentFrame == 2)
					{
						score += 1;
					}
				break;
			}
			trace("score = " + score);
			resetQuestions();
		}
		
		public function showScore():void
		{
			sc.text = String(score);
		}
		
		public function addCalcListeners():void
		{
			k1.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k2.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k3.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k4.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k5.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k6.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k7.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k8.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k9.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k0.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
		}
		
		public function removeCalLiseners():void
		{
			k1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k4.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k5.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k6.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k7.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k8.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k9.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			k0.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
		}
		
		private function addAmount(e:TuioTouchEvent):void
		{
			switch(whichField)
			{
				case 0:
					s += e.currentTarget.t.text;
					trace(Number(s)/100);
					savings.text = String((Number(s)/100).toFixed(2));
				break;
				
				case 1:
					sa += e.currentTarget.t.text;
					trace(Number(sa)/100);
					cCalc.savings.text = String((Number(sa)/100).toFixed(2));
				break;
				
				case 2:
					sy += e.currentTarget.t.text;
					cCalc.years.text = String(Number(sy));
				break;
				
				case 3:
					sm += e.currentTarget.t.text;
					cCalc.months.text = String(Number(sm));
				break;
			}
			
		}
		
		public function resetQuestions():void
		{
			q1.gotoAndStop(1);
			q2.gotoAndStop(1);
			q3.gotoAndStop(1);
		}
		
		public function addQuestListeners():void
		{
			q1.addEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
			q2.addEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
			q3.addEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
		}
		
		public function removeQuestListeners():void
		{
			q1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
			q2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
			q3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, loChosen);
		}
		
		private function loChosen(e:TuioTouchEvent):void
		{
			q1.gotoAndStop(1);
			q2.gotoAndStop(1);
			q3.gotoAndStop(1);
			e.currentTarget.gotoAndStop(2);
			countdown();
		}
		
		public function resetGs():void
		{
			cCalc.visible = false;
			g1.gotoAndStop(1);
			g2.gotoAndStop(1);
			g3.gotoAndStop(1);
			g4.gotoAndStop(1);
			g5.gotoAndStop(1);
			g6.gotoAndStop(1);
			g7.gotoAndStop(1);
			g8.gotoAndStop(1);
			g9.gotoAndStop(1);
			g10.gotoAndStop(1);
			g11.gotoAndStop(1);
			g12.gotoAndStop(1);
		}
		
		public function addGListeners():void
		{
			g1.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g2.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g3.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g4.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g5.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g6.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g7.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g8.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g9.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g10.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g11.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g12.addEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			
			cCalc.k1.t.text = "1";
			cCalc.k2.t.text = "2";
			cCalc.k3.t.text = "3";
			cCalc.k4.t.text = "4";
			cCalc.k5.t.text = "5";
			cCalc.k6.t.text = "6";
			cCalc.k7.t.text = "7";
			cCalc.k8.t.text = "8";
			cCalc.k9.t.text = "9";
			cCalc.k0.t.text = "0";
			cCalc.savings.mouseEnabled = false;
			cCalc.savings.mouseChildren = false;
			cCalc.years.mouseEnabled = false;
			cCalc.years.mouseChildren = false;
			cCalc.months.mouseEnabled = false;
			cCalc.months.mouseChildren = false;
			
			cCalc.k1.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k2.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k3.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k4.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k5.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k6.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k7.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k8.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k9.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k0.addEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			
			cCalc.ok.addEventListener(TuioTouchEvent.TOUCH_DOWN, hideCT);
			
			cCalc.yBG.alpha = .3;
			cCalc.mBG.alpha = .3;
			whichField = 1;
			cCalc.yBG.addEventListener(TuioTouchEvent.TOUCH_DOWN, setY);
			cCalc.mBG.addEventListener(TuioTouchEvent.TOUCH_DOWN, setM);
		}
		
		private function setA(e:TuioTouchEvent = null):void
		{
			cCalc.aBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setA);
			cCalc.yBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setY);
			cCalc.mBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setM);
			cCalc.yBG.addEventListener(TuioTouchEvent.TOUCH_DOWN, setY);
			cCalc.mBG.addEventListener(TuioTouchEvent.TOUCH_DOWN, setM);
			whichField = 1;
			cCalc.aBG.alpha = 1;
			cCalc.yBG.alpha = .3;
			cCalc.mBG.alpha = .3;
		}
		
		private function setY(e:TuioTouchEvent):void
		{
			cCalc.aBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setA);
			cCalc.yBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setY);
			cCalc.mBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setM);
			cCalc.aBG.addEventListener(TuioTouchEvent.TOUCH_DOWN, setA);
			cCalc.mBG.addEventListener(TuioTouchEvent.TOUCH_DOWN, setM);
			whichField = 2;
			cCalc.aBG.alpha = .3;
			cCalc.yBG.alpha = 1;
			cCalc.mBG.alpha = .3;
		}
		
		private function setM(e:TuioTouchEvent):void
		{
			cCalc.aBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setA);
			cCalc.yBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setY);
			cCalc.mBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setM);
			cCalc.aBG.addEventListener(TuioTouchEvent.TOUCH_DOWN, setA);
			cCalc.yBG.addEventListener(TuioTouchEvent.TOUCH_DOWN, setY);
			whichField = 3;
			cCalc.aBG.alpha = .3;
			cCalc.yBG.alpha = .3;
			cCalc.mBG.alpha = 1;
		}
		
		private function hideCT(e:TuioTouchEvent):void
		{
			cCalc.visible = false;
			gArray[whichG - 1][0] = cCalc.savings.text;
			gArray[whichG - 1][1] = cCalc.years.text;
			gArray[whichG - 1][2] = cCalc.months.text;
			trace(gArray);
		}
		
		public function removeGListeners():void
		{
			g1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g4.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g5.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g6.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g7.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g8.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g9.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g10.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g11.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			g12.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setG);
			
			cCalc.k1.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k2.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k3.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k4.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k5.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k6.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k7.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k8.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k9.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			cCalc.k0.removeEventListener(TuioTouchEvent.TOUCH_DOWN, addAmount);
			
			cCalc.ok.removeEventListener(TuioTouchEvent.TOUCH_DOWN, hideCT);
			
			cCalc.aBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setA);
			cCalc.yBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setY);
			cCalc.mBG.removeEventListener(TuioTouchEvent.TOUCH_DOWN, setM);
		}
		
		private function setG(e:TuioTouchEvent):void
		{
			e.currentTarget.gotoAndStop(2);
			cCalc.visible = true;
			switch (e.currentTarget.name)
			{
				case ("g1"):
					whichG = 1;
				break;
				
				case ("g2"):
					whichG = 2;
				break;
				
				case ("g3"):
					whichG = 3;
				break;
				
				case ("g4"):
					whichG = 4;
				break;
				
				case ("g5"):
					whichG = 5;
				break;
				
				case ("g6"):
					whichG = 6;
				break;
				
				case ("g7"):
					whichG = 7;
				break;
				
				case ("g8"):
					whichG = 8;
				break;
				
				case ("g9"):
					whichG = 9;
				break;
				
				case ("g10"):
					whichG = 10;
				break;
				
				case ("g11"):
					whichG = 11;
				break;
				
				case ("g12"):
					whichG = 12;
				break;
				
			}
			setA();
			
			cCalc.savings.text = sa = gArray[whichG - 1][0];
			cCalc.years.text = sy = gArray[whichG - 1][1];
			cCalc.months.text = sm = gArray[whichG - 1][2];
		}*/
	}
	
}
