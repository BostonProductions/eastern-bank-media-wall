﻿package  code
{
	import flash.display.Sprite;
	import flash.events.*;
	import flash.display.*;
	import flash.xml.*;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import code.Document;
	
	public class LoadXML extends Sprite
	{
		public var configXML:XML;
		private var configLoader:URLLoader;
		public var statsXML:XML;
		private var statsLoader:URLLoader;
		
		public var hideMouse:Boolean;
		public var isNativeWindow:Boolean;
		public var useTuio:Boolean;
		public var useAcceleration:Boolean;
		public var checkCMS:Boolean;
		public var showDrawApp:Boolean;
		public var screenWidth:Number;
		public var screenHeight:Number;
		public var numberOfScreens:Number;
		
		public var waterSound:String;
		
		public var credit2Speed:Number = 6;
		
		public var creditAnswersPopIn:String;
		public var creditCorrectRound1:String;
		public var creditCorrectRound2:String;
		public var creditFeedbackScreensRound1:String;
		public var creditIncorrectRound1:String;
		public var creditIncorrectRound2:String;
		public var creditIntro_and_QuestionMusicRound2:String;
		public var creditIntroMusicStartGame:String;
		public var creditMeter:String;
		public var creditNosedive:String;
		public var creditPressAnswerRound1:String;
		public var creditQuestionMusicRound1:String;
		public var creditQuestionSlideIn:String;
		public var creditTimeRanOut:String;
		public var creditWinner:String;
		public var saverButtonClick:String;
		public var saverCar:String;
		public var saverCloudPopIn:String;
		public var saverCollege:String;
		public var saverGambler:String;
		public var saverGoGetter:String;
		public var saverHome:String;
		public var saverMoneyMeter:String;
		public var saverRetirement:String;
		public var saverTortoise:String;
		public var saverVacation:String;
		public var saverWardrobe:String;
		public var saverWedding:String;
		public var saverQuizMusic:String;
		public var saverScaleShortVersion:String;
		public var saverMoneyRolling:String;
		
		public var iconsArray:Array = new Array();
		public var iconArray:Array;
		public var anIcon:String;
		
		public var aSpeed:Number;
		public var speedArray:Array = new Array();
		
		private var owner:Document;
		
		public var cmsXml:XML;
		//public var cmsLoader:URLLoader;
		
		public var popupArray:Array;
		public var businessArray:Array = new Array();
		public var dreamsArray:Array = new Array();
		public var homeArray:Array = new Array();
		public var lifeArray:Array = new Array();
		public var playArray:Array = new Array();
		
		public var titleArray:Array;
		public var fileArray:Array;
		public var fileSizeArray:Array;
		
		public var businessTitle:String;
		public var businessFile:String;
		public var businessFileSize:Number;
		
		public var dreamsTitle:String;
		public var dreamsFile:String;
		public var dreamsFileSize:Number;
		
		public var homeTitle:String;
		public var homeFile:String;
		public var homeFileSize:Number;
		
		public var lifeTitle:String;
		public var lifeFile:String;
		public var lifeFileSize:Number;
		
		public var playTitle:String;
		public var playFile:String;
		public var playFileSize:Number;
		public var playImage:String;
		public var playText:String;
		
		public var mortgageRateURL:String;
		public var mortgageRateToday:String;
		public var mortgageRateRate:String;
		public var mortgageRateAPR:String;
		public var mortgageRatePoints:String;
		
		public var equityRateTableString:String;
		public var mortgageRateTableString:String;
		public var mortgageCalculatorString:String;
		public var doodleCanvasString:String;
		public var billBlasterString:String;
		public var checkBounceString:String;
		public var platformerString:String;
		public var buckBreakerString:String;
		
		public var picAlpha:Number = .4;
		public var trans:Number = 10;
		public var del:Number = 80;
		public var picTime:Number = 10;
		public var maskSpeed:Number = 3;
		public var maskStart1:Number = 4320;
		public var maskStart2:Number = 4320;
		public var wordsStart1:Number = 250;
		public var wordsStart2:Number = 500;
		public var wordsAlpha:Number = .5;
		
		public var isDone:Boolean = false;
		
		public var saverTimeout:Number = 30;
		
		public var VO2Sound:String;
		public var VO3Sound:String;
		public var VO4Sound:String;
		public var VO6Sound:String;
		public var VO6aSound:String;
		public var VO6bSound:String;
		public var VO7Sound:String;
		public var VO9Sound:String;
		public var VO9aSound:String;
		public var VO9bSound:String;
		public var VO10Sound:String;
		public var VO12Sound:String;
		public var VO12aSound:String;
		public var VO12bSound:String;
		public var VO13Sound:String;
		public var VO15Sound:String;
		public var VO15aSound:String;
		public var VO15bSound:String;
		public var VO16Sound:String;
		public var VO18Sound:String;
		public var VO18aSound:String;
		public var VO18bSound:String;
		public var VO19Sound:String;
		public var VO20Sound:String;
		public var VO21Sound:String;
		public var VO22Sound:String;
		public var VO24aSound:String;
		public var VO24bSound:String;
		public var VO25Sound:String;
		
		public var sing1Sound:String;
		public var sing2Sound:String;
		public var sing3Sound:String;
		public var sing4Sound:String;
		public var sing5Sound:String;
		
		public var startEnglish:String;
		public var startSpanish:String;
		
		public var oldXML:XML;
		
		public var numTort:Number = 0;
		public var numGo:Number = 0;
		public var numGamb:Number = 0;
		
		public var lifePlayer:Array;
		public var lifeWins:Array;
		public var lifeTies:Array;
		public var lifeFinal:Array;
		public var lifeHomeEquity:Array;
		public var lifeReserveCredit:Array;
		public var lifeCreditCard:Array;
		public var lifeCarLoan:Array;
		public var lifeMortgage:Array;
		public var lifeWelcome:Array;
		
		public var lifeStartGame:Array;
		public var lifeMainTitle1:Array;
		public var lifeMainTitle2:Array;
		public var lifeImproveCreditScore:Array;
		public var lifeAGameToTest:Array;
		public var lifeJoinGame:Array;
		public var lifeRound:Array;
		public var lifeSample:Array;
		public var lifeR1SampleQuestion:Array;
		public var lifeR1SampleAnswer1:Array;
		public var lifeR1SampleAnswer2:Array;
		public var lifeR1SampleAnswer3:Array;
		public var lifeR1SampleAnswer4:Array;
		public var lifeR1Question:Array;
		public var lifeR1Question1:Array;
		public var lifeR1Q1Answer1:Array;
		public var lifeR1Q1Answer2:Array;
		public var lifeR1Q1Answer3:Array;
		public var lifeR1Q1Answer4:Array;
		public var lifeR1Q1CorrectAnswer:Array;
		public var lifeR1Q1AnswerExplanation1:Array;
		public var lifeR1Q1AnswerExplanation2:Array;
		public var lifeR1Question2:Array;
		public var lifeR1Q2Answer1:Array;
		public var lifeR1Q2Answer2:Array;
		public var lifeR1Q2Answer3:Array;
		public var lifeR1Q2Answer4:Array;
		public var lifeR1Q2CorrectAnswer:Array;
		public var lifeR1Q2AnswerExplanation1:Array;
		public var lifeR1Q2AnswerExplanation2:Array;
		public var lifeR1Question3:Array;
		public var lifeR1Q3Answer1:Array;
		public var lifeR1Q3Answer2:Array;
		public var lifeR1Q3Answer3:Array;
		public var lifeR1Q3Answer4:Array;
		public var lifeR1Q3CorrectAnswer:Array;
		public var lifeR1Q3AnswerExplanation1:Array;
		public var lifeR1Q3AnswerExplanation2:Array;
		public var lifeR1Q3AnswerExplanation3:Array;
		public var lifeR1Question4:Array;
		public var lifeR1Q4Answer1:Array;
		public var lifeR1Q4Answer2:Array;
		public var lifeR1Q4Answer3:Array;
		public var lifeR1Q4Answer4:Array;
		public var lifeR1Q4CorrectAnswer:Array;
		public var lifeR1Q4AnswerExplanation1:Array;
		public var lifeR1Q4AnswerExplanation2:Array;
		public var lifeR1Question5:Array;
		public var lifeR1Q5Answer1:Array;
		public var lifeR1Q5Answer2:Array;
		public var lifeR1Q5Answer3:Array;
		public var lifeR1Q5Answer4:Array;
		public var lifeR1Q5CorrectAnswer:Array;
		public var lifeR1Q5AnswerExplanation1:Array;
		public var lifeR1Q5AnswerExplanation2:Array;
		public var lifeR1Q5AnswerExplanation3:Array;
		public var lifeR2SampleQuestion:Array;
		public var lifeR2SampleAnswer1:Array;
		public var lifeR2SampleAnswer2:Array;
		public var lifeR2SampleAnswer3:Array;
		public var lifeR2SampleAnswer4:Array;
		public var lifeR2SampleAnswer5:Array;
		public var lifeR2SampleAnswer6:Array;
		public var lifeR2Question1:Array;
		public var lifeR2Q1Answer1:Array;
		public var lifeR2Q1Answer2:Array;
		public var lifeR2Q1Answer3:Array;
		public var lifeR2Q1Answer4:Array;
		public var lifeR2Q1Answer5:Array;
		public var lifeR2Q1Answer6:Array;
		public var lifeR2Q1Answer7:Array;
		public var lifeR2Q1Answer8:Array;
		public var lifeR2Q1Answer9:Array;
		public var lifeR2Q1Answer10:Array;
		public var lifeR2Q1Answer11:Array;
		public var lifeR2Q1Answer12:Array;
		public var lifeR2Question2:Array;
		public var lifeR2Q2Answer1:Array;
		public var lifeR2Q2Answer2:Array;
		public var lifeR2Q2Answer3:Array;
		public var lifeR2Q2Answer4:Array;
		public var lifeR2Q2Answer5:Array;
		public var lifeR2Q2Answer6:Array;
		public var lifeR2Question3:Array;
		public var lifeR2Q3Answer1:Array;
		public var lifeR2Q3Answer2:Array;
		public var lifeR2Q3Answer3:Array;
		public var lifeR2Q3Answer4:Array;
		public var lifeR2Q3Answer5:Array;
		public var lifeR2Q3Answer6:Array;
		public var lifeR2Q3Answer7:Array;
		public var lifeR2Q3Answer8:Array;
		
		
		public var dreamIntro1:Array;
		public var dreamIntro2:Array;
		public var dreamClickHere:Array;
		public var dreamWantToSave:Array;
		public var dreamClickStart:Array;
		public var dreamEnglishOrSpanish:Array;
		public var dreamQuestion1:Array;
		public var dreamYes:Array;
		public var dreamNo:Array;
		public var dreamMidtermSavingsGoal:Array;
		public var dreamQ1Info1:Array;
		public var dreamQ1Info2:Array;
		public var dreamQ1Info3:Array;
		public var dreamQ1Info4:Array;
		public var dreamQ1Dictionary:Array;
		public var dreamQuestion2:Array;
		public var dreamShortTermSavingsGoal:Array;
		public var dreamQ2Info1:Array;
		public var dreamQ2Info2:Array;
		public var dreamQ2Info3:Array;
		public var dreamQuestion3:Array;
		public var dreamLongTermSavingsGoal:Array;
		public var dreamQ3Info1:Array;
		public var dreamQ3Info2:Array;
		public var dreamQ3Info3:Array;
		public var dreamQ3Info4:Array;
		public var dreamQ3Dictionary:Array;
		public var dreamQuestion4:Array;
		public var dreamQ4Info1:Array;
		public var dreamQ4Info2:Array;
		public var dreamQuestion5:Array;
		public var dreamQ5Info1:Array;
		public var dreamQ5Info2:Array;
		public var dreamQuestion6:Array;
		public var dreamQ6Info1:Array;
		public var dreamQ6Info2:Array;
		public var dreamQ6Info3:Array;
		public var dreamQuestion7:Array;
		public var dreamQ7Info1:Array;
		public var dreamQ7Info2:Array;
		public var dreamQ7Info3:Array;
		public var dreamQ7Dictionary1:Array;
		public var dreamQ7Dictionary2:Array;
		public var dreamQ7DIctionary3:Array;
		public var dreamSavingHabits:Array;
		public var dreamSHClickHere:Array;
		public var dreamTouchTheChoice:Array;
		public var dreamQuestionNumber:Array;
		public var dreamSHQuestion1:Array;
		public var dreamSHQ1AnswerA:Array;
		public var dreamSHQ1AnswerB:Array;
		public var dreamSHQ1AnswerC:Array;
		public var dreamSHQuestion2:Array;
		public var dreamSHQ2AnswerA:Array;
		public var dreamSHQ2AnswerB:Array;
		public var dreamSHQ2AnswerC:Array;
		public var dreamSHQuestion3:Array;
		public var dreamSHQ3AnswerA:Array;
		public var dreamSHQ3AnswerB:Array;
		public var dreamSHQ3AnswerC:Array;
		public var dreamSHQuestion4:Array;
		public var dreamSHQ4AnswerA:Array;
		public var dreamSHQ4AnswerB:Array;
		public var dreamSHQ4AnswerC:Array;
		public var dreamSHQuestion5:Array;
		public var dreamSHQ5AnswerA:Array;
		public var dreamSHQ5AnswerB:Array;
		public var dreamSHQ5AnswerC:Array;
		public var dreamSHQ5Dictionary1:Array;
		public var dreamSHQ5Dictionary2:Array;
		public var dreamSHQ5Dictionary3:Array;
		public var dreamSHQuestion6:Array;
		public var dreamSHQ6AnswerA:Array;
		public var dreamSHQ6AnswerB:Array;
		public var dreamSHQ6AnswerC:Array;
		public var dreamSHQuestion7:Array;
		public var dreamSHQ7AnswerA:Array;
		public var dreamSHQ7AnswerB:Array;
		public var dreamSHQ7AnswerC:Array;
		public var dreamSHQuestion8:Array;
		public var dreamSHQ8AnswerA:Array;
		public var dreamSHQ8AnswerB:Array;
		public var dreamSHQ8AnswerC:Array;
		public var dreamSHQuestion9:Array;
		public var dreamSHQ9AnswerA:Array;
		public var dreamSHQ9AnswerB:Array;
		public var dreamSHQ9AnswerC:Array;
		public var dreamSHQ9Dictionary:Array;
		public var dreamTheTortoise:Array;
		public var dreamTortoise1:Array;
		public var dreamTortoise2:Array;
		public var dreamTortoise3:Array;
		public var dreamTortoise4:Array;
		public var dreamTortoise5:Array;
		public var dreamTortoise6:Array;
		public var dreamCheckOutOtherEBVisitors:Array;
		public var dreamTheGoGetter:Array;
		public var dreamGoGetter1:Array;
		public var dreamGoGetter2:Array;
		public var dreamGoGetter3:Array;
		public var dreamGoGetter4:Array;
		public var dreamGoGetter5:Array;
		public var dreamTheGambler:Array;
		public var dreamGambler1:Array;
		public var dreamGambler2:Array;
		public var dreamGambler3:Array;
		public var dreamGambler4:Array;
		public var dreamFinal1:Array;
		public var dreamFinal2:Array;
		public var dreamSavings:Array;
		public var dreamSavingsAccount:Array;
		public var dreamAutomaticTransfers:Array;
		public var dreamMoneyMarketAccount:Array;
		public var dreamCD:Array;
		public var dreamRetirementPlanning:Array;
		public var dreamInvestments:Array;
		public var dream401K:Array;
		public var dreamRothIRA:Array;
		public var dreamBuyAHouse:Array;
		public var dreamBuildCredit:Array;
		public var dreamAutomaticSavingsPlan:Array;
		public var dreamStocks:Array;
		public var dreamTouchDream:Array;
		public var dreamSpenderKind:Array;
		public var dreamTouchFind:Array;
		public var dreamBigDreamsSave:Array;
		public var dreamModestDreamsSave:Array;
		public var dreamSavingsTips:Array;
		public var dreamSaving:Array;
		public var dreamSpending:Array;
		public var dreamWhatKindSaver:Array;
		public var dreamWhatKindSaver1:Array;
		public var dreamWhatKindSaver2:Array;
		public var dreamHowEBRanked:Array;
		public var dreamBDBS:Array;
		public var dreamMA:Array;
		public var dreamSS:Array;
		public var dreamRH:Array;
		public var dreamBH:Array;
		public var dreamSpendStyle:Array;
		public var dreamSaveStyle:Array;
		public var dreamResults:Array;
		public var dreamMismatch:Array;
		public var dreamMatch:Array;
		public var dreamMisMatch1:Array;
		public var dreamMisMatch2:Array;
		public var dreamMatch1:Array;
		public var dreamMisMatch3:Array;
		public var dreamMatch2:Array;
		public var dreamMatch3:Array;
		public var dreamMatch4:Array;
		public var dreamMisMatch4:Array;
		public var dreamSavingsTips2:Array;
		public var dreamTips:Array;
		public var dreamCheckOutOther:Array;
		public var dreamNext:Array;
		public var dreamDone:Array;
		public var dreamCheckOutThese:Array;
		public var dreamStillThere:Array;
		public var dreamQuit:Array;
		public var dreamExit:Array;
		public var dreamAverageCost:Array;
		
		public var whichMode:Number = 1;
		
		public var businessVidEnglish:String;
		public var businessVidSpanish:String;
		
		public var homeVidEnglish:String;
		public var homeVidSpanish:String;
		
		public var showBEVid:Boolean = false;
		public var showBSVid:Boolean = false;
		public var showHEVid:Boolean = false;
		public var showHSVid:Boolean = false;
		
		public var showBigGamesInPopups:Boolean = false;
		
		//public var dreams:Array;
		
		public function LoadXML(myOwner:Document = null)
		{
			owner =  myOwner;
			if (owner != null)
			{
				
				//owner.addText("LoadXML");
			}
			configLoader = new URLLoader();
			configLoader.load(new URLRequest("config.xml"));
			configLoader.addEventListener(Event.COMPLETE, processConfigXML);
			
			loadStats();
		}
		
		public function CMSData(s:XML):void
		{
			if (owner != null)
			{
				//owner.addText("CMSData");
			}
			//parse cms xml here
			cmsXml = s;
			if (owner != null)
			{
				//owner.addText("cmsXml = " + cmsXml);
			}
			
			//cmsLoader = new URLLoader();
			//cmsLoader.load(new URLRequest("config.xml"));
			//cmsLoader.addEventListener(Event.COMPLETE, processConfigXML);
			
			
			//loop through the data tags then loop through each tab to get the info
			//for each popup
			businessArray = new Array();
			var tabArray:Array;
			
			businessTitle = cmsXml.data[0].title;
			businessFile = cmsXml.data[0].file;
			businessFileSize = cmsXml.data[0].file_size;
			//trace("array length = " + cmsXml.data[0].tab.length());
			for (var j:int = 0; j < cmsXml.data[0].tab.length(); j++)
			{
				tabArray = new Array();
				tabArray.push(cmsXml.data[0].tab[j].title);
				tabArray.push(cmsXml.data[0].tab[j].button_text);
				tabArray.push(cmsXml.data[0].tab[j].body);
				tabArray.push(cmsXml.data[0].tab[j].photo.file);
				tabArray.push(cmsXml.data[0].tab[j].photo.file_size);
				tabArray.push(cmsXml.data[0].tab[j].video.file);
				tabArray.push(cmsXml.data[0].tab[j].video.file_size);
				tabArray.push(cmsXml.data[0].tab[j].disabled);
				businessArray.push(tabArray);
			}
			if (owner != null)
			{
				//owner.addText("businessArray");
			}
			
			dreamsTitle = cmsXml.data[1].title;
			dreamsFile = cmsXml.data[1].file;
			dreamsFileSize = cmsXml.data[1].file_size;
			//trace("array length = " + cmsXml.data[1].tab.length());
			for (var k:int = 0; k < cmsXml.data[1].tab.length(); k++)
			{
				tabArray = new Array();
				tabArray.push(cmsXml.data[1].tab[k].title);
				tabArray.push(cmsXml.data[1].tab[k].button_text);
				tabArray.push(cmsXml.data[1].tab[k].body);
				tabArray.push(cmsXml.data[1].tab[k].photo.file);
				tabArray.push(cmsXml.data[1].tab[k].photo.file_size);
				tabArray.push(cmsXml.data[1].tab[k].video.file);
				tabArray.push(cmsXml.data[1].tab[k].video.file_size);
				tabArray.push(cmsXml.data[1].tab[k].disabled);
				dreamsArray.push(tabArray);
			}
			if (owner != null)
			{
				//owner.addText("dreamsArray");
			}
			
			homeTitle = cmsXml.data[2].title;
			homeFile = cmsXml.data[2].file;
			homeFileSize = cmsXml.data[2].file_size;
			//trace("array length = " + cmsXml.data[2].tab.length());
			for (var l:int = 0; l < cmsXml.data[2].tab.length(); l++)
			{
				tabArray = new Array();
				tabArray.push(cmsXml.data[2].tab[l].title);
				tabArray.push(cmsXml.data[2].tab[l].button_text);
				tabArray.push(cmsXml.data[2].tab[l].body);
				tabArray.push(cmsXml.data[2].tab[l].photo.file);
				tabArray.push(cmsXml.data[2].tab[l].photo.file_size);
				tabArray.push(cmsXml.data[2].tab[l].video.file);
				tabArray.push(cmsXml.data[2].tab[l].video.file_size);
				tabArray.push(cmsXml.data[2].tab[l].disabled);
				homeArray.push(tabArray);
			}
			if (owner != null)
			{
				//owner.addText("homeArray");
			}
			
			lifeTitle = cmsXml.data[3].title;
			lifeFile = cmsXml.data[3].file;
			lifeFileSize = cmsXml.data[3].file_size;
			if (owner != null)
			{
				//owner.addText("array length = " + cmsXml.data[3].tab.length());
			}
			for (var m:int = 0; m < cmsXml.data[3].tab.length(); m++)
			{
				tabArray = new Array();
				tabArray.push(cmsXml.data[3].tab[m].title);
				tabArray.push(cmsXml.data[3].tab[m].button_text);
				tabArray.push(cmsXml.data[3].tab[m].body);
				tabArray.push(cmsXml.data[3].tab[m].photo.file);
				tabArray.push(cmsXml.data[3].tab[m].photo.file_size);
				tabArray.push(cmsXml.data[3].tab[m].video.file);
				tabArray.push(cmsXml.data[3].tab[m].video.file_size);
				tabArray.push(cmsXml.data[3].tab[m].disabled);
				lifeArray.push(tabArray);
			}
			if (owner != null)
			{
				//owner.addText("lifeArray");
				//owner.addText("after life array");
			}
			////trace("lifeArray = " + lifeArray);
			
			playTitle = cmsXml.data[4].title;
			if (owner != null)
			{
				//owner.addText("cmsXml.data[4]");
				//owner.addText(cmsXml.data[4]);
			}
			if (owner != null)
			{
				//owner.addText("playTitle = " + playTitle);
			}
			playFile = cmsXml.data[4].file;
			if (owner != null)
			{
				//owner.addText("playFile = " + playFile);
			}
			playFileSize = cmsXml.data[4].file_size;
			if (owner != null)
			{
				//owner.addText("playFileSize = " + playFileSize);
			}
			playImage = cmsXml.data[4].tab.photo.file;
			if (owner != null)
			{
				//owner.addText("playImage = " + playImage);
			}
			playText = cmsXml.data[4].tab.body;
			if (owner != null)
			{
				//owner.addText("playText = " + playText);
			}
			playArray.push("HOME");
			if (owner != null)
			{
				//owner.addText("HOME");
			}
			for (var n:int = 0; n < cmsXml.data[4].activatedGames.game.length(); n++)
			{
			if (owner != null)
			{
				//owner.addText("for loop " + n);
			}
				var game:String = cmsXml.data[4].activatedGames.game[n];
			if (owner != null)
			{
				//owner.addText("game = " + game);
			}
				/*tabArray.push(cmsXml.data[4].tab[n].title);
				tabArray.push(cmsXml.data[4].tab[n].file);
				tabArray.push(cmsXml.data[4].tab[n].file_size);
				tabArray.push(cmsXml.data[4].tab[n].photo.file);
				tabArray.push(cmsXml.data[4].tab[n].photo.file_size);
				tabArray.push(cmsXml.data[4].tab[n].video.file);
				tabArray.push(cmsXml.data[4].tab[n].video.file_size);
				tabArray.push(cmsXml.data[4].tab[n].disabled);*/
				playArray.push(game);
			}
			/*//trace("*******************");
			//trace(playArray);
			//trace("*******************");*/
			if (owner != null)
			{
				//owner.addText("playArray");
			}
			
			popupArray = new Array(businessArray, dreamsArray, homeArray, lifeArray, playArray);
			titleArray = new Array(businessTitle, dreamsTitle, homeTitle, lifeTitle, playTitle);
			fileArray = new Array(businessFile, dreamsFile, homeFile, lifeFile, playFile);
			fileSizeArray = new Array(businessFileSize, dreamsFileSize, homeFileSize, lifeFileSize, playFileSize);
			////trace(popupArray);
			////trace(titleArray);
			////trace(fileArray);
			////trace(fileSizeArray);
			
			if (owner != null)
			{
				owner.init();
				//owner.addText("owner != null");
			}
		}
		
		private function processConfigXML(e:Event):void
		{
			if (owner != null)
			{
				//owner.addText("processConfigXML");
			}
			configXML = new XML(e.target.data);
			
			//initialize game vars
			if (configXML.HideMouse == "yes")
			{
				hideMouse = true;
			}
			else
			{
				hideMouse = false;
			}
			if (owner != null)
			{
				//owner.addText("HideMouse");
			}
			
			if(configXML.InNativeWindow == "yes")
			{
				isNativeWindow = true;
			}
			else
			{
				isNativeWindow = false;
			}
			
			if (owner != null)
			{
				//owner.addText("InNativeWindow");
			}
			if(configXML.UseTuio == "yes")
			{
				useTuio = true;
			}
			else
			{
				useTuio = false;
			}
			
			if (owner != null)
			{
				//owner.addText("************** " + useTuio);
			}
			
			if(configXML.UseHardwareAcceleration == "yes")
			{
				useAcceleration = true;
			}
			else
			{
				useAcceleration = false;
			}
			
			if (owner != null)
			{
				//owner.addText("UseHardwareAcceleration");
			}
			
			if(configXML.checkCMS == "yes")
			{
				checkCMS = true;
			}
			else
			{
				checkCMS = false;
			}
			
			if (owner != null)
			{
				//owner.addText("UseHardwareAcceleration");
			}
			
			if(configXML.ShowDrawApp == "yes")
			{
				showDrawApp = true;
			}
			else
			{
				showDrawApp = false;
			}
			
			if (configXML.BigGamesInPopups == "yes")
			{
				showBigGamesInPopups = true;
			}
			else
			{
				showBigGamesInPopups = false;
			}
			
			if (owner != null)
			{
				//owner.addText("ShowDrawApp");
			}
			
			for (var i:int = 0; i < configXML.Icons.Icon.length(); i++)
			{
				iconArray = new Array(configXML.Icons.Icon[i].@name, configXML.Icons.Icon[i]);
				iconsArray.push(iconArray);
			}
			if
			(owner != null)
			{
				//owner.addText(iconsArray.toString());
			}
			
			for (var j:int = 0; j < configXML.Speeds.Speed.length(); j++)
			{
				speedArray.push(configXML.Speeds.Speed[j]);
			}
			
			if (owner != null)
			{
				//owner.addText(speedArray.toString());
			}
			
			whichMode = configXML.WhatMode;
			
			saverTimeout = configXML.SaverTimeout;
			
			waterSound = configXML.Paths.Sounds.WaterSound;
			
			
		
			businessVidEnglish = configXML.BigGames.BigBusiness.EnglishPath;
			businessVidSpanish = configXML.BigGames.BigBusiness.SpanishPath;
			
			if(configXML.BigGames.BigBusiness.@showEnglish == "yes")
			{
				showBEVid = true;
			}
			else
			{
				showBEVid = false;
			}
			if(configXML.BigGames.BigBusiness.@showSpanish == "yes")
			{
				showBSVid = true;
			}
			else
			{
				showBSVid = false;
			}
		
			homeVidEnglish = configXML.BigGames.BigHome.EnglishPath;
			homeVidSpanish = configXML.BigGames.BigHome.SpanishPath;
			
			if(configXML.BigGames.BigHome.@showEnglish == "yes")
			{
				showHEVid = true;
			}
			else
			{
				showHEVid = false;
			}
			if(configXML.BigGames.BigHome.@showSpanish == "yes")
			{
				showHSVid = true;
			}
			else
			{
				showHSVid = false;
			}
			
			creditAnswersPopIn = configXML.Paths.Sounds.CreditAnswersPopIn;
			creditCorrectRound1 = configXML.Paths.Sounds.CreditCorrectRound1;
			creditCorrectRound2 = configXML.Paths.Sounds.CreditCorrectRound2;
			creditFeedbackScreensRound1 = configXML.Paths.Sounds.CreditFeedbackScreensRound1;
			creditIncorrectRound1 = configXML.Paths.Sounds.CreditIncorrectRound1;
			creditIncorrectRound2 = configXML.Paths.Sounds.CreditIncorrectRound2;
			creditIntro_and_QuestionMusicRound2 = configXML.Paths.Sounds.CreditIntro_and_QuestionMusicRound2;
			creditIntroMusicStartGame = configXML.Paths.Sounds.CreditIntroMusicStartGame;
			creditMeter = configXML.Paths.Sounds.CreditMeter;
			creditNosedive = configXML.Paths.Sounds.CreditNosedive;
			creditPressAnswerRound1 = configXML.Paths.Sounds.CreditPressAnswerRound1;
			creditQuestionMusicRound1 = configXML.Paths.Sounds.CreditQuestionMusicRound1;
			creditQuestionSlideIn = configXML.Paths.Sounds.CreditQuestionSlideIn;
			creditTimeRanOut = configXML.Paths.Sounds.CreditTimeRanOut;
			creditWinner = configXML.Paths.Sounds.CreditWinner;
			saverButtonClick = configXML.Paths.Sounds.SaverButtonClick;
			saverCar = configXML.Paths.Sounds.SaverCar;
			saverCloudPopIn = configXML.Paths.Sounds.SaverCloudPopIn;
			saverCollege = configXML.Paths.Sounds.SaverCollege;
			saverGambler = configXML.Paths.Sounds.SaverGambler;
			saverGoGetter = configXML.Paths.Sounds.SaverGoGetter;
			saverHome = configXML.Paths.Sounds.SaverHome;
			saverMoneyMeter = configXML.Paths.Sounds.SaverMoneyMeter;
			saverRetirement = configXML.Paths.Sounds.SaverRetirement;
			saverTortoise = configXML.Paths.Sounds.SaverTortoise;
			saverVacation = configXML.Paths.Sounds.SaverVacation;
			saverWardrobe = configXML.Paths.Sounds.SaverWardrobe;
			saverWedding = configXML.Paths.Sounds.SaverWedding;
			saverQuizMusic = configXML.Paths.Sounds.SaverQuizMusic;
			saverMoneyRolling = configXML.Paths.Sounds.MoneyRolling;
			saverScaleShortVersion = configXML.Paths.Sounds.ScaleShortVersion;
			
			
			VO2Sound = configXML.Paths.VO.VO2;
			//trace("loadXML VO2Sound = " + VO2Sound);
			VO3Sound = configXML.Paths.VO.VO3;
			VO4Sound = configXML.Paths.VO.VO4;
			VO6Sound = configXML.Paths.VO.VO6;
			VO6aSound = configXML.Paths.VO.VO6a;
			VO6bSound = configXML.Paths.VO.VO6b;
			VO7Sound = configXML.Paths.VO.VO7;
			VO9Sound = configXML.Paths.VO.VO9;
			VO9aSound = configXML.Paths.VO.VO9a;
			VO9bSound = configXML.Paths.VO.VO9b;
			VO10Sound = configXML.Paths.VO.VO10;
			VO12Sound = configXML.Paths.VO.VO12;
			VO12aSound = configXML.Paths.VO.VO12a;
			VO12bSound = configXML.Paths.VO.VO12b;
			VO13Sound = configXML.Paths.VO.VO13;
			VO15Sound = configXML.Paths.VO.VO15;
			VO15aSound = configXML.Paths.VO.VO15a;
			VO15bSound = configXML.Paths.VO.VO15b;
			VO16Sound = configXML.Paths.VO.VO16;
			VO18Sound = configXML.Paths.VO.VO18;
			VO18aSound = configXML.Paths.VO.VO18a;
			VO18bSound = configXML.Paths.VO.VO18b;
			VO19Sound = configXML.Paths.VO.VO19;
			VO20Sound = configXML.Paths.VO.VO20;
			VO21Sound = configXML.Paths.VO.VO21;
			VO22Sound = configXML.Paths.VO.VO22;
			VO24aSound = configXML.Paths.VO.VO24a;
			VO24bSound = configXML.Paths.VO.VO24b;
			VO25Sound = configXML.Paths.VO.VO25;
			sing1Sound = configXML.Paths.VO.S1;
			sing2Sound = configXML.Paths.VO.S2;
			sing3Sound = configXML.Paths.VO.S3;
			sing4Sound = configXML.Paths.VO.S4;
			sing5Sound = configXML.Paths.VO.S5;
			
			screenWidth = configXML.ScreenWidth;
			screenHeight = configXML.ScreenHeight;
			numberOfScreens = configXML.NumberOfScreens;
			mortgageRateURL = configXML.MortgageRateURL;
			if (owner != null)
			{
				//owner.addText("mortgageRateURL");
			}
			
			mortgageRateToday = configXML.TodaysRates;
			mortgageRateRate = configXML.Rate;
			mortgageRateAPR = configXML.APR;
			mortgageRatePoints = configXML.Points;
			if (owner != null)
			{
				//owner.addText("mortgageRatePoints");
			}
			
			equityRateTableString = configXML.Strings.EquityRateTable;
			mortgageRateTableString = configXML.Strings.MortgageRateTable;
			mortgageCalculatorString = configXML.Strings.MortgageCalculator;
			doodleCanvasString = configXML.Strings.DoodleCanvas;
			billBlasterString = configXML.Strings.BillBlaster;
			checkBounceString = configXML.Strings.CheckBounce;
			platformerString = configXML.Strings.Platformer;
			buckBreakerString = configXML.Strings.BuckBreaker;
			if (owner != null)
			{
				//owner.addText("buckBreakerString");
			}
			
			picAlpha = configXML.Background.PicAlpha;
			trans = configXML.Background.Trans;
			del = configXML.Background.Delay;
			picTime = configXML.Background.PicTime;
			maskSpeed = configXML.Background.MaskSpeed;
			maskStart1 = configXML.Background.Mask1Start;
			maskStart2 = configXML.Background.Mask2Start;
			wordsStart1 = configXML.Background.Words1Start;
			wordsStart2 = configXML.Background.Words2Start;
			wordsAlpha = configXML.Background.WordsAlpha;
			//trace(maskSpeed + " = maskSpeed");
			
			startEnglish = configXML.Strings.BigLife.StartGame.@english;
			startSpanish = configXML.Strings.BigLife.StartGame.@spanish;
			//trace("*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^&*^*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^*&^&*^");
			
			//trace("startEnglish = " + startEnglish);
			//trace("startSpanish = " + startSpanish);
			
			credit2Speed = configXML.CreditGame.BubbleSpeed;
			
			
			lifePlayer = new Array(configXML.Strings.BigLife.Player.@english, configXML.Strings.BigLife.Player.@spanish);
			
			lifeWins = new Array(configXML.Strings.BigLife.Wins.@english, configXML.Strings.BigLife.Wins.@spanish);
			lifeTies = new Array(configXML.Strings.BigLife.Ties.@english, configXML.Strings.BigLife.Ties.@spanish);
			lifeFinal = new Array(configXML.Strings.BigLife.Final.@english, configXML.Strings.BigLife.Final.@spanish);
			lifeHomeEquity = new Array(configXML.Strings.BigLife.HomeEquity.@english, configXML.Strings.BigLife.HomeEquity.@spanish);
			lifeReserveCredit = new Array(configXML.Strings.BigLife.ReserveCredit.@english, configXML.Strings.BigLife.ReserveCredit.@spanish);
			lifeCreditCard = new Array(configXML.Strings.BigLife.CreditCard.@english, configXML.Strings.BigLife.CreditCard.@spanish);
			lifeCarLoan = new Array(configXML.Strings.BigLife.CarLoan.@english, configXML.Strings.BigLife.CarLoan.@spanish);
			lifeMortgage = new Array(configXML.Strings.BigLife.Mortgage.@english, configXML.Strings.BigLife.Mortgage.@spanish);
			lifeWelcome = new Array(configXML.Strings.BigLife.Welcome.@english, configXML.Strings.BigLife.Welcome.@spanish);
			
			lifeStartGame = new Array(configXML.Strings.BigLife.StartGame.@english, configXML.Strings.BigLife.StartGame.@spanish);
			//trace("lifeStartGame = " + lifeStartGame);
			lifeMainTitle1 = new Array(configXML.Strings.BigLife.MainTitle1.@english, configXML.Strings.BigLife.MainTitle1.@spanish);
			//trace("lifeMainTitle1 = " + lifeMainTitle1);
			lifeMainTitle2 = new Array(configXML.Strings.BigLife.MainTitle2.@english, configXML.Strings.BigLife.MainTitle2.@spanish);
			//trace("lifeMainTitle2 = " + lifeMainTitle2);
			lifeImproveCreditScore = new Array(configXML.Strings.BigLife.ImproveCreditScore.@english, configXML.Strings.BigLife.ImproveCreditScore.@spanish);
			lifeAGameToTest = new Array(configXML.Strings.BigLife.AGameToTest.@english, configXML.Strings.BigLife.AGameToTest.@spanish);
			lifeJoinGame = new Array(configXML.Strings.BigLife.JoinGame.@english, configXML.Strings.BigLife.JoinGame.@spanish);
			lifeRound = new Array(configXML.Strings.BigLife.Round.@english, configXML.Strings.BigLife.Round.@spanish);
			
			lifeSample = new Array(configXML.Strings.BigLife.Sample.@english, configXML.Strings.BigLife.Sample.@spanish);
			lifeR1SampleQuestion = new Array(configXML.Strings.BigLife.R1SampleQuestion.@english, configXML.Strings.BigLife.R1SampleQuestion.@spanish);
			lifeR1SampleAnswer1 = new Array(configXML.Strings.BigLife.R1SampleAnswer1.@english, configXML.Strings.BigLife.R1SampleAnswer1.@spanish);
			lifeR1SampleAnswer2 = new Array(configXML.Strings.BigLife.R1SampleAnswer2.@english, configXML.Strings.BigLife.R1SampleAnswer2.@spanish);
			lifeR1SampleAnswer3 = new Array(configXML.Strings.BigLife.R1SampleAnswer3.@english, configXML.Strings.BigLife.R1SampleAnswer3.@spanish);
			lifeR1SampleAnswer4 = new Array(configXML.Strings.BigLife.R1SampleAnswer4.@english, configXML.Strings.BigLife.R1SampleAnswer4.@spanish);
			
			lifeR1Question = new Array(configXML.Strings.BigLife.R1Question.@english, configXML.Strings.BigLife.R1Question.@spanish);
			lifeR1Question1 = new Array(configXML.Strings.BigLife.R1Question1.@english, configXML.Strings.BigLife.R1Question1.@spanish);
			lifeR1Q1Answer1 = new Array(configXML.Strings.BigLife.R1Q1Answer1.@english, configXML.Strings.BigLife.R1Q1Answer1.@spanish);
			lifeR1Q1Answer2 = new Array(configXML.Strings.BigLife.R1Q1Answer2.@english, configXML.Strings.BigLife.R1Q1Answer2.@spanish);
			lifeR1Q1Answer3 = new Array(configXML.Strings.BigLife.R1Q1Answer3.@english, configXML.Strings.BigLife.R1Q1Answer3.@spanish);
			lifeR1Q1Answer4 = new Array(configXML.Strings.BigLife.R1Q1Answer4.@english, configXML.Strings.BigLife.R1Q1Answer4.@spanish);
			lifeR1Q1CorrectAnswer = new Array(configXML.Strings.BigLife.R1Q1CorrectAnswer.@english, configXML.Strings.BigLife.R1Q1CorrectAnswer.@spanish);
			lifeR1Q1AnswerExplanation1 = new Array(configXML.Strings.BigLife.R1Q1AnswerExplanation1.@english, configXML.Strings.BigLife.R1Q1AnswerExplanation1.@spanish);
			lifeR1Q1AnswerExplanation2 = new Array(configXML.Strings.BigLife.R1Q1AnswerExplanation2.@english, configXML.Strings.BigLife.R1Q1AnswerExplanation2.@spanish);
			
			lifeR1Question2 = new Array(configXML.Strings.BigLife.R1Question2.@english, configXML.Strings.BigLife.R1Question2.@spanish);
			lifeR1Q2Answer1 = new Array(configXML.Strings.BigLife.R1Q2Answer1.@english, configXML.Strings.BigLife.R1Q2Answer1.@spanish);
			lifeR1Q2Answer2 = new Array(configXML.Strings.BigLife.R1Q2Answer2.@english, configXML.Strings.BigLife.R1Q2Answer2.@spanish);
			lifeR1Q2Answer3 = new Array(configXML.Strings.BigLife.R1Q2Answer3.@english, configXML.Strings.BigLife.R1Q2Answer3.@spanish);
			lifeR1Q2Answer4 = new Array(configXML.Strings.BigLife.R1Q2Answer4.@english, configXML.Strings.BigLife.R1Q2Answer4.@spanish);
			lifeR1Q2CorrectAnswer = new Array(configXML.Strings.BigLife.R1Q2CorrectAnswer.@english, configXML.Strings.BigLife.R1Q2CorrectAnswer.@spanish);
			lifeR1Q2AnswerExplanation1 = new Array(configXML.Strings.BigLife.R1Q2AnswerExplanation1.@english, configXML.Strings.BigLife.R1Q2AnswerExplanation1.@spanish);
			lifeR1Q2AnswerExplanation2 = new Array(configXML.Strings.BigLife.R1Q2AnswerExplanation2.@english, configXML.Strings.BigLife.R1Q2AnswerExplanation2.@spanish);
			
			lifeR1Question3 = new Array(configXML.Strings.BigLife.R1Question3.@english, configXML.Strings.BigLife.R1Question3.@spanish);
			lifeR1Q3Answer1 = new Array(configXML.Strings.BigLife.R1Q3Answer1.@english, configXML.Strings.BigLife.R1Q3Answer1.@spanish);
			lifeR1Q3Answer2 = new Array(configXML.Strings.BigLife.R1Q3Answer2.@english, configXML.Strings.BigLife.R1Q3Answer2.@spanish);
			lifeR1Q3Answer3 = new Array(configXML.Strings.BigLife.R1Q3Answer3.@english, configXML.Strings.BigLife.R1Q3Answer3.@spanish);
			lifeR1Q3Answer4 = new Array(configXML.Strings.BigLife.R1Q3Answer4.@english, configXML.Strings.BigLife.R1Q3Answer4.@spanish);
			lifeR1Q3CorrectAnswer = new Array(configXML.Strings.BigLife.R1Q3CorrectAnswer.@english, configXML.Strings.BigLife.R1Q3CorrectAnswer.@spanish);
			lifeR1Q3AnswerExplanation1 = new Array(configXML.Strings.BigLife.R1Q3AnswerExplanation1.@english, configXML.Strings.BigLife.R1Q3AnswerExplanation1.@spanish);
			lifeR1Q3AnswerExplanation2 = new Array(configXML.Strings.BigLife.R1Q3AnswerExplanation2.@english, configXML.Strings.BigLife.R1Q3AnswerExplanation2.@spanish);
			lifeR1Q3AnswerExplanation3 = new Array(configXML.Strings.BigLife.R1Q3AnswerExplanation3.@english, configXML.Strings.BigLife.R1Q3AnswerExplanation3.@spanish);
			
			lifeR1Question4 = new Array(configXML.Strings.BigLife.R1Question4.@english, configXML.Strings.BigLife.R1Question4.@spanish);
			lifeR1Q4Answer1 = new Array(configXML.Strings.BigLife.R1Q4Answer1.@english, configXML.Strings.BigLife.R1Q4Answer1.@spanish);
			lifeR1Q4Answer2 = new Array(configXML.Strings.BigLife.R1Q4Answer2.@english, configXML.Strings.BigLife.R1Q4Answer2.@spanish);
			lifeR1Q4Answer3 = new Array(configXML.Strings.BigLife.R1Q4Answer3.@english, configXML.Strings.BigLife.R1Q4Answer3.@spanish);
			lifeR1Q4Answer4 = new Array(configXML.Strings.BigLife.R1Q4Answer4.@english, configXML.Strings.BigLife.R1Q4Answer4.@spanish);
			lifeR1Q4CorrectAnswer = new Array(configXML.Strings.BigLife.R1Q4CorrectAnswer.@english, configXML.Strings.BigLife.R1Q4CorrectAnswer.@spanish);
			lifeR1Q4AnswerExplanation1 = new Array(configXML.Strings.BigLife.R1Q4AnswerExplanation1.@english, configXML.Strings.BigLife.R1Q4AnswerExplanation1.@spanish);
			lifeR1Q4AnswerExplanation2 = new Array(configXML.Strings.BigLife.R1Q4AnswerExplanation2.@english, configXML.Strings.BigLife.R1Q4AnswerExplanation2.@spanish);
			
			lifeR1Question5 = new Array(configXML.Strings.BigLife.R1Question5.@english, configXML.Strings.BigLife.R1Question5.@spanish);
			lifeR1Q5Answer1 = new Array(configXML.Strings.BigLife.R1Q5Answer1.@english, configXML.Strings.BigLife.R1Q5Answer1.@spanish);
			lifeR1Q5Answer2 = new Array(configXML.Strings.BigLife.R1Q5Answer2.@english, configXML.Strings.BigLife.R1Q5Answer2.@spanish);
			lifeR1Q5Answer3 = new Array(configXML.Strings.BigLife.R1Q5Answer3.@english, configXML.Strings.BigLife.R1Q5Answer3.@spanish);
			lifeR1Q5Answer4 = new Array(configXML.Strings.BigLife.R1Q5Answer4.@english, configXML.Strings.BigLife.R1Q5Answer4.@spanish);
			lifeR1Q5CorrectAnswer = new Array(configXML.Strings.BigLife.R1Q5CorrectAnswer.@english, configXML.Strings.BigLife.R1Q5CorrectAnswer.@spanish);
			lifeR1Q5AnswerExplanation1 = new Array(configXML.Strings.BigLife.R1Q5AnswerExplanation1.@english, configXML.Strings.BigLife.R1Q5AnswerExplanation1.@spanish);
			lifeR1Q5AnswerExplanation2 = new Array(configXML.Strings.BigLife.R1Q5AnswerExplanation2.@english, configXML.Strings.BigLife.R1Q5AnswerExplanation2.@spanish);
			lifeR1Q5AnswerExplanation3 = new Array(configXML.Strings.BigLife.R1Q5AnswerExplanation3.@english, configXML.Strings.BigLife.R1Q5AnswerExplanation3.@spanish);
			
			lifeR2SampleQuestion = new Array(configXML.Strings.BigLife.R2SampleQuestion.@english, configXML.Strings.BigLife.R2SampleQuestion.@spanish);
			lifeR2SampleAnswer1 = new Array(configXML.Strings.BigLife.R2SampleAnswer1.@english, configXML.Strings.BigLife.R2SampleAnswer1.@spanish);
			lifeR2SampleAnswer2 = new Array(configXML.Strings.BigLife.R2SampleAnswer2.@english, configXML.Strings.BigLife.R2SampleAnswer2.@spanish);
			lifeR2SampleAnswer3 = new Array(configXML.Strings.BigLife.R2SampleAnswer3.@english, configXML.Strings.BigLife.R2SampleAnswer3.@spanish);
			lifeR2SampleAnswer4 = new Array(configXML.Strings.BigLife.R2SampleAnswer4.@english, configXML.Strings.BigLife.R2SampleAnswer4.@spanish);
			lifeR2SampleAnswer5 = new Array(configXML.Strings.BigLife.R2SampleAnswer5.@english, configXML.Strings.BigLife.R2SampleAnswer5.@spanish);
			lifeR2SampleAnswer6 = new Array(configXML.Strings.BigLife.R2SampleAnswer6.@english, configXML.Strings.BigLife.R2SampleAnswer6.@spanish);
			
			lifeR2Question1 = new Array(configXML.Strings.BigLife.R2Question1.@english, configXML.Strings.BigLife.R2Question1.@spanish);
			lifeR2Q1Answer1 = new Array(configXML.Strings.BigLife.R2Q1Answer1.@english, configXML.Strings.BigLife.R2Q1Answer1.@spanish);
			lifeR2Q1Answer2 = new Array(configXML.Strings.BigLife.R2Q1Answer2.@english, configXML.Strings.BigLife.R2Q1Answer2.@spanish);
			lifeR2Q1Answer3 = new Array(configXML.Strings.BigLife.R2Q1Answer3.@english, configXML.Strings.BigLife.R2Q1Answer3.@spanish);
			lifeR2Q1Answer4 = new Array(configXML.Strings.BigLife.R2Q1Answer4.@english, configXML.Strings.BigLife.R2Q1Answer4.@spanish);
			lifeR2Q1Answer5 = new Array(configXML.Strings.BigLife.R2Q1Answer5.@english, configXML.Strings.BigLife.R2Q1Answer5.@spanish);
			lifeR2Q1Answer6 = new Array(configXML.Strings.BigLife.R2Q1Answer6.@english, configXML.Strings.BigLife.R2Q1Answer6.@spanish);
			lifeR2Q1Answer7 = new Array(configXML.Strings.BigLife.R2Q1Answer7.@english, configXML.Strings.BigLife.R2Q1Answer7.@spanish);
			lifeR2Q1Answer8 = new Array(configXML.Strings.BigLife.R2Q1Answer8.@english, configXML.Strings.BigLife.R2Q1Answer8.@spanish);
			lifeR2Q1Answer9 = new Array(configXML.Strings.BigLife.R2Q1Answer9.@english, configXML.Strings.BigLife.R2Q1Answer9.@spanish);
			lifeR2Q1Answer10 = new Array(configXML.Strings.BigLife.R2Q1Answer10.@english, configXML.Strings.BigLife.R2Q1Answer10.@spanish);
			lifeR2Q1Answer11 = new Array(configXML.Strings.BigLife.R2Q1Answer11.@english, configXML.Strings.BigLife.R2Q1Answer11.@spanish);
			lifeR2Q1Answer12 = new Array(configXML.Strings.BigLife.R2Q1Answer12.@english, configXML.Strings.BigLife.R2Q1Answer12.@spanish);
			
			lifeR2Question2 = new Array(configXML.Strings.BigLife.R2Question2.@english, configXML.Strings.BigLife.R2Question2.@spanish);
			lifeR2Q2Answer1 = new Array(configXML.Strings.BigLife.R2Q2Answer1.@english, configXML.Strings.BigLife.R2Q2Answer1.@spanish);
			lifeR2Q2Answer2 = new Array(configXML.Strings.BigLife.R2Q2Answer2.@english, configXML.Strings.BigLife.R2Q2Answer2.@spanish);
			lifeR2Q2Answer3 = new Array(configXML.Strings.BigLife.R2Q2Answer3.@english, configXML.Strings.BigLife.R2Q2Answer3.@spanish);
			lifeR2Q2Answer4 = new Array(configXML.Strings.BigLife.R2Q2Answer4.@english, configXML.Strings.BigLife.R2Q2Answer4.@spanish);
			lifeR2Q2Answer5 = new Array(configXML.Strings.BigLife.R2Q2Answer5.@english, configXML.Strings.BigLife.R2Q2Answer5.@spanish);
			lifeR2Q2Answer6 = new Array(configXML.Strings.BigLife.R2Q2Answer6.@english, configXML.Strings.BigLife.R2Q2Answer6.@spanish);
			
			lifeR2Question3 = new Array(configXML.Strings.BigLife.R2Question3.@english, configXML.Strings.BigLife.R2Question3.@spanish);
			lifeR2Q3Answer1 = new Array(configXML.Strings.BigLife.R2Q3Answer1.@english, configXML.Strings.BigLife.R2Q3Answer1.@spanish);
			lifeR2Q3Answer2 = new Array(configXML.Strings.BigLife.R2Q3Answer2.@english, configXML.Strings.BigLife.R2Q3Answer2.@spanish);
			lifeR2Q3Answer3 = new Array(configXML.Strings.BigLife.R2Q3Answer3.@english, configXML.Strings.BigLife.R2Q3Answer3.@spanish);
			lifeR2Q3Answer4 = new Array(configXML.Strings.BigLife.R2Q3Answer4.@english, configXML.Strings.BigLife.R2Q3Answer4.@spanish);
			lifeR2Q3Answer5 = new Array(configXML.Strings.BigLife.R2Q3Answer5.@english, configXML.Strings.BigLife.R2Q3Answer5.@spanish);
			lifeR2Q3Answer6 = new Array(configXML.Strings.BigLife.R2Q3Answer6.@english, configXML.Strings.BigLife.R2Q3Answer6.@spanish);
			lifeR2Q3Answer7 = new Array(configXML.Strings.BigLife.R2Q3Answer7.@english, configXML.Strings.BigLife.R2Q3Answer7.@spanish);
			lifeR2Q3Answer8 = new Array(configXML.Strings.BigLife.R2Q3Answer8.@english, configXML.Strings.BigLife.R2Q3Answer8.@spanish);
			
			dreamIntro1 = new Array(configXML.Strings.BigDreams.Intro1.@english, configXML.Strings.BigDreams.Intro1.@spanish);
			dreamIntro2 = new Array(configXML.Strings.BigDreams.Intro2.@english, configXML.Strings.BigDreams.Intro2.@spanish);
			dreamClickHere = new Array(configXML.Strings.BigDreams.ClickHere.@english, configXML.Strings.BigDreams.ClickHere.@spanish);
			dreamWantToSave = new Array(configXML.Strings.BigDreams.WantToSave.@english, configXML.Strings.BigDreams.WantToSave.@spanish);
			dreamClickStart = new Array(configXML.Strings.BigDreams.ClickStart.@english, configXML.Strings.BigDreams.ClickStart.@spanish);
			dreamEnglishOrSpanish = new Array(configXML.Strings.BigDreams.EnglishOrSpanish.@english, configXML.Strings.BigDreams.EnglishOrSpanish.@spanish);
			dreamQuestion1 = new Array(configXML.Strings.BigDreams.Question1.@english, configXML.Strings.BigDreams.Question1.@spanish);
			dreamYes = new Array(configXML.Strings.BigDreams.Yes.@english, configXML.Strings.BigDreams.Yes.@spanish);
			dreamNo = new Array(configXML.Strings.BigDreams.No.@english, configXML.Strings.BigDreams.No.@spanish);
			dreamMidtermSavingsGoal = new Array(configXML.Strings.BigDreams.MidtermSavingsGoal.@english, configXML.Strings.BigDreams.MidtermSavingsGoal.@spanish);
			dreamQ1Info1 = new Array(configXML.Strings.BigDreams.Q1Info1.@english, configXML.Strings.BigDreams.Q1Info1.@spanish);
			dreamQ1Info2 = new Array(configXML.Strings.BigDreams.Q1Info2.@english, configXML.Strings.BigDreams.Q1Info2.@spanish);
			dreamQ1Info3 = new Array(configXML.Strings.BigDreams.Q1Info3.@english, configXML.Strings.BigDreams.Q1Info3.@spanish);
			dreamQ1Info4 = new Array(configXML.Strings.BigDreams.Q1Info4.@english, configXML.Strings.BigDreams.Q1Info4.@spanish);
			dreamQ1Dictionary = new Array(configXML.Strings.BigDreams.Q1Dictionary.@english, configXML.Strings.BigDreams.Q1Dictionary.@spanish);
			dreamQuestion2 = new Array(configXML.Strings.BigDreams.Question2.@english, configXML.Strings.BigDreams.Question2.@spanish);
			dreamShortTermSavingsGoal = new Array(configXML.Strings.BigDreams.ShortTermSavingsGoal.@english, configXML.Strings.BigDreams.ShortTermSavingsGoal.@spanish);
			dreamQ2Info1 = new Array(configXML.Strings.BigDreams.Q2Info1.@english, configXML.Strings.BigDreams.Q2Info1.@spanish);
			dreamQ2Info2 = new Array(configXML.Strings.BigDreams.Q2Info2.@english, configXML.Strings.BigDreams.Q2Info2.@spanish);
			dreamQ2Info3 = new Array(configXML.Strings.BigDreams.Q2Info3.@english, configXML.Strings.BigDreams.Q2Info3.@spanish);
			dreamQuestion3 = new Array(configXML.Strings.BigDreams.Question3.@english, configXML.Strings.BigDreams.Question3.@spanish);
			dreamLongTermSavingsGoal = new Array(configXML.Strings.BigDreams.LongTermSavingsGoal.@english, configXML.Strings.BigDreams.LongTermSavingsGoal.@spanish);
			dreamQ3Info1 = new Array(configXML.Strings.BigDreams.Q3Info1.@english, configXML.Strings.BigDreams.Q3Info1.@spanish);
			dreamQ3Info2 = new Array(configXML.Strings.BigDreams.Q3Info2.@english, configXML.Strings.BigDreams.Q3Info2.@spanish);
			dreamQ3Info3 = new Array(configXML.Strings.BigDreams.Q3Info3.@english, configXML.Strings.BigDreams.Q3Info3.@spanish);
			dreamQ3Info4 = new Array(configXML.Strings.BigDreams.Q3Info4.@english, configXML.Strings.BigDreams.Q3Info4.@spanish);
			
			dreamQ3Dictionary = new Array(configXML.Strings.BigDreams.Q3Info1.@english, configXML.Strings.BigDreams.Q3Info1.@spanish);
			dreamQuestion4 = new Array(configXML.Strings.BigDreams.Question4.@english, configXML.Strings.BigDreams.Question4.@spanish);
			dreamQ4Info1 = new Array(configXML.Strings.BigDreams.Q4Info1.@english, configXML.Strings.BigDreams.Q4Info1.@spanish);
			dreamQ4Info2 = new Array(configXML.Strings.BigDreams.Q4Info2.@english, configXML.Strings.BigDreams.Q4Info2.@spanish);
			dreamQuestion5 = new Array(configXML.Strings.BigDreams.Question5.@english, configXML.Strings.BigDreams.Question5.@spanish);
			dreamQ5Info1 = new Array(configXML.Strings.BigDreams.Q5Info1.@english, configXML.Strings.BigDreams.Q5Info1.@spanish);
			dreamQ5Info2 = new Array(configXML.Strings.BigDreams.Q5Info2.@english, configXML.Strings.BigDreams.Q5Info2.@spanish);
			dreamQuestion6 = new Array(configXML.Strings.BigDreams.Question6.@english, configXML.Strings.BigDreams.Question6.@spanish);
			dreamQ6Info1 = new Array(configXML.Strings.BigDreams.Q6Info1.@english, configXML.Strings.BigDreams.Q6Info1.@spanish);
			dreamQ6Info2 = new Array(configXML.Strings.BigDreams.Q6Info2.@english, configXML.Strings.BigDreams.Q6Info2.@spanish);
			dreamQ6Info3 = new Array(configXML.Strings.BigDreams.Q6Info3.@english, configXML.Strings.BigDreams.Q6Info3.@spanish);
			dreamQuestion7 = new Array(configXML.Strings.BigDreams.Question7.@english, configXML.Strings.BigDreams.Question7.@spanish);
			dreamQ7Info1 = new Array(configXML.Strings.BigDreams.Q7Info1.@english, configXML.Strings.BigDreams.Q7Info1.@spanish);
			dreamQ7Info2 = new Array(configXML.Strings.BigDreams.Q7Info2.@english, configXML.Strings.BigDreams.Q7Info2.@spanish);
			dreamQ7Info3 = new Array(configXML.Strings.BigDreams.Q7Info3.@english, configXML.Strings.BigDreams.Q7Info3.@spanish);
			dreamQ7Dictionary1 = new Array(configXML.Strings.BigDreams.Q7Dictionary1.@english, configXML.Strings.BigDreams.Q7Dictionary1.@spanish);
			dreamQ7Dictionary2 = new Array(configXML.Strings.BigDreams.Q7Dictionary2.@english, configXML.Strings.BigDreams.Q7Dictionary2.@spanish);
			dreamQ7DIctionary3 = new Array(configXML.Strings.BigDreams.Q7Dictionary3.@english, configXML.Strings.BigDreams.Q7Dictionary3.@spanish);
			dreamSavingHabits = new Array(configXML.Strings.BigDreams.SavingHabits.@english, configXML.Strings.BigDreams.SavingHabits.@spanish);
			dreamSHClickHere = new Array(configXML.Strings.BigDreams.SHClickHere.@english, configXML.Strings.BigDreams.SHClickHere.@spanish);
			dreamTouchTheChoice = new Array(configXML.Strings.BigDreams.TouchTheChoice.@english, configXML.Strings.BigDreams.TouchTheChoice.@spanish);
			dreamQuestionNumber = new Array(configXML.Strings.BigDreams.QuestionNumber.@english, configXML.Strings.BigDreams.QuestionNumber.@spanish);
			dreamSHQuestion1 = new Array(configXML.Strings.BigDreams.SHQuestion1.@english, configXML.Strings.BigDreams.SHQuestion1.@spanish);
			dreamSHQ1AnswerA = new Array(configXML.Strings.BigDreams.SHQ1AnswerA.@english, configXML.Strings.BigDreams.SHQ1AnswerA.@spanish);
			dreamSHQ1AnswerB = new Array(configXML.Strings.BigDreams.SHQ1AnswerB.@english, configXML.Strings.BigDreams.SHQ1AnswerB.@spanish);
			dreamSHQ1AnswerC = new Array(configXML.Strings.BigDreams.SHQ1AnswerC.@english, configXML.Strings.BigDreams.SHQ1AnswerC.@spanish);
			dreamSHQuestion2 = new Array(configXML.Strings.BigDreams.SHQuestion2.@english, configXML.Strings.BigDreams.SHQuestion2.@spanish);
			dreamSHQ2AnswerA = new Array(configXML.Strings.BigDreams.SHQ2AnswerA.@english, configXML.Strings.BigDreams.SHQ2AnswerA.@spanish);
			dreamSHQ2AnswerB = new Array(configXML.Strings.BigDreams.SHQ2AnswerB.@english, configXML.Strings.BigDreams.SHQ2AnswerB.@spanish);
			dreamSHQ2AnswerC = new Array(configXML.Strings.BigDreams.SHQ2AnswerC.@english, configXML.Strings.BigDreams.SHQ2AnswerC.@spanish);
			dreamSHQuestion3 = new Array(configXML.Strings.BigDreams.SHQuestion3.@english, configXML.Strings.BigDreams.SHQuestion3.@spanish);
			dreamSHQ3AnswerA = new Array(configXML.Strings.BigDreams.SHQ3AnswerA.@english, configXML.Strings.BigDreams.SHQ3AnswerA.@spanish);
			dreamSHQ3AnswerB = new Array(configXML.Strings.BigDreams.SHQ3AnswerB.@english, configXML.Strings.BigDreams.SHQ3AnswerB.@spanish);
			dreamSHQ3AnswerC = new Array(configXML.Strings.BigDreams.SHQ3AnswerC.@english, configXML.Strings.BigDreams.SHQ3AnswerC.@spanish);
			dreamSHQuestion4 = new Array(configXML.Strings.BigDreams.SHQuestion4.@english, configXML.Strings.BigDreams.SHQuestion4.@spanish);
			dreamSHQ4AnswerA = new Array(configXML.Strings.BigDreams.SHQ4AnswerA.@english, configXML.Strings.BigDreams.SHQ4AnswerA.@spanish);
			dreamSHQ4AnswerB = new Array(configXML.Strings.BigDreams.SHQ4AnswerB.@english, configXML.Strings.BigDreams.SHQ4AnswerB.@spanish);
			dreamSHQ4AnswerC = new Array(configXML.Strings.BigDreams.SHQ4AnswerC.@english, configXML.Strings.BigDreams.SHQ4AnswerC.@spanish);
			dreamSHQuestion5 = new Array(configXML.Strings.BigDreams.SHQuestion5.@english, configXML.Strings.BigDreams.SHQuestion5.@spanish);
			dreamSHQ5AnswerA = new Array(configXML.Strings.BigDreams.SHQ5AnswerA.@english, configXML.Strings.BigDreams.SHQ5AnswerA.@spanish);
			dreamSHQ5AnswerB = new Array(configXML.Strings.BigDreams.SHQ5AnswerB.@english, configXML.Strings.BigDreams.SHQ5AnswerB.@spanish);
			dreamSHQ5AnswerC = new Array(configXML.Strings.BigDreams.SHQ5AnswerC.@english, configXML.Strings.BigDreams.SHQ5AnswerC.@spanish);
			dreamSHQ5Dictionary1 = new Array(configXML.Strings.BigDreams.SHQ5Dictionary1.@english, configXML.Strings.BigDreams.SHQ5Dictionary1.@spanish);
			dreamSHQ5Dictionary2 = new Array(configXML.Strings.BigDreams.SHQ5Dictionary2.@english, configXML.Strings.BigDreams.SHQ5Dictionary2.@spanish);
			dreamSHQ5Dictionary3 = new Array(configXML.Strings.BigDreams.SHQ5Dictionary3.@english, configXML.Strings.BigDreams.SHQ5Dictionary3.@spanish);
			dreamSHQuestion6 = new Array(configXML.Strings.BigDreams.SHQuestion6.@english, configXML.Strings.BigDreams.SHQuestion6.@spanish);
			dreamSHQ6AnswerA = new Array(configXML.Strings.BigDreams.SHQ6AnswerA.@english, configXML.Strings.BigDreams.SHQ6AnswerA.@spanish);
			dreamSHQ6AnswerB = new Array(configXML.Strings.BigDreams.SHQ6AnswerB.@english, configXML.Strings.BigDreams.SHQ6AnswerB.@spanish);
			dreamSHQ6AnswerC = new Array(configXML.Strings.BigDreams.SHQ6AnswerC.@english, configXML.Strings.BigDreams.SHQ6AnswerC.@spanish);
			dreamSHQuestion7 = new Array(configXML.Strings.BigDreams.SHQuestion7.@english, configXML.Strings.BigDreams.SHQuestion7.@spanish);
			dreamSHQ7AnswerA = new Array(configXML.Strings.BigDreams.SHQ7AnswerA.@english, configXML.Strings.BigDreams.SHQ7AnswerA.@spanish);
			dreamSHQ7AnswerB = new Array(configXML.Strings.BigDreams.SHQ7AnswerB.@english, configXML.Strings.BigDreams.SHQ7AnswerB.@spanish);
			dreamSHQ7AnswerC = new Array(configXML.Strings.BigDreams.SHQ7AnswerC.@english, configXML.Strings.BigDreams.SHQ7AnswerC.@spanish);
			dreamSHQuestion8 = new Array(configXML.Strings.BigDreams.SHQuestion8.@english, configXML.Strings.BigDreams.SHQuestion8.@spanish);
			dreamSHQ8AnswerA = new Array(configXML.Strings.BigDreams.SHQ8AnswerA.@english, configXML.Strings.BigDreams.SHQ8AnswerA.@spanish);
			dreamSHQ8AnswerB = new Array(configXML.Strings.BigDreams.SHQ8AnswerB.@english, configXML.Strings.BigDreams.SHQ8AnswerB.@spanish);
			dreamSHQ8AnswerC = new Array(configXML.Strings.BigDreams.SHQ8AnswerC.@english, configXML.Strings.BigDreams.SHQ8AnswerC.@spanish);
			dreamSHQuestion9 = new Array(configXML.Strings.BigDreams.SHQuestion9.@english, configXML.Strings.BigDreams.SHQuestion9.@spanish);
			dreamSHQ9AnswerA = new Array(configXML.Strings.BigDreams.SHQ9AnswerA.@english, configXML.Strings.BigDreams.SHQ9AnswerA.@spanish);
			dreamSHQ9AnswerB = new Array(configXML.Strings.BigDreams.SHQ9AnswerB.@english, configXML.Strings.BigDreams.SHQ9AnswerB.@spanish);
			dreamSHQ9AnswerC = new Array(configXML.Strings.BigDreams.SHQ9AnswerC.@english, configXML.Strings.BigDreams.SHQ9AnswerC.@spanish);
			dreamSHQ9Dictionary = new Array(configXML.Strings.BigDreams.SHQ9Dictionary.@english, configXML.Strings.BigDreams.SHQ9Dictionary.@spanish);
			dreamTheTortoise = new Array(configXML.Strings.BigDreams.TheTortoise.@english, configXML.Strings.BigDreams.TheTortoise.@spanish);
			dreamTortoise1 = new Array(configXML.Strings.BigDreams.Tortoise1.@english, configXML.Strings.BigDreams.Tortoise1.@spanish);
			dreamTortoise2 = new Array(configXML.Strings.BigDreams.Tortoise2.@english, configXML.Strings.BigDreams.Tortoise2.@spanish);
			dreamTortoise3 = new Array(configXML.Strings.BigDreams.Tortoise3.@english, configXML.Strings.BigDreams.Tortoise3.@spanish);
			dreamTortoise4 = new Array(configXML.Strings.BigDreams.Tortoise4.@english, configXML.Strings.BigDreams.Tortoise4.@spanish);
			dreamTortoise5 = new Array(configXML.Strings.BigDreams.Tortoise5.@english, configXML.Strings.BigDreams.Tortoise5.@spanish);
			dreamTortoise6 = new Array(configXML.Strings.BigDreams.Tortoise6.@english, configXML.Strings.BigDreams.Tortoise6.@spanish);
			dreamCheckOutOtherEBVisitors = new Array(configXML.Strings.BigDreams.CheckOutOtherEBVisitors.@english, configXML.Strings.BigDreams.CheckOutOtherEBVisitors.@spanish);
			dreamTheGoGetter = new Array(configXML.Strings.BigDreams.TheGoGetter.@english, configXML.Strings.BigDreams.TheGoGetter.@spanish);
			dreamGoGetter1 = new Array(configXML.Strings.BigDreams.GoGetter1.@english, configXML.Strings.BigDreams.GoGetter1.@spanish);
			dreamGoGetter2 = new Array(configXML.Strings.BigDreams.GoGetter2.@english, configXML.Strings.BigDreams.GoGetter2.@spanish);
			dreamGoGetter3 = new Array(configXML.Strings.BigDreams.GoGetter3.@english, configXML.Strings.BigDreams.GoGetter3.@spanish);
			dreamGoGetter4 = new Array(configXML.Strings.BigDreams.GoGetter4.@english, configXML.Strings.BigDreams.GoGetter4.@spanish);
			dreamGoGetter5 = new Array(configXML.Strings.BigDreams.GoGetter5.@english, configXML.Strings.BigDreams.GoGetter5.@spanish);
			dreamTheGambler = new Array(configXML.Strings.BigDreams.TheGambler.@english, configXML.Strings.BigDreams.TheGambler.@spanish);
			dreamGambler1 = new Array(configXML.Strings.BigDreams.Gambler1.@english, configXML.Strings.BigDreams.Gambler1.@spanish);
			dreamGambler2 = new Array(configXML.Strings.BigDreams.Gambler2.@english, configXML.Strings.BigDreams.Gambler2.@spanish);
			dreamGambler3 = new Array(configXML.Strings.BigDreams.Gambler3.@english, configXML.Strings.BigDreams.Gambler3.@spanish);
			dreamGambler4 = new Array(configXML.Strings.BigDreams.Gambler4.@english, configXML.Strings.BigDreams.Gambler4.@spanish);
			dreamFinal1 = new Array(configXML.Strings.BigDreams.Final1.@english, configXML.Strings.BigDreams.Final1.@spanish);
			dreamFinal2 = new Array(configXML.Strings.BigDreams.Final2.@english, configXML.Strings.BigDreams.Final2.@spanish);
			dreamSavings = new Array(configXML.Strings.BigDreams.Savings.@english, configXML.Strings.BigDreams.Savings.@spanish);
			dreamSavingsAccount = new Array(configXML.Strings.BigDreams.SavingsAccount.@english, configXML.Strings.BigDreams.SavingsAccount.@spanish);
			dreamAutomaticTransfers = new Array(configXML.Strings.BigDreams.AutomaticTransfers.@english, configXML.Strings.BigDreams.AutomaticTransfers.@spanish);
			dreamMoneyMarketAccount = new Array(configXML.Strings.BigDreams.MoneyMarketAccount.@english, configXML.Strings.BigDreams.MoneyMarketAccount.@spanish);
			dreamCD = new Array(configXML.Strings.BigDreams.CD.@english, configXML.Strings.BigDreams.CD.@spanish);
			dreamRetirementPlanning = new Array(configXML.Strings.BigDreams.RetirementPlanning.@english, configXML.Strings.BigDreams.RetirementPlanning.@spanish);
			dreamInvestments = new Array(configXML.Strings.BigDreams.Investments.@english, configXML.Strings.BigDreams.Investments.@spanish);
			
			dream401K = new Array(configXML.Strings.BigDreams.K401.@english, configXML.Strings.BigDreams.K401.@spanish);
			dreamRothIRA = new Array(configXML.Strings.BigDreams.RothIRA.@english, configXML.Strings.BigDreams.RothIRA.@spanish);
			dreamBuyAHouse = new Array(configXML.Strings.BigDreams.BuyAHouse.@english, configXML.Strings.BigDreams.BuyAHouse.@spanish);
			dreamBuildCredit = new Array(configXML.Strings.BigDreams.BuildCredit.@english, configXML.Strings.BigDreams.BuildCredit.@spanish);
			dreamAutomaticSavingsPlan = new Array(configXML.Strings.BigDreams.AutomaticSavingsPlan.@english, configXML.Strings.BigDreams.AutomaticSavingsPlan.@spanish);
			dreamStocks = new Array(configXML.Strings.BigDreams.Stocks.@english, configXML.Strings.BigDreams.Stocks.@spanish);
			dreamTouchDream = new Array(configXML.Strings.BigDreams.TouchDream.@english, configXML.Strings.BigDreams.TouchDream.@spanish);
			dreamSpenderKind = new Array(configXML.Strings.BigDreams.SpenderKind.@english, configXML.Strings.BigDreams.SpenderKind.@spanish);
			dreamTouchFind = new Array(configXML.Strings.BigDreams.TouchFind.@english, configXML.Strings.BigDreams.TouchFind.@spanish);
			dreamBigDreamsSave = new Array(configXML.Strings.BigDreams.BigDreamsSave.@english, configXML.Strings.BigDreams.BigDreamsSave.@spanish);
			dreamModestDreamsSave = new Array(configXML.Strings.BigDreams.ModestDreamsSave.@english, configXML.Strings.BigDreams.ModestDreamsSave.@spanish);
			dreamSavingsTips = new Array(configXML.Strings.BigDreams.SavingsTips.@english, configXML.Strings.BigDreams.SavingsTips.@spanish);
			dreamSaving = new Array(configXML.Strings.BigDreams.Saving.@english, configXML.Strings.BigDreams.Saving.@spanish);
			dreamSpending = new Array(configXML.Strings.BigDreams.Spending.@english, configXML.Strings.BigDreams.Spending.@spanish);
			dreamWhatKindSaver = new Array(configXML.Strings.BigDreams.WhatKindSaver.@english, configXML.Strings.BigDreams.WhatKindSaver.@spanish);
			dreamWhatKindSaver1 = new Array(configXML.Strings.BigDreams.WhatKindSaver1.@english, configXML.Strings.BigDreams.WhatKindSaver1.@spanish);
			dreamWhatKindSaver2 = new Array(configXML.Strings.BigDreams.WhatKindSaver2.@english, configXML.Strings.BigDreams.WhatKindSaver2.@spanish);
			dreamHowEBRanked = new Array(configXML.Strings.BigDreams.HowEBRanked.@english, configXML.Strings.BigDreams.HowEBRanked.@spanish);
			dreamBDBS = new Array(configXML.Strings.BigDreams.BDBS.@english, configXML.Strings.BigDreams.BDBS.@spanish);
			dreamMA = new Array(configXML.Strings.BigDreams.MA.@english, configXML.Strings.BigDreams.MA.@spanish);
			dreamSS = new Array(configXML.Strings.BigDreams.SS.@english, configXML.Strings.BigDreams.SS.@spanish);
			dreamRH = new Array(configXML.Strings.BigDreams.RH.@english, configXML.Strings.BigDreams.RH.@spanish);
			dreamBH = new Array(configXML.Strings.BigDreams.BH.@english, configXML.Strings.BigDreams.BH.@spanish);
			dreamSpendStyle = new Array(configXML.Strings.BigDreams.SpendStyle.@english, configXML.Strings.BigDreams.SpendStyle.@spanish);
			dreamSaveStyle = new Array(configXML.Strings.BigDreams.SaveStyle.@english, configXML.Strings.BigDreams.SaveStyle.@spanish);
			dreamResults = new Array(configXML.Strings.BigDreams.Results.@english, configXML.Strings.BigDreams.Results.@spanish);
			dreamMismatch = new Array(configXML.Strings.BigDreams.Mismatch.@english, configXML.Strings.BigDreams.Mismatch.@spanish);
			dreamMatch = new Array(configXML.Strings.BigDreams.Match.@english, configXML.Strings.BigDreams.Match.@spanish);
			dreamMisMatch1 = new Array(configXML.Strings.BigDreams.MisMatch1.@english, configXML.Strings.BigDreams.MisMatch1.@spanish);
			dreamMisMatch2 = new Array(configXML.Strings.BigDreams.MisMatch2.@english, configXML.Strings.BigDreams.MisMatch2.@spanish);
			dreamMatch1 = new Array(configXML.Strings.BigDreams.Match1.@english, configXML.Strings.BigDreams.Match1.@spanish);
			dreamMisMatch3 = new Array(configXML.Strings.BigDreams.MisMatch3.@english, configXML.Strings.BigDreams.MisMatch3.@spanish);
			dreamMatch2 = new Array(configXML.Strings.BigDreams.Match2.@english, configXML.Strings.BigDreams.Match2.@spanish);
			dreamMatch3 = new Array(configXML.Strings.BigDreams.Match3.@english, configXML.Strings.BigDreams.Match3.@spanish);
			dreamMatch4 = new Array(configXML.Strings.BigDreams.Match4.@english, configXML.Strings.BigDreams.Match4.@spanish);
			dreamMisMatch4 = new Array(configXML.Strings.BigDreams.MisMatch4.@english, configXML.Strings.BigDreams.MisMatch4.@spanish);
			dreamSavingsTips2 = new Array(configXML.Strings.BigDreams.SavingsTips2.@english, configXML.Strings.BigDreams.SavingsTips2.@spanish);
			dreamTips = new Array(configXML.Strings.BigDreams.Tips.@english, configXML.Strings.BigDreams.Tips.@spanish);
			dreamCheckOutOther = new Array(configXML.Strings.BigDreams.CheckOutOther.@english, configXML.Strings.BigDreams.CheckOutOther.@spanish);
			dreamNext = new Array(configXML.Strings.BigDreams.Next.@english, configXML.Strings.BigDreams.Next.@spanish);
			dreamDone = new Array(configXML.Strings.BigDreams.Done.@english, configXML.Strings.BigDreams.Done.@spanish);
			dreamCheckOutThese = new Array(configXML.Strings.BigDreams.CheckOutThese.@english, configXML.Strings.BigDreams.CheckOutThese.@spanish);
			dreamStillThere = new Array(configXML.Strings.BigDreams.StillThere.@english, configXML.Strings.BigDreams.StillThere.@spanish);
			dreamQuit = new Array(configXML.Strings.BigDreams.Quit.@english, configXML.Strings.BigDreams.Quit.@spanish);
			dreamExit = new Array(configXML.Strings.BigDreams.Exit.@english, configXML.Strings.BigDreams.Exit.@spanish);
			dreamAverageCost = new Array(configXML.Strings.BigDreams.AverageCost.@english, configXML.Strings.BigDreams.AverageCost.@spanish);
			
			
			if (owner != null)
			{
				//owner.addText("wordsStart2");
			}
			
			if (owner != null)
			{
				//owner._api.get_media_wall_all();
				//owner.addText("owner != null");
			}
			
			isDone = true;
		}
		
		private function loadStats():void
		{
			statsLoader = new URLLoader();
			statsLoader.load(new URLRequest("C:/MediaWall/stats.xml"));
			statsLoader.addEventListener(Event.COMPLETE, processStatsXML);
		}
		private function processStatsXML(e:Event):void
		{
			//trace("processStatsXML");
			statsXML = new XML(e.target.data);
			//trace(peopleXML);
			oldXML = statsXML;
			
			//trace("statsXML.Stat.length()");
			for (var i:int = 0; i < statsXML.Stat.length(); i++)
			{
				if (statsXML.Stat[i] == "tortoise")
				{
					numTort++;
				}
				if (statsXML.Stat[i] == "gogetter")
				{
					numGo++;
				}
				if (statsXML.Stat[i] == "gambler")
				{
					numGamb++;
				}
			}
			
			//trace(numTort);
			//trace(numGo);
			//trace(numGamb);
		}
	}
}
