﻿package code 
{
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.utils.*;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	
	public class DrGm extends MovieClip 
	{
		public var bubbleArray:Array = new Array();
		public var dollaArray:Array = new Array();
		private var fadeCounter:Number = 0;
		private var dollaValue:Number = 0;
		public var isBeginning:Boolean = true;
		
		private var soundDirectory:String;
		private var sChannel:SoundChannel = new SoundChannel();
		private var soundArray:Array = new Array();
		private var playSArray:Array = new Array();
		private var loadNum:Number = 0;

		public var owner:LoGm;
		/*public var b1:DrGmBubble;
		public var b2:DrGmBubble;
		public var b3:DrGmBubble;
		public var b4:DrGmBubble;
		public var b5:DrGmBubble;
		public var b6:DrGmBubble;
		public var b7:DrGmBubble;*/
				
		public function DrGm(myOwner:LoGm)//myOwner:LoGm
		{
			owner = myOwner;

			/*b1 = new DrGmBubble(this, 330, 1025, 1, .9, 2);
			b2 = new DrGmBubble(this, 800, 1290, 2, .35, 1);
			b3 = new DrGmBubble(this, 850, 1425, 3, .35, 4);
			b4 = new DrGmBubble(this, 655, 1540, 4, .35, 2);
			b5 = new DrGmBubble(this, 730, 1690, 5, .35, 4);
			b6 = new DrGmBubble(this, 800, 800, 6, .9, 1);
			b7 = new DrGmBubble(this, 350, 1370, 7, .35, 2);
			addChild(b1);
			addChild(b2);
			addChild(b3);
			addChild(b4);
			addChild(b5);
			addChild(b6);
			addChild(b7);
			setChildIndex(dreamNext, numChildren-1);*/
			
			loadSounds("assets/sounds/Eastern_bank_dream.mp3");
			loadSounds("assets/sounds/single_pop.mp3");
			
			introBubs.car.mouseChildren = false;
			introBubs.vacation.mouseChildren = false;
			introBubs.college.mouseChildren = false;
			introBubs2.wedding.mouseChildren = false;
			introBubs2.retirement.mouseChildren = false;
			introBubs2.house.mouseChildren = false;
			introBubs3.wardrobe.mouseChildren = false;
						
			bubbleArray.push(introBubs.car, introBubs.vacation, introBubs.college, introBubs2.wedding, introBubs2.retirement, introBubs2.house, introBubs3.wardrobe);
			//bubbleArray.push(b1, b2, b3, b4, b5, b6, b7);
			//dollaArray.push(dreamNext.d1, dreamNext.d2, dreamNext.d3, dreamNext.d4, dreamNext.d5, dreamNext.d6, dreamNext.d7, dreamNext.d8, 
							//dreamNext.d9, dreamNext.d10, dreamNext.d11, dreamNext.d12, dreamNext.d13, dreamNext.d14, dreamNext.d15, dreamNext.d16);
		}
		
		//--------------------------------------
		//  Sound Handlers
		//--------------------------------------

		private function startSound(newSound:Sound, isAttract:Boolean=false):void
		{
			sChannel.stop();
			if (isAttract == true)
			{
				sChannel = newSound.play(0,9001);//IT'S OVER 9000!
			}
			else
			{
				sChannel = newSound.play();
			}

		}

		private function loadSounds(newPath:String):void
		{
			var sSound:Sound = new Sound();
			sSound.addEventListener(Event.COMPLETE, soundLoaded);

			var req:URLRequest = new URLRequest(newPath);
			sSound.load(req);
			playSArray.push(sSound);
		}

		function soundLoaded(event:Event):void
		{
			loadNum++;

			if (loadNum == 2)
			{
				//////Build the video player and grab the video paths once all sounds are loaded//////
				TweenMax.to(dreamFace, 1, {y:1920, ease:Strong.easeOut, onComplete:startBubbles});
				TweenMax.fromTo(bground.bInner, 90, {y:1616.4}, {y:-462, repeat:-1, ease:Linear.easeNone});
				doneButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
			}
		}
		
		public function startBubbles()
		{
			startSound(playSArray[0], true);
			TweenMax.to(introBubs, .5, {alpha:1, ease:Linear.easeNone});
			TweenMax.to(introBubs2, .5, {alpha:1, ease:Linear.easeNone});
			TweenMax.to(introBubs3, .5, {alpha:1, ease:Linear.easeNone});
			introBubs.gotoAndPlay(2);
			scales.gotoAndStop(2);
			addEventListener(Event.ENTER_FRAME, checkFrame);
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				if(!bubbleArray[i].hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					trace(i);
					bubbleArray[i].addEventListener(TuioTouchEvent.TOUCH_DOWN, tester);
				}
			}
			
			bigB.visible = false;
			medB.visible = false;
			
			//TweenMax.to(bc1, .5, {frame:21, ease:Strong.easeOut});
			//TweenMax.to(bc2, 1, {frame:40, ease:Strong.easeOut});
			//TweenMax.to(b1, .5, {delay:.25, alpha:1, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["car"]});
			//TweenMax.to(b6, .5, {delay:.5, alpha:1, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["vacation"]});
		}
		
		public function tester(e:TuioTouchEvent) //e:TuioTouchEvent
		{
			startSound(playSArray[1]);
			introBubs.stop();
			introBubs2.stop();
			introBubs3.stop();
			removeEventListener(Event.ENTER_FRAME, checkFrame);
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				bubbleArray[i].removeEventListener(TuioTouchEvent.TOUCH_DOWN, tester);
			}
					
			TweenMax.to(introBubs, .5, {alpha:0, ease:Linear.easeNone});
			TweenMax.to(introBubs2, .5, {alpha:0, ease:Linear.easeNone});
			TweenMax.to(introBubs3, .5, {alpha:0, ease:Linear.easeNone});
			TweenMax.to(dreamText, .5, {alpha:0, ease:Linear.easeNone});
			
			if(e.currentTarget.name == "car")
			{
				trace("name == car");
				medB.gotoAndStop(1);
				bigB.gotoAndStop(1);
			}
			if(e.currentTarget.name == "wardrobe")
			{
				trace("name == wardrobe");
				medB.gotoAndStop(2);
				bigB.gotoAndStop(2);
			}
			if(e.currentTarget.name == "house")
			{
				trace("name == house");
				medB.gotoAndStop(3);
				bigB.gotoAndStop(3);
			}
			if(e.currentTarget.name == "wedding")
			{
				trace("name == wedding");
				medB.gotoAndStop(4);
				bigB.gotoAndStop(4);
			}
			if(e.currentTarget.name == "retirement")
			{
				trace("name == retirement");
				medB.gotoAndStop(5);
				bigB.gotoAndStop(5);
			}
			if(e.currentTarget.name == "vacation")
			{
				trace("name == vacation");
				medB.gotoAndStop(6);
				bigB.gotoAndStop(6);
			}
			if(e.currentTarget.name == "college")
			{
				trace("name == college");
				medB.gotoAndStop(7);
				bigB.gotoAndStop(7);
			}
			
			medB.visible = true;
			bigB.visible = true;
			bigB.scaleX = bigB.scaleY = .1;
			TweenMax.to(medB, 1, {alpha:1, ease:Linear.easeNone}); //, onComplete:addBigB
			TweenMax.to(bigB, 1.5, {delay:.5, scaleX:1, scaleY:1, alpha:1, ease:Strong.easeOut, onComplete:addButts});
			TweenMax.to(scales, 1, {y:0, ease:Strong.easeOut});
			TweenMax.to(dreamFace, 1, {x:322, ease:Strong.easeOut});
		}
		
		/*public function addBigB()
		{
			trace("I like bigBubbles");
			TweenMax.to(bigB, 1.5, {scaleX:1, scaleY:1, alpha:1, ease:Strong.easeOut, onComplete:addButts});
		}*/
		
		public function addButts()
		{
			trace("I like bigButts");
			TweenMax.to(bigB.bigButts, 1, {frame:30, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["all"]}); 
			//bigB.bigButts.alpha = 1;
			//TweenMax.fromTo(bigB.bigButts, 1, {x:-115}, {x:253, ease:Strong.easeOut, onComplete:addEvent, onCompleteParams:["both"]});
		}
		
		public function backToStart()
		{
			TweenMax.to(dreamFace, 1.5, {delay:.5, x:515, ease:Strong.easeOut, onComplete:startBubbles});
			TweenMax.to(scales, 1.5, {y:630, ease:Strong.easeOut});
			TweenMax.to(bigB, 1, {scaleX:.1, scaleY:.1, alpha:0, ease:Strong.easeOut});
			TweenMax.to(medB, 1, {delay:.5, alpha:0, ease:Linear.easeNone});
			TweenMax.to(dreamText, 1, {delay:.5, alpha:1, ease:Linear.easeNone});
			introBubs.gotoAndStop(1);
			introBubs2.gotoAndStop(1);
			introBubs3.gotoAndStop(1);
		}
		
		public function checkFrame(e:Event)
		{
			if(introBubs.currentFrame == 180)
			{
				introBubs2.gotoAndPlay(2);
			}
			if(introBubs2.currentFrame == 160)
			{
				introBubs3.gotoAndPlay(2);
			}
			if(introBubs3.currentFrame == 150)
			{
				introBubs.gotoAndPlay(2);
			}
		}
		
		public function addEvent(type:String)
		{
			/*if(type == "car")
			{
				b1.addClick();
			}
			if(type == "vacation")
			{
				b6.addClick();
			}*/
			if(type == "yes")
			{
				if(!bigB.bigButts.yesButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					bigB.bigButts.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
					//bigB.bigButts.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				}
			}
			if(type == "no")
			{
				if(!bigB.bigButts.noButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					bigB.bigButts.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
					//bigB.bigButts.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				}
			}
			if(type == "done")
			{
				if(!dreamNext.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					dreamNext.addEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
					//dreamNext.addEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				}
			}
			if(type == "both")
			{
				if(!bigB.bigButts.yesButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					bigB.bigButts.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
					//bigB.bigButts.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				}
				
				if(!bigB.bigButts.noButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					bigB.bigButts.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
					//bigB.bigButts.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				}
			}
			if(type == "all")
			{
				//if(!doneButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				//{
					//doneButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
					//doneButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				//}
				
				if(!bigB.bigButts.yesButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					bigB.bigButts.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
					//bigB.bigButts.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				}
				
				if(!bigB.bigButts.noButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					bigB.bigButts.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
					//bigB.bigButts.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				}
			}
		}
		public function removeEvent(type:String, bubble:DrGmBubble = null, index:Number = 0)
		{
			if(type == "yes")
			{
				bigB.bigButts.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				//bigB.bigButts.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
			}
			if(type == "no")
			{
				bigB.bigButts.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				//bigB.bigButts.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
			}
			if(type == "done")
			{
				//dreamNext.removeEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				//dreamNext.removeEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
			}
			if(type == "both")
			{
				bigB.bigButts.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				//bigB.bigButts.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				
				bigB.bigButts.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				//bigB.bigButts.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
			}
			if(type == "all")
			{
				//doneButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				//doneButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
				
				bigB.bigButts.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				//bigB.bigButts.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				
				bigB.bigButts.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				//bigB.bigButts.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
			}
			/*if(type == "object")
			{
				if(bubble != null)
				{
					trace("removing your dreams...awww");
					removeChild(bubble);
					bubbleArray.splice(index, 1);
					
					if(bubbleArray.length <= 0)
					{
						onward();
					}
				}
			}*/
		}
		
		/*public function playBubbleNoise():void
		{
			owner.playSound(3);
		}
		
		public function fader(type:String, bubble:DrGmBubble = null, nDelay:Number = 7)
		{			
			if(type == "in")
			{
				//////Have bubbles fade in. Wait 5 seconds then start fading them out//////
				for (var i:Number=0; i < bubbleArray.length; i++)
				{
					if (bubbleArray[i].isOpen != true && bubbleArray[i].isHiding == true)
					{
						trace("FADING IN");
						bubbleArray[i].isHiding = false;
						TweenMax.to(bubbleArray[i], 1, {delay:4*i, alpha:1, ease:Linear.easeNone, onComplete:fader, onCompleteParams:["out", bubbleArray[i]]});
					}
				}
			}
			if(type == "out")
			{
				bubble.addClick();
				TweenMax.to(bubble, 1, {delay:nDelay, alpha:0, ease:Linear.easeNone, onComplete:checkHide, onCompleteParams:[bubble]});
				TweenMax.delayedCall(nDelay, fader, ["in"]);
			}
		}*/
		
		/*public function select(e:TuioTouchEvent) //e:TuioTouchEvent
		{
			//////When selected, remove listener and move to center//////
			e.target.removeClick();
			TweenMax.killTweensOf(e.target);
			gotoEnd(e.target);
			e.target.alpha = 1;
			e.target.isHiding = false;
			
			trace("DrGm select currentTarget = " + e.currentTarget.name);
			trace("DrGm select target = " + e.target.name);
			
			switch (e.target)
			{
				case b1:
					trace("is 1");
					owner.playSound(2);
				break;
				case b2:
					trace("is 2");
					owner.playSound(12);
				break;
				case b3:
					trace("is 3");
					owner.playSound(7);
				break;
				case b4:
					trace("is 4");
					owner.playSound(13);
				break;
				case b5:
					trace("is 5");
					owner.playSound(9);
				break;
				case b6:
					trace("is 6");
					owner.playSound(11);
				break;
				case b7:
					trace("is 7");
					owner.playSound(4);
				break;
			}
			
			if(isBeginning == true)
			{
				isBeginning = false;
				TweenMax.to(bc1, .3, {alpha:0, ease:Linear.easeNone});
				TweenMax.to(bc2, .3, {alpha:0, ease:Linear.easeNone});				
				
				if(e.target.pos == 1)
				{
					b6.isOpen = false;
					b6.isHiding = false;
					b1.gotoAndStop(8);
					b6.gotoAndStop(9);
					fader("out", b6, 5);
				}
				if(e.target.pos == 6)
				{
					b1.isOpen = false;
					b1.isHiding = false;
					b1.gotoAndStop(8);
					b6.gotoAndStop(9);
					fader("out", b1, 5);
				}
				
				fader("in");
			}
		}*/
		
		/*public function gotoEnd(targ:Object)
		{
			//////Set up screen for large bubble. Move bubble to front//////
			TweenMax.to(dreamNext, .5, {x:0, ease:Strong.easeOut});
			TweenMax.to(dreamFace, .5, {scaleX:.65, scaleY:.65, x:300, y:1734, ease:Strong.easeOut});
			TweenMax.to(bigB, .5, {alpha:1, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["all"]});
			TweenMax.to(dreamText, .5, {alpha:0, ease:Linear.easeNone});
			bigB.gotoAndStop(targ.pos);
			
			//////Set selected as open, move it to center and reset all other bubbles//////			
			targ.isOpen = true;
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				if (bubbleArray[i].isOpen == true)
				{
					TweenMax.to(bubbleArray[i], .5, {scaleX:.65, scaleY:.65, x:358, y:1146, ease:Strong.easeOut});
					targ.isOpen = false;
				}
				else
				{
					bubbleArray[i].resetDream("start");
				}
			}
		}
		
		public function checkHide(bubble:DrGmBubble)
		{
			bubble.removeClick();
			bubble.isHiding = true;
		}*/
		
		public function heckYeah(e:TuioTouchEvent)//e:TuioTouchEvent
		{
			trace("heckYeah");
			removeEvent("all");
			TweenMax.to(scales, 3, {frame:90, ease:Linear.easeNone, onComplete:backToStart});
			/*//owner.playSound(1);
			//owner.playSound(8);
			TweenMax.to(bigB, .5, {alpha:0, ease:Linear.easeNone});
			TweenMax.to(dreamText, .5, {alpha:1, ease:Linear.easeNone});
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				if (bubbleArray[i].scaleX > .5)
				{
					TweenMax.to(bubbleArray[i], .5, {alpha:0, ease:Linear.easeNone, onComplete:removeEvent, onCompleteParams:["object", bubbleArray[i], i]});
					dollaValue += bubbleArray[i].price;
					
					for (var d:Number=0; d < dollaValue; d++)
					{
						TweenMax.to(dollaArray[d], 1, {frame:30, ease:Strong.easeOut});
					}
				}
				else
				{
					TweenMax.killTweensOf(bubbleArray[i]);
					bubbleArray[i].alpha = 0;
					bubbleArray[i].isHiding = true;
					fader("in");
					bubbleArray[i].resetDream("end");
				}
			}*/
		}
		
		public function heckNah(e:TuioTouchEvent)//e:TuioTouchEvent
		{
			trace("heckNah");
			removeEvent("all");
			backToStart();
			/*TweenMax.to(bigB, .5, {alpha:0, ease:Linear.easeNone});
			TweenMax.to(dreamText, .5, {alpha:1, ease:Linear.easeNone});
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				if (bubbleArray[i].scaleX > .5)
				{
					TweenMax.to(bubbleArray[i], .5, {alpha:0, ease:Linear.easeNone, onComplete:removeEvent, onCompleteParams:["object", bubbleArray[i], i]});
				}
				else
				{
					TweenMax.killTweensOf(bubbleArray[i]);
					bubbleArray[i].alpha = 0;
					bubbleArray[i].isHiding = true;
					fader("in");
					bubbleArray[i].resetDream("end");
				}
			}*/
		}
		
		public function onward(e:TuioTouchEvent = null)//e:TuioTouchEvent
		{
			trace("something obviously awesome is going to happen here. Possibly even life changing.");
			sChannel.stop();
			playSArray = [];
			removeEvent("all");
			TweenMax.killTweensOf(scales);
			//TweenMax.killAll();
			owner.playSound(1);
			owner.countdown();
		}
	}
}
