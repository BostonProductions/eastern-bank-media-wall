﻿package code
{
	import flash.display.MovieClip;

	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;

	import flash.events.*;
	import flash.utils.*;

	import com.greensock.*;
	import com.greensock.easing.*;

	public class DrGmBubble extends MovieClip
	{
		private var owner:DrGm;
		public var pos:Number;
		public var size:Number;
		public var price:Number;
		public var isOpen:Boolean;
		public var isHiding:Boolean;

		public function DrGmBubble(manager:DrGm, newX:Number, newY:Number, index:Number, sizer:Number, pricer:Number)
		{
			//////Set all initial parameters, position, scale, owner, bubble#, mouse properties, listeners//////
			owner = manager;
			pos = index;
			this.x = newX;
			this.y = newY;
			size = sizer;
			price = pricer;
			this.alpha = 0;
			this.scaleX = this.scaleY = size;
			this.gotoAndStop(pos);
			this.mouseChildren = false;
			this.name = String(pos);

			if (pos == 1 || pos == 6)
			{
				isHiding = false;
			}
			else
			{
				isHiding = true;
			}
		}

		public function addClick()
		{
			//addEventListener(MouseEvent.CLICK, owner.select);
			addEventListener(TuioTouchEvent.TOUCH_DOWN, owner.select);
		}

		public function removeClick()
		{
			//removeEventListener(MouseEvent.CLICK, owner.select);
			removeEventListener(TuioTouchEvent.TOUCH_DOWN, owner.select);
		}

		public function resetDream(type:String)
		{
			//////Add back the event listener, and move the bubble to its reset start position//////
			if (type == "start")
			{
				if (pos == 1)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.35, scaleY:.35, x:920, y:1570, ease:Strong.easeOut});
				}
				if (pos == 2)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.35, scaleY:.35, x:800, y:1290, ease:Strong.easeOut});
				}
				if (pos == 3)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.35, scaleY:.35, x:850, y:1425, ease:Strong.easeOut});
				}
				if (pos == 4)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.35, scaleY:.35, x:655, y:1540, ease:Strong.easeOut});
				}
				if (pos == 5)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.35, scaleY:.35, x:730, y:1670, ease:Strong.easeOut});
				}
				if (pos == 6)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.35, scaleY:.35, x:600, y:1390, ease:Strong.easeOut});
				}
				if (pos == 7)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.35, scaleY:.35, x:350, y:1370, ease:Strong.easeOut});
				}
			}
			
			if (type == "end")
			{
				if (pos == 1)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.75, scaleY:.75, x:800, y:550, ease:Strong.easeOut});
				}
				if (pos == 2)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.75, scaleY:.75, x:330, y:700, ease:Strong.easeOut});
				}
				if (pos == 3)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.75, scaleY:.75, x:810, y:900, ease:Strong.easeOut});
				}
				if (pos == 4)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.75, scaleY:.75, x:250, y:1070, ease:Strong.easeOut});
				}
				if (pos == 5)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.75, scaleY:.75, x:875, y:1275, ease:Strong.easeOut});
				}
				if (pos == 6)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.65, scaleY:.65, x:775, y:1490, ease:Strong.easeOut});
				}
				if (pos == 7)
				{
					isOpen = false;
					TweenMax.to(this, .5, {scaleX:.75, scaleY:.75, x:800, y:210, ease:Strong.easeOut});
				}
			}
		}
	}
}