﻿package code 
{
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.utils.*;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import fl.motion.Color;
	import flashx.textLayout.operations.MoveChildrenOperation;
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.gestures.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	import code.TenMillionSpinner;
	
	public class DrGm extends MovieClip 
	{
		public var bubbleArray:Array = new Array();
		public var dollaArray:Array = new Array();
		public var sliderArray:Array = new Array();
		public var iconArray:Array = new Array();
		public var score:Number = 0;
		private var dollaValue:Number = 0;
		private var bubIndex:Number;
		public var isBeginning:Boolean = true;
		public var spinnerTimer:Timer = new Timer(10,1);
		private var activeBub:String;
		
		private var soundDirectory:String;
		private var sChannel:SoundChannel = new SoundChannel();
		private var soundArray:Array = new Array();
		private var playSArray:Array = new Array();
		private var tweenArray:Array = new Array();
		private var loadNum:Number = 0;

		public var owner:LoGm;
		public var b1:Car;
		public var b2:Vacation;
		public var b3:College;
		public var b4:Retirement;
		public var b5:House;
		public var b6:Wardrobe;
		public var b7:Wedding;
		public var numberSpinner:TenMillionSpinner;
		var tm:TweenMax;
		
		public function DrGm(myOwner:LoGm)//myOwner:LoGm
		{
			owner = myOwner;

			b1 = new Car();
			b1.name = "car";
			b2 = new Vacation();
			b2.name = "vacation";
			b3 = new College();
			b3.name = "college";
			b4 = new Retirement();
			b4.name = "retirement";
			b5 = new House();
			b5.name = "house";
			b6 = new Wardrobe();
			b6.name = "wardrobe";
			b7 = new Wedding();
			b7.name = "wedding";
			
			addChildAt(b2, 1);
			addChildAt(b7, 1);
			addChildAt(b3, 1);
			addChildAt(b6, 1);
			addChildAt(b1, 1);
			addChildAt(b4, 1);
			addChildAt(b5, 1);
			
			numberSpinner = new TenMillionSpinner();
			numberSpinner.x = 650;
			numberSpinner.y = 1822;
			scales.addChild(numberSpinner);
			numberSpinner.init(score);
			numberSpinner.stopAnimation();
			
			loadSounds("assets/sounds/dreaming_JohnYoung.mp3");
			loadSounds("assets/sounds/single_pop.mp3");
			loadSounds("assets/sounds/SaverButtonClick.mp3");
			loadSounds("assets/sounds/SaverMoneyMeter.mp3");
			loadSounds("assets/sounds/SaverCar.mp3");
			loadSounds("assets/sounds/SaverVacation.mp3");
			loadSounds("assets/sounds/SaverCollege.mp3");
			loadSounds("assets/sounds/SaverRetirement.mp3");
			loadSounds("assets/sounds/SaverHome.mp3");
			loadSounds("assets/sounds/SaverWardrobe.mp3");
			loadSounds("assets/sounds/SaverWedding.mp3");
			loadSounds("assets/sounds/SaverWedding.mp3");
			loadSounds("assets/sounds/ScaleShortversion.mp3");
			loadSounds("assets/sounds/moneyrolling.mp3");

			scales.dollaStack.alpha = 0;
			medB.mouseEnabled = false;
			bubbleArray.push(b1, b2, b7, b5, b4, b3, b6);
			iconArray.push(iconCar, iconVac, iconCol, iconRet, iconHou, iconWar, iconWed);
			sliderArray.push(bigB.sliderBar.sb1, bigB.sliderBar.sb2, bigB.sliderBar.sb3, bigB.sliderBar.sb4, bigB.sliderBar.sb5, bigB.sliderBar.sb6, bigB.sliderBar.sb7);
		}
		
		//--------------------------------------
		//  Sound Handlers
		//--------------------------------------

		private function startSound(newSound:Sound, isAttract:Boolean=false):void
		{
			if (sChannel != null)
			{
				sChannel.stop();
				if (isAttract == true)
				{
					sChannel = newSound.play(0,9001);//IT'S OVER 9000!
				}
				else
				{
					sChannel = newSound.play();
				}
			}
		}

		private function loadSounds(newPath:String):void
		{
			var sSound:Sound = new Sound();
			sSound.addEventListener(Event.COMPLETE, soundLoaded);

			var req:URLRequest = new URLRequest(newPath);
			sSound.load(req);
			playSArray.push(sSound);
		}

		private function soundLoaded(event:Event):void
		{
			loadNum++;

			if (loadNum == 13)
			{
				//////Build the video player and grab the video paths once all sounds are loaded//////
				TweenMax.to(dreamFace, 1, {y:1920, ease:Strong.easeOut, onComplete:startStartBub});
				TweenMax.fromTo(bground.bInner, 90, {y:1616.4}, {y:-462, repeat:-1, ease:Linear.easeNone});
				TweenMax.fromTo(doneButt, 1, {frame:2}, {frame:30, ease:Linear.easeNone, onComplete:nextRepeater});
				startSound(playSArray[0], true);
				doneButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
			}
		}
		private function startStartBub()
		{
			TweenMax.fromTo(startBub, 7, {frame:2}, {frame:190, ease:Linear.easeNone, onComplete:startBubbles});
		}
		
		private function startBubbles()
		{
			TweenMax.to(dreamText, 1, {alpha:1, ease:Strong.easeOut});
			scales.gotoAndStop(2);
			
			addEventListener(Event.ENTER_FRAME, checkAlpha);
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				/*if(!bubbleArray[i].innerMC.hasEventListener(MouseEvent.CLICK))
				{
					tm = new TweenMax(bubbleArray[i], 7, {delay:i*3, frame:190, ease:Linear.easeNone, onComplete:repeater, onCompleteParams:[i]});
					tweenArray.push(tm);
				}*/
				if(!bubbleArray[i].innerMC.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
				{
					tm = new TweenMax(bubbleArray[i], 7, {delay:i*3, frame:190, ease:Linear.easeNone, onComplete:repeater, onCompleteParams:[i]});
					tweenArray.push(tm);
				}
			}
			
			bigB.visible = false;
			medB.visible = false;
		}
		
		private function repeater(index:Number)
		{
			//trace("repeat repeat repeat");
			bubbleArray[index].gotoAndStop(1);
			if(bubbleArray.length > 1)
			{
				TweenMax.to(bubbleArray[index], 7, {delay:(bubbleArray.length-2)*3, frame:190, ease:Linear.easeNone, onComplete:repeater, onCompleteParams:[index]});
			}
			else
			{
				TweenMax.to(bubbleArray[index], 7, {frame:190, ease:Linear.easeNone, onComplete:repeater, onCompleteParams:[index]});
			}
		}
		
		private function nextRepeater()
		{
			TweenMax.fromTo(doneButt, 1, {frame:2}, {delay:3, frame:30, ease:Linear.easeNone, onComplete:nextRepeater});
		}
		
		private function checkAlpha(e:Event)
		{
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				if(bubbleArray[i].currentFrame >= 80)
				{
					/*if(!bubbleArray[i].innerMC.hasEventListener(MouseEvent.CLICK))
					{
						bubbleArray[i].innerMC.addEventListener(MouseEvent.CLICK, tester);
					}*/
					if(!bubbleArray[i].innerMC.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
					{
						bubbleArray[i].innerMC.addEventListener(TuioTouchEvent.TOUCH_DOWN, tester);
					}
				}
				else
				{
					//bubbleArray[i].innerMC.removeEventListener(MouseEvent.CLICK, tester);
					bubbleArray[i].innerMC.removeEventListener(TuioTouchEvent.TOUCH_DOWN, tester);
				}
			}
		}
		
		private function tester(e:TuioTouchEvent) //e:TuioTouchEvent
		{
			removeEvent("butts");
			removeEventListener(Event.ENTER_FRAME, checkAlpha);
			
			for (var s:Number=0; s < sliderArray.length; s++)
			{
				changeTint(sliderArray[s], 0);
			}
			changeTint(sliderArray[3], 1);
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				TweenMax.killTweensOf(bubbleArray[i]);
				TweenMax.to(bubbleArray[i], .5, {alpha:0, ease:Linear.easeNone, onComplete:hideBubs});
				bubbleArray[i].gotoAndStop(1);
								
				if(e.target.parent.name == "car" && bubbleArray[i].name == "car")
				{
					activeBub = "car";
					startSound(playSArray[4]);
					medB.gotoAndStop(1);
					bigB.gotoAndStop(1);
					bigB.sliderBar.sliderTextLow.text = "$15,000"
					bigB.sliderBar.sliderTextHigh.text = "$100,000"
					bigB.sliderBar.sliderMarker.sliderText.text = "$30,000";
					dollaValue = 30000;
					bubIndex = i;
				}
				if(e.target.parent.name == "wardrobe" && bubbleArray[i].name == "wardrobe")
				{
					activeBub = "wardrobe";
					startSound(playSArray[9]);
					medB.gotoAndStop(2);
					bigB.gotoAndStop(2);
					bigB.sliderBar.sliderTextLow.text = "$300"
					bigB.sliderBar.sliderTextHigh.text = "$30,000"
					bigB.sliderBar.sliderMarker.sliderText.text = "$3,000";
					dollaValue = 3000;
					bubIndex = i;
				}
				if(e.target.parent.name == "house" && bubbleArray[i].name == "house")
				{
					activeBub = "house";
					startSound(playSArray[8]);
					medB.gotoAndStop(3);
					bigB.gotoAndStop(3);
					bigB.sliderBar.sliderTextLow.text = "$100,000"
					bigB.sliderBar.sliderTextHigh.text = "$2,000,000"
					bigB.sliderBar.sliderMarker.sliderText.text = "$280,000";
					dollaValue = 280000;
					bubIndex = i;
				}
				if(e.target.parent.name == "wedding" && bubbleArray[i].name == "wedding")
				{
					activeBub = "wedding";
					startSound(playSArray[10]);
					medB.gotoAndStop(4);
					bigB.gotoAndStop(4);
					bigB.sliderBar.sliderTextLow.text = "$5,000"
					bigB.sliderBar.sliderTextHigh.text = "$200,000"
					bigB.sliderBar.sliderMarker.sliderText.text = "$28,000";
					dollaValue = 28000;
					bubIndex = i;
				}
				if(e.target.parent.name == "retirement" && bubbleArray[i].name == "retirement")
				{
					activeBub = "retirement";
					startSound(playSArray[7]);
					medB.gotoAndStop(5);
					bigB.gotoAndStop(5);
					bigB.sliderBar.sliderTextLow.text = "$500"
					bigB.sliderBar.sliderTextHigh.text = "$1,000,000"
					bigB.sliderBar.sliderMarker.sliderText.text = "$550,000";
					dollaValue = 550000;
					bubIndex = i;
				}
				if(e.target.parent.name == "vacation" && bubbleArray[i].name == "vacation")
				{
					activeBub = "vacation";
					startSound(playSArray[5]);
					medB.gotoAndStop(6);
					bigB.gotoAndStop(6);
					bigB.sliderBar.sliderTextLow.text = "$500"
					bigB.sliderBar.sliderTextHigh.text = "$30,000"
					bigB.sliderBar.sliderMarker.sliderText.text = "$4,000";
					dollaValue = 4000;
					bubIndex = i;
				}
				if(e.target.parent.name == "college" && bubbleArray[i].name == "college")
				{
					activeBub = "college";
					startSound(playSArray[6]);
					medB.gotoAndStop(7);
					bigB.gotoAndStop(7);
					bigB.sliderBar.sliderTextLow.text = "$22,000"
					bigB.sliderBar.sliderTextHigh.text = "$200,000"
					bigB.sliderBar.sliderMarker.sliderText.text = "$120,000";
					dollaValue = 120000;
					bubIndex = i;
				}
			}
			
			trace("bubIndex = " + bubIndex);
			tweenArray = [];
			TweenMax.to(dreamText, .5, {alpha:0, ease:Linear.easeNone});			
			
			medB.visible = true;
			bigB.visible = true;
			bigB.scaleX = bigB.scaleY = .1;
			TweenMax.to(medB, 1, {alpha:1, ease:Linear.easeNone}); //, onComplete:addBigB
			TweenMax.to(bigB, 1.5, {delay:.5, scaleX:1, scaleY:1, alpha:1, ease:Strong.easeOut, onComplete:addButts});
			TweenMax.to(scales, 1, {y:-99, ease:Strong.easeOut});
			TweenMax.to(doneButt, 1, {y:1822, ease:Strong.easeOut});
			bigB.sliderBar.sliderMarker.x = sliderArray[3].x;
		}

		private function addButts()
		{
			trace("I like bigButts");
			TweenMax.fromTo(bigB.bigButts, 1, {frame:1}, {frame:30, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["all"]}); 
			TweenMax.to(scales.dollaStack, 1, {alpha:1, ease:Linear.easeNone});
		}
		
		private function addEvent(type:String)
		{
			if(type == "butts")
			{
				for (var i:Number=0; i < bubbleArray.length; i++)
				{
					/*if(!bubbleArray[i].innerMC.hasEventListener(MouseEvent.CLICK))
					{
						bubbleArray[i].innerMC.addEventListener(MouseEvent.CLICK, tester);
					}*/
					if(!bubbleArray[i].innerMC.hasEventListener(TuioTouchEvent.TOUCH_DOWN))
					{
						bubbleArray[i].innerMC.addEventListener(TuioTouchEvent.TOUCH_DOWN, tester);
					}
				}
			}
			if(type == "slider")
			{
				for (var s:Number=0; s < sliderArray.length; s++)
				{
					/*if(!sliderArray[s].hasEventListener(MouseEvent.CLICK))
					{
						sliderArray[s].addEventListener(MouseEvent.CLICK, sliderClick);
					}*/
					if(!sliderArray[s].hasEventListener(TuioTouchEvent.TOUCH_DOWN))
					{
						sliderArray[s].addEventListener(TuioTouchEvent.TOUCH_DOWN, sliderClick);
					}
				}
			}
			if(type == "yes")
			{
				if(!bigB.bigButts.yesButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))//TuioTouchEvent.TOUCH_DOWN//MouseEvent.CLICK
				{
					//bigB.bigButts.yesButt.addEventListener(MouseEvent.CLICK, heckYeah);
					bigB.bigButts.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				}
			}
			if(type == "no")
			{
				if(!bigB.bigButts.noButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))//TuioTouchEvent.TOUCH_DOWN//MouseEvent.CLICK
				{
					//bigB.bigButts.noButt.addEventListener(MouseEvent.CLICK, heckNah);
					bigB.bigButts.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				}
			}
			if(type == "both")
			{
				if(!bigB.bigButts.yesButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))//TuioTouchEvent.TOUCH_DOWN//MouseEvent.CLICK
				{
					//bigB.bigButts.yesButt.addEventListener(MouseEvent.CLICK, heckYeah);
					bigB.bigButts.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				}
				
				if(!bigB.bigButts.noButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))//TuioTouchEvent.TOUCH_DOWN//MouseEvent.CLICK
				{
					//bigB.bigButts.noButt.addEventListener(MouseEvent.CLICK, heckNah);
					bigB.bigButts.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				}
			}
			if(type == "all")
			{				
				if(!bigB.bigButts.yesButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))//TuioTouchEvent.TOUCH_DOWN//MouseEvent.CLICK
				{
					//bigB.bigButts.yesButt.addEventListener(MouseEvent.CLICK, heckYeah);
					bigB.bigButts.yesButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				}
				
				if(!bigB.bigButts.noButt.hasEventListener(TuioTouchEvent.TOUCH_DOWN))//TuioTouchEvent.TOUCH_DOWN//MouseEvent.CLICK
				{
					//bigB.bigButts.noButt.addEventListener(MouseEvent.CLICK, heckNah);
					bigB.bigButts.noButt.addEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				}
				for (var sa:Number=0; sa < sliderArray.length; sa++)
				{
					/*if(!sliderArray[sa].hasEventListener(MouseEvent.CLICK))
					{
						sliderArray[sa].addEventListener(MouseEvent.CLICK, sliderClick);
					}*/
					if(!sliderArray[sa].hasEventListener(TuioTouchEvent.TOUCH_DOWN))
					{
						sliderArray[sa].addEventListener(TuioTouchEvent.TOUCH_DOWN, sliderClick);
					}
				}
			}
		}
		private function removeEvent(type:String, bubble:MovieClip = null, index:Number = 0)
		{
			if(type == "butts")
			{
				/*for (var i:Number=0; i < bubbleArray.length; i++)
				{
					bubbleArray[i].innerMC.removeEventListener(MouseEvent.CLICK, tester);
				}*/
				for (var i:Number=0; i < bubbleArray.length; i++)
				{
					bubbleArray[i].innerMC.removeEventListener(TuioTouchEvent.TOUCH_DOWN, tester);
				}
			}
			if(type == "slider")
			{
				/*for (var s:Number=0; s < sliderArray.length; s++)
				{
					sliderArray[s].removeEventListener(MouseEvent.CLICK, sliderClick);
				}*/
				for (var s:Number=0; s < sliderArray.length; s++)
				{
					sliderArray[s].removeEventListener(TuioTouchEvent.TOUCH_DOWN, sliderClick);
				}
			}
			if(type == "yes")
			{
				//bigB.bigButts.yesButt.removeEventListener(MouseEvent.CLICK, heckYeah);
				bigB.bigButts.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
			}
			if(type == "no")
			{
				//bigB.bigButts.noButt.removeEventListener(MouseEvent.CLICK, heckNah);
				bigB.bigButts.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
			}
			if(type == "both")
			{
				//bigB.bigButts.yesButt.removeEventListener(MouseEvent.CLICK, heckYeah);
				bigB.bigButts.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				
				//bigB.bigButts.noButt.removeEventListener(MouseEvent.CLICK, heckNah);
				bigB.bigButts.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
			}
			if(type == "all")
			{
				//bigB.bigButts.yesButt.removeEventListener(MouseEvent.CLICK, heckYeah);
				bigB.bigButts.yesButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckYeah);
				
				//bigB.bigButts.noButt.removeEventListener(MouseEvent.CLICK, heckNah);
				bigB.bigButts.noButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, heckNah);
				
				/*for (var ss:Number=0; ss < sliderArray.length; ss++)
				{
					sliderArray[ss].removeEventListener(MouseEvent.CLICK, sliderClick);
				}*/
				for (var ss:Number=0; ss < sliderArray.length; ss++)
				{
					sliderArray[ss].removeEventListener(TuioTouchEvent.TOUCH_DOWN, sliderClick);
				}
			}
			if(type == "object")
			{
				if(bubble != null)
				{
					trace("removing your dreams...awww");
					removeChild(bubble);
					bubbleArray.splice(index, 1);
					
					if(bubbleArray.length <= 0)
					{
						onward();
					}
				}
			}
		}
		
		private function changeTint(obj:Object, amount:Number):void {
			var c:Color = new Color();
			c.setTint(0xEC8223, amount);
			obj.transform.colorTransform = c;
		}
	
		private function sliderClick(e:TuioTouchEvent)//e:TuioTouchEvente:MouseEvent
		{
			trace("moving slider to " + bigB.currentFrame);
			TweenMax.to(bigB.sliderBar.sliderMarker, 1, {x:e.currentTarget.x, ease:Strong.easeOut});
			startSound(playSArray[2]);
			
			if(bigB.currentFrame == 1)
			{
				if(e.currentTarget.name == "sb1")
				{
					
					dollaValue = 15000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$15,000";
				}
				if(e.currentTarget.name == "sb2")
				{
					dollaValue = 20000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$20,000";
				}
				if(e.currentTarget.name == "sb3")
				{
					dollaValue = 25000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$25,000";
				}
				if(e.currentTarget.name == "sb4")
				{
					dollaValue = 30000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$30,000";
				}
				if(e.currentTarget.name == "sb5")
				{
					dollaValue = 50000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$50,000";
				}
				if(e.currentTarget.name == "sb6")
				{
					dollaValue = 75000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$75,000";
				}
				if(e.currentTarget.name == "sb7")
				{
					dollaValue = 100000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$100,000";
				}
				
				for (var i:Number=0; i < sliderArray.length; i++)
				{
					if(e.currentTarget.name == sliderArray[i].name)
					{
						changeTint(e.currentTarget, 1);
					}
					else
					{
						changeTint(sliderArray[i], 0);
					}
				}
			}
			if(bigB.currentFrame == 6)
			{
				
				if(e.currentTarget.name == "sb1")
				{
					dollaValue = 500;
					bigB.sliderBar.sliderMarker.sliderText.text = "$500";
				}
				if(e.currentTarget.name == "sb2")
				{
					dollaValue = 2000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$2,000";
				}
				if(e.currentTarget.name == "sb3")
				{
					dollaValue = 3000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$3,000";
				}
				if(e.currentTarget.name == "sb4")
				{
					dollaValue = 4000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$4,000";
				}
				if(e.currentTarget.name == "sb5")
				{
					dollaValue = 10000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$10,000";
				}
				if(e.currentTarget.name == "sb6")
				{
					dollaValue = 20000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$20,000";
				}
				if(e.currentTarget.name == "sb7")
				{
					dollaValue = 30000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$30,000";
				}
				for (var i2:Number=0; i2 < sliderArray.length; i2++)
				{
					if(e.currentTarget.name == sliderArray[i2].name)
					{
						changeTint(e.currentTarget, 1);
					}
					else
					{
						changeTint(sliderArray[i2], 0);
					}
				}
			}
			if(bigB.currentFrame == 3)
			{
				if(e.currentTarget.name == "sb1")
				{
					dollaValue = 100000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$100,000";
				}
				if(e.currentTarget.name == "sb2")
				{
					dollaValue = 150000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$150,000";
				}
				if(e.currentTarget.name == "sb3")
				{
					dollaValue = 200000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$200,000";
				}
				if(e.currentTarget.name == "sb4")
				{
					dollaValue = 280000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$280,000";
				}
				if(e.currentTarget.name == "sb5")
				{
					dollaValue = 700000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$700,000";
				}
				if(e.currentTarget.name == "sb6")
				{
					dollaValue = 1250000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$1,250,000";
				}
				if(e.currentTarget.name == "sb7")
				{
					dollaValue = 2000000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$2,000,000";
				}
				for (var i3:Number=0; i3 < sliderArray.length; i3++)
				{
					if(e.currentTarget.name == sliderArray[i3].name)
					{
						changeTint(e.currentTarget, 1);
					}
					else
					{
						changeTint(sliderArray[i3], 0);
					}
				}
			}
			if(bigB.currentFrame == 4)
			{
				if(e.currentTarget.name == "sb1")
				{
					dollaValue = 5000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$5,000";
				}
				if(e.currentTarget.name == "sb2")
				{
					dollaValue = 10000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$10,000";
				}
				if(e.currentTarget.name == "sb3")
				{
					dollaValue = 17000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$17,000";
				}
				if(e.currentTarget.name == "sb4")
				{
					dollaValue = 28000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$28,000";
				}
				if(e.currentTarget.name == "sb5")
				{
					dollaValue = 50000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$50,000";
				}
				if(e.currentTarget.name == "sb6")
				{
					dollaValue = 100000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$100,000";
				}
				if(e.currentTarget.name == "sb7")
				{
					dollaValue = 200000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$200,000";
				}
				for (var i4:Number=0; i4 < sliderArray.length; i4++)
				{
					if(e.currentTarget.name == sliderArray[i4].name)
					{
						changeTint(e.currentTarget, 1);
					}
					else
					{
						changeTint(sliderArray[i4], 0);
					}
				}
			}
			if(bigB.currentFrame == 5)
			{
				if(e.currentTarget.name == "sb1")
				{
					dollaValue = 100000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$100,000";
				}
				if(e.currentTarget.name == "sb2")
				{
					dollaValue = 250000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$250,000";
				}
				if(e.currentTarget.name == "sb3")
				{
					dollaValue = 400000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$400,000";
				}
				if(e.currentTarget.name == "sb4")
				{
					dollaValue = 550000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$550,000";
				}
				if(e.currentTarget.name == "sb5")
				{
					dollaValue = 700000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$700,000";
				}
				if(e.currentTarget.name == "sb6")
				{
					dollaValue = 850000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$850,000";
				}
				if(e.currentTarget.name == "sb7")
				{
					dollaValue = 1000000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$1,000,000";
				}
				for (var i5:Number=0; i5 < sliderArray.length; i5++)
				{
					if(e.currentTarget.name == sliderArray[i5].name)
					{
						changeTint(e.currentTarget, 1);
					}
					else
					{
						changeTint(sliderArray[i5], 0);
					}
				}
			}
			if(bigB.currentFrame == 2)
			{
				if(e.currentTarget.name == "sb1")
				{
					dollaValue = 300;
					bigB.sliderBar.sliderMarker.sliderText.text = "$300";
				}
				if(e.currentTarget.name == "sb2")
				{
					dollaValue = 900;
					bigB.sliderBar.sliderMarker.sliderText.text = "$900";
				}
				if(e.currentTarget.name == "sb3")
				{
					dollaValue = 1700;
					bigB.sliderBar.sliderMarker.sliderText.text = "$1,700";
				}
				if(e.currentTarget.name == "sb4")
				{
					dollaValue = 3000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$3,000";
				}
				if(e.currentTarget.name == "sb5")
				{
					dollaValue = 10000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$10,000";
				}
				if(e.currentTarget.name == "sb6")
				{
					dollaValue = 20000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$20,000";
				}
				if(e.currentTarget.name == "sb7")
				{
					dollaValue = 30000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$30,000";
				}
				for (var i6:Number=0; i6 < sliderArray.length; i6++)
				{
					if(e.currentTarget.name == sliderArray[i6].name)
					{
						changeTint(e.currentTarget, 1);
					}
					else
					{
						changeTint(sliderArray[i6], 0);
					}
				}
			}
			if(bigB.currentFrame == 7)
			{
				if(e.currentTarget.name == "sb1")
				{
					dollaValue = 22000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$22,000";
				}
				if(e.currentTarget.name == "sb2")
				{
					dollaValue = 55000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$55,000";
				}
				if(e.currentTarget.name == "sb3")
				{
					dollaValue = 80000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$80,000";
				}
				if(e.currentTarget.name == "sb4")
				{
					dollaValue = 120000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$120,000";
				}
				if(e.currentTarget.name == "sb5")
				{
					dollaValue = 150000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$150,000";
				}
				if(e.currentTarget.name == "sb6")
				{
					dollaValue = 180000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$180,000";
				}
				if(e.currentTarget.name == "sb7")
				{
					dollaValue = 200000;
					bigB.sliderBar.sliderMarker.sliderText.text = "$200,000";
				}
				for (var i7:Number=0; i7 < sliderArray.length; i7++)
				{
					if(e.currentTarget.name == sliderArray[i7].name)
					{
						changeTint(e.currentTarget, 1);
					}
					else
					{
						changeTint(sliderArray[i7], 0);
					}
				}
			}
		}
		
		private function checkIcon(type:String)
		{
			if(type == "yes")
			{
				if(activeBub == "car")
				{
					iconCar.gotoAndStop(2);
				}
				if(activeBub == "vacation")
				{
					iconVac.gotoAndStop(2);
				}
				if(activeBub == "college")
				{
					iconCol.gotoAndStop(2);
				}
				if(activeBub == "retirement")
				{
					iconRet.gotoAndStop(2);
				}
				if(activeBub == "wardrobe")
				{
					iconWar.gotoAndStop(2);
				}
				if(activeBub == "wedding")
				{
					iconWed.gotoAndStop(2);
				}
				if(activeBub == "house")
				{
					iconHou.gotoAndStop(2);
				}
			}
			if(type == "no")
			{
				if(activeBub == "car")
				{
					iconCar.gotoAndStop(3);
				}
				if(activeBub == "vacation")
				{
					iconVac.gotoAndStop(3);
				}
				if(activeBub == "college")
				{
					iconCol.gotoAndStop(3);
				}
				if(activeBub == "retirement")
				{
					iconRet.gotoAndStop(3);
				}
				if(activeBub == "wardrobe")
				{
					iconWar.gotoAndStop(3);
				}
				if(activeBub == "wedding")
				{
					iconWed.gotoAndStop(3);
				}
				if(activeBub == "house")
				{
					iconHou.gotoAndStop(3);
				}
			}
		}
		
		private function heckYeah(e:TuioTouchEvent)//e:TuioTouchEvent
		{
			trace("heckYeah " + bubIndex);
			removeEvent("all");
			removeEvent("object", bubbleArray[bubIndex], bubIndex);
			checkIcon("yes");
			
			if(isBeginning == true)
			{
				TweenMax.fromTo(scales.dollaStack, 2, {frame:2}, {frame:90, ease:Linear.easeNone});
				TweenMax.fromTo(scales, 2, {frame:2}, {frame:90, ease:Linear.easeNone});
			}
			else
			{
				TweenMax.fromTo(scales.dollaStack, 2, {frame:90}, {frame:150, ease:Linear.easeNone});
				TweenMax.fromTo(scales, 2, {frame:90}, {frame:150, ease:Linear.easeNone});
			}
			
			isBeginning = false;
		
			score += dollaValue;
			trace(score);
			numberSpinner.init(score);
			numberSpinner.addEventListener(TenMillionSpinner.SPINNER_DONE, showDone,false,0,true);
			numberSpinner.stopAnimation();
			
			startSound(playSArray[13]);
			TweenMax.delayedCall(1.2, startSound, [playSArray[12]]);
			//owner.playSound(8);
		}
		
		public function showDone(e:Event):void
		{
			numberSpinner.removeEventListener(TenMillionSpinner.SPINNER_DONE, showDone);
			TweenMax.delayedCall(.5, backToStart);
		}
		
		private function heckNah(e:TuioTouchEvent)//e:TuioTouchEvent
		{
			trace("heckNah");
			removeEvent("all");
			removeEvent("object", bubbleArray[bubIndex], bubIndex);
			startSound(playSArray[2]);			
			isBeginning = false;
			checkIcon("no");
			
			backToStart();
		}

		private function backToStart()
		{
			scales.dollaStack.alpha = 0;
			scales.dollaStack.gotoAndStop(91);
			bigB.bigButts.gotoAndStop(1);
			TweenMax.to(scales, 1.5, {y:630, ease:Strong.easeOut});
			TweenMax.to(bigB, 1, {scaleX:.1, scaleY:.1, alpha:0, ease:Strong.easeOut});
			TweenMax.to(medB, 1, {delay:.5, alpha:0, ease:Linear.easeNone, onComplete:restartBubs});
			TweenMax.to(dreamText, 1, {delay:.5, alpha:1, ease:Linear.easeNone});
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				TweenMax.killTweensOf(bubbleArray[i]);
			}
			
			startSound(playSArray[0], true);
		}
		
		private function hideBubs()
		{
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				bubbleArray[i].visible = false;
			}
		}
		
		private function restartBubs()
		{
			for (var p:Number=0; p < bubbleArray.length; p++)
			{
				if(i < bubIndex)
				{
					trace("moving " + i + " up to " + bubIndex);
					bubbleArray.push(bubbleArray.shift());
				}
			}
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				bubbleArray[i].visible = true;
				TweenMax.to(bubbleArray[i], 1, {alpha:1, ease:Linear.easeNone, onComplete:addEvent, onCompleteParams:["butts"]});
				TweenMax.to(bubbleArray[i], 7, {delay:i*3, frame:190, ease:Linear.easeNone, onComplete:repeater, onCompleteParams:[i]});
			}
			
			addEventListener(Event.ENTER_FRAME, checkAlpha);
			scales.dollaStack.alpha = 0;
		}
		
		public function allTimedOut():void
		{
			removeEvent("all");
			doneButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
			
			for (var i:Number=0; i < bubbleArray.length; i++)
			{
				TweenMax.killTweensOf(bubbleArray[i]);
			}
			
			TweenMax.killTweensOf(dreamFace);
			TweenMax.killTweensOf(bground.bInner);
			TweenMax.killTweensOf(dreamText);	
			TweenMax.killTweensOf(medB);
			TweenMax.killTweensOf(bigB);
			TweenMax.killTweensOf(scales);
			TweenMax.killTweensOf(doneButt);
			TweenMax.killTweensOf(bigB.bigButts);
			TweenMax.killTweensOf(scales.dollaStack);
			TweenMax.killTweensOf(bigB.sliderBar.sliderMarker);
			sChannel.stop();
			playSArray = [];
			sChannel = null;
			removeEventListener(Event.ENTER_FRAME, checkAlpha);
			}
			
		private function onward(e:TuioTouchEvent = null)//e:TuioTouchEvent
		{
			trace("something obviously awesome is going to happen here. Possibly even life changing.");
			
			owner.howMuchMoney = score;
			
			allTimedOut();
			
			//startSound(playSArray[2]);
			owner.countdown();
			
			//sChannel.stop();
			//playSArray = [];
			//removeEvent("all");
			//TweenMax.killTweensOf(scales);
			//TweenMax.killAll();
			//owner.playSound(1);
			//owner.countdown();
		}
		
		/*private function onward(e:TuioTouchEvent = null)//e:TuioTouchEvent
		{
			trace("something obviously awesome is going to happen here. Possibly even life changing.");
			removeEvent("all");
			startSound(playSArray[2]);
			doneButt.removeEventListener(TuioTouchEvent.TOUCH_DOWN, onward);
			//owner.countdown();
		}*/
	}
}
