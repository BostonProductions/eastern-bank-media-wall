﻿
package code 
{	
	import flash.display.*;
	import flash.events.Event;
	import flash.events.TimerEvent;
	//import flash.events.TuioTouchEvent;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.utils.Timer;
	import flash.ui.Mouse;
	import flash.filters.BitmapFilter;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.BlurFilter;
	
	import com.greensock.*;
	import com.greensock.easing.*;
	import Box2D.Dynamics.*;
	import Box2D.Dynamics.Joints.*;
	import Box2D.Collision.*;
	import Box2D.Collision.Shapes.*;
	import Box2D.Common.Math.*; 
	
	import org.tuio.*;
	import org.tuio.adapters.*;
	import org.tuio.debug.TuioDebug;
	import org.tuio.TuioManager;
	import org.tuio.connectors.*;
	
	
		
	public class BillNinja extends Sprite
	{	
		/*
			Box2D body slicer, created by Antoan Angelov.
		*/
		public static const PM:uint = 30;
		private var world:b2World;
		private var polyShape:b2PolygonShape;
		private var enterPointsVec:Vector.<b2Vec2> = new Vector.<b2Vec2>();
		private var fixtureDef:b2FixtureDef;
		private var owner:Document;
		private var bg:Bg;
		private var endClip:EndClip;
		private var scoreboard:Scoreboard;
		private var billHeader:BillHeader;
		private var closeLarge:CloseLarge;
		
		private var mouseReleased:Boolean = false;
		private var lineFade:Boolean = false;
		
		private var cont:Sprite = new Sprite();
		private var objectsCont:Sprite = new Sprite();
		private var laserCont:Sprite = new Sprite();
		
		private var numEnterPoints:int = 0; 
		private var i:int;
		private var boxNum:int = 0;
		private var buckNum:int = 0;
		private var score:int = 0;
		private var tempScore:int = 0;
		private var cDown:Number = 60;
		private var explodeBuckText:int = 3;
		private var begX:Number; 
		private var begY:Number; 
		private var endX:Number; 
		private var endY:Number;
		private var gravityStep:Number = 0;
		private var newGravity:b2Vec2;
		
		private var slicedArray:Array = new Array();
		private var slicedBuckArray:Array = new Array();
		private var sliceGraphicArray:Array = new Array();
		private var explodeLocArray:Array = new Array();
		private var endArray:Array = new Array();
		
		private var respawnTimer:Timer;
		private var scoreTimer:Timer;
		private var buckTimer:Timer;		
		private var closeoutTimer:Timer;
		
		private var billTexture:BitmapData; 
		private var buckTexture1:BitmapData;
		private var buckTexture50:BitmapData;
		private var buckTexture100:BitmapData;
		
		private var curX:Number;
		private var curY:Number;
		
		public var whichNinja:Number;
		public var whichScreen:Number;
		
		private var isEnding:Boolean = false;
		
		public var isSolo:Boolean = false;
		
		public function BillNinja(xPos:Number, yPos:Number, myOwner:Document, w:Number, s:Number)
		{
			whichNinja = w;
			whichScreen = s;
			this.x = xPos;
			this.y = yPos;
			owner = myOwner;
			//owner.addText("the bill ninja is on screen " + whichScreen);
			//owner.addText("BillNinja");
			createNinjaWorld();
			
			laserCont.visible = false;
			//Mouse.hide();
			
			if (closeoutTimer != null)
			{
				closeoutTimer.reset();
				closeoutTimer.stop();
				closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			}
			
			bg.addEventListener(TuioTouchEvent.TOUCH_DOWN, mDown);
			addEventListener(Event.ENTER_FRAME, update);
			
			owner.addText("initial isEnding = " + isEnding);
		}
		
		private function createNinjaWorld()
		{ 		
			//owner.addText("createNinjaWorld");
			// First, I create the textures, which are BitmapData objects.
			var tempSpr:Sprite = new texture1();
			billTexture = new BitmapData(tempSpr.width, tempSpr.height);
			billTexture.draw(tempSpr);
			
			var tempSpr2:Sprite = new texture2();
			buckTexture1 = new BitmapData(tempSpr2.width, tempSpr2.height);
			buckTexture1.draw(tempSpr2);
			
			var tempSpr3:Sprite = new texture3();
			buckTexture50 = new BitmapData(tempSpr3.width, tempSpr3.height);
			buckTexture50.draw(tempSpr3);
			
			var tempSpr4:Sprite = new texture4();
			buckTexture100 = new BitmapData(tempSpr4.width, tempSpr4.height);
			buckTexture100.draw(tempSpr4);
		
			// World setup	
			world = new b2World(new b2Vec2(0, 2), true);
			createBody(400, -300, [new b2Vec2(-110/30, -65/25), new b2Vec2(110/30, -65/25), new b2Vec2(110/30, 75/10), new b2Vec2(-110/30, 75/10)], billTexture, String(boxNum), false);
			
			
			
			//add different timers. One for buck spawns, one for bill spawns the other for the timer
			respawnTimer = new Timer(6000); //3 seconds
			respawnTimer.addEventListener(TimerEvent.TIMER, repopulate);
			respawnTimer.start();
			
			var randomTime:int = 4000 + Math.random()*4000; //between 2-4 seconds
			buckTimer = new Timer(randomTime); 
			buckTimer.addEventListener(TimerEvent.TIMER, repopulateBucks);
			buckTimer.start();
			
			scoreTimer = new Timer(1000); //1 second
			scoreTimer.addEventListener(TimerEvent.TIMER, countdown);
			scoreTimer.start();
			
			//add object containers
			this.addChild(cont);
			cont.addChild(objectsCont);
			cont.addChild(laserCont);
			
			//create background. Allows for the TOUCH_DOWN events
			bg = new Bg();
			addChildAt(bg, 0);
			bg.alpha = .7;
			
			billHeader = new BillHeader();
			addChild(billHeader);
			
			//create scoreboard
			scoreboard = new Scoreboard();
			addChild(scoreboard);
			scoreboard.x = -20;
			scoreboard.y = -30;
			scoreboard.scaleX = scoreboard.scaleY = .5;
			
			closeLarge = new CloseLarge();
			addChild(closeLarge);
			closeLarge.x = 1080 - closeLarge.width;
			closeLarge.endTxt.text == " "
			closeLarge.addEventListener(TuioTouchEvent.TOUCH_DOWN, endClickX);
			
		}
		
		private function createBody(xPos:Number, yPos:Number, verticesArr:Array, texture:BitmapData, oName:String, buckBill:Boolean)
		{
			//owner.addText("createBody");
			var timesSliced:int = 0;
			var vec:Vector.<b2Vec2> = Vector.<b2Vec2>(verticesArr);
			
			//I use my own userData class to store the unique ID of each body, its vertices and its texture.
			var bodySprite:Sprite = new Sprite();
			bodySprite = new userData(numEnterPoints, vec, texture, oName, buckBill);
			bodySprite.x = xPos;
        	bodySprite.y = yPos;
			
			//Add the different parmeters to the body
			var bodyDef:b2BodyDef = new b2BodyDef();
			bodyDef.userData = bodySprite;
			bodyDef.position.Set(xPos / PM, yPos / PM);
			bodyDef.type = b2Body.b2_dynamicBody;
			bodyDef.angle = Math.random()*360;
			
			//Add a body shape
			var boxDef:b2PolygonShape = new b2PolygonShape();
			boxDef.SetAsVector(vec);
			
			//Add a fixture to the body and give it structure 
			var fixtureDef:b2FixtureDef = new b2FixtureDef();
			fixtureDef.shape = boxDef;
			fixtureDef.density = 2;
			fixtureDef.friction = .2;
			fixtureDef.restitution = 0;
			
			//Add the body to the world
			var body:b2Body = world.CreateBody(bodyDef);
			body.CreateFixture(fixtureDef);
			body.SetBullet(true);
			objectsCont.addChild(bodyDef.userData);
			
			// You can see the reason for creating the enterPointsVec in the coments in the intersection() method.	
			numEnterPoints++;
			enterPointsVec = new Vector.<b2Vec2>(numEnterPoints);
			slicedArray.push(timesSliced);
		}
		
		private function createMultipleBucks(amount:int, yPos:Number)
		{ 
			//owner.addText("createMultipleBucks");
			var randText:int;
			var randX:int;
			var randY:int;
			
			//create bodies = to the amount specified. An incremental global number (boxNum) is specified for the name, linked to index in array.
			for(i=0; i<amount; i++) 
			{
				//create a random texture for each buck at a random location
				boxNum++;
				buckNum++;
				randText = 1 + Math.random()*3;
				randX = 200 + Math.random()*600;
				randY = 300 + Math.random()*200;
				
				if(randText == 1)
				{
					createBody(randX, -randY, [new b2Vec2(-100/30, -40/25), new b2Vec2(100/30, -40/25), new b2Vec2(100/30, 12/10), new b2Vec2(-100/30, 12/10)], buckTexture1, String(buckNum), true);
				}
				if(randText == 2)
				{
					createBody(randX, -randY, [new b2Vec2(-100/30, -40/25), new b2Vec2(100/30, -40/25), new b2Vec2(100/30, 12/10), new b2Vec2(-100/30, 12/10)], buckTexture50, String(buckNum), true);
				}
				if(randText == 3)
				{
					createBody(randX, -randY, [new b2Vec2(-100/30, -40/25), new b2Vec2(100/30, -40/25), new b2Vec2(100/30, 12/10), new b2Vec2(-100/30, 12/10)], buckTexture100, String(buckNum), true);
				}
				
			}
		}
		
		private function createMultipleBills(amount:int, xPos:Number, yPos:Number)
		{
			//owner.addText("createMultipleBills");
			//create a body = to the amount specified. An incremental global number is specified for the name. 
			for(i=0; i<amount; i++) 
			{
				boxNum++;
				createBody(xPos, yPos, [new b2Vec2(-110/30, -65/25), new b2Vec2(110/30, -65/25), new b2Vec2(110/30, 75/10), new b2Vec2(-110/30, 75/10)], billTexture, String(boxNum), false);
			}
		}
		
		private function destroyMultipleBodies(yPosition:int, spr:Sprite, b:b2Body)
		{
			//owner.addText("destroyMultipleBodies");
			//If a body falls below a certain yPosition, delete it.
			if(spr.y > yPosition){
				world.DestroyBody(b);
				if (spr != null)
				{
					objectsCont.removeChild(spr);
				}
			}
		}
		
		function repopulate(event:TimerEvent):void 
		{
			//owner.addText("repopulate");
			//on timeout create a specified number of bodies
			var randX:int = 100 + Math.random()*600;
			createMultipleBills(1, randX, -300);
		}
		
		function repopulateBucks(event:TimerEvent):void 
		{
			//owner.addText("repopulateBucks");
			//on timeout create a specified number of bodies
			var randNum:int = 1+Math.random()*2;
			createMultipleBucks(randNum, -300);
		}
		
		function countdown(event:TimerEvent):void 
		{
			//owner.addText("countdown");
			//it's the final countdown! 
			var cFrame:int = scoreboard.circleLoader.currentFrame;
			cDown--;
			cFrame++;
			scoreboard.circleLoader.gotoAndStop(cFrame);
			
			//When countdown ends, call the end screen
			if(cDown <= 0){
				cDown = 0;				
				ending();				
			}
		}
		
		private function ending()
		{
			isEnding = true;
			
			owner.addText("******************************************************* ending isEnding = " + isEnding);
			
			removeEventListener(Event.ENTER_FRAME, update);
			scoreboard.scoreTimer.text = "0";
			//owner.addText("ending");
			//remove the listeners and stop timers
			respawnTimer.removeEventListener(TimerEvent.TIMER, repopulate);
			buckTimer.removeEventListener(TimerEvent.TIMER, repopulateBucks);
			scoreTimer.removeEventListener(TimerEvent.TIMER, countdown);
			scoreTimer.stop();
			respawnTimer.stop();
			buckTimer.stop();
			
			//destory all objects
			for(var p = world.GetBodyList(); p; p = p.GetNext())
			{
				var spr:Sprite = p.GetUserData();
				if(spr)
				{
					world.DestroyBody(p);
					if (spr != null)
					{
						objectsCont.removeChild(spr);
					}
				}	
			}
			
			//add the final score screen
			endClip = new EndClip();
			endClip.x = bg.x + bg.width/2;
			endClip.y = bg.y + bg.height/2;
			endClip.scoreText.text = scoreboard.scoreText.text;
			addChild(endClip);
			endArray.push(endClip);
			
			//add button listeners for restart and close
			endClip.restartBtn.endTxt.text = "Restart";
			endClip.closeBtn.endTxt.text = "Close";
			endClip.restartBtn.endTxt.mouseEnabled = false;
			endClip.closeBtn.endTxt.mouseEnabled = false;
			//endClip.restartBtn.endTxt.mouseChildren = false;
			//endClip.closeBtn.endTxt.mouseChildren = false;
			//endClip.restartBtn.mouseChildren = false;
			//endClip.closeBtn.mouseChildren = false;
			endClip.restartBtn.buttonMode = true;
			endClip.closeBtn.buttonMode = true;
			endClip.restartBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, endClick);
			endClip.closeBtn.addEventListener(TuioTouchEvent.TOUCH_DOWN, endClick);
			/*endClip.restartBtn.addEventListener(TuioTouchEvent.ROLL_OVER, endOn);
			endClip.closeBtn.addEventListener(TuioTouchEvent.ROLL_OVER, endOn);
			endClip.restartBtn.addEventListener(TuioTouchEvent.ROLL_OUT, endOut);
			endClip.closeBtn.addEventListener(TuioTouchEvent.ROLL_OUT, endOut);*/
			closeoutTimer = new Timer(20000, 1);
			closeoutTimer.start();
			closeoutTimer.addEventListener(TimerEvent.TIMER_COMPLETE, timeout);
		}
		
		private function timeout(e:TimerEvent):void
		{
			owner.addText("isEnding = " + isEnding);
			if (isEnding == true)
			{
				owner.addText("BillNinja timeout");
				closeoutTimer.reset();
				closeoutTimer.stop();
				closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
				owner.removeNewBillNinja(whichNinja, whichScreen, isSolo);
			}
		}
		
		private function endClick(e:TuioTouchEvent)
		{
			owner.addText("endClick");
			owner.addText(e.target.endTxt.text);
			//endClip.restartBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, endClick);
			//endClip.closeBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, endClick);
			closeoutTimer.reset();
			closeoutTimer.stop();
			closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			
			respawnTimer.reset();
			respawnTimer.stop();
			respawnTimer.removeEventListener(TimerEvent.TIMER, repopulate);
			scoreTimer.reset();
			scoreTimer.stop();
			scoreTimer.removeEventListener(TimerEvent.TIMER, countdown);
			buckTimer.reset();
			buckTimer.stop();	
			buckTimer.removeEventListener(TimerEvent.TIMER, repopulateBucks);	
			
			//reset all assets and play again!
			if(e.target.endTxt.text == "Restart")
			{
				isEnding = false;
				owner.addText("endClick reset isEnding = " + isEnding);
				addEventListener(Event.ENTER_FRAME, update);
				closeoutTimer.reset();
				closeoutTimer.stop();
				closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
				//remove end screen
				if (endArray[0] != null)
				{
					removeChild(endArray[0]);
					endArray.pop();
				}
				
				scoreboard.circleLoader.gotoAndStop(1);
				score = 0;
				cDown = 60;
				createBody(400, -300, [new b2Vec2(-110/30, -65/25), new b2Vec2(110/30, -65/25), new b2Vec2(110/30, 75/10), new b2Vec2(-110/30, 75/10)], billTexture, String(boxNum), false);
				
				respawnTimer.addEventListener(TimerEvent.TIMER, repopulate);
				buckTimer.addEventListener(TimerEvent.TIMER, repopulateBucks);
				scoreTimer.addEventListener(TimerEvent.TIMER, countdown);
				scoreTimer.start();
				respawnTimer.start();
				buckTimer.start();
			}
			
			//exit the game
			if(e.target.endTxt.text == "Close")
			{
				isEnding = false;
				owner.addText("endClick close isEnding = " + isEnding);
				//trace("close");
				if (closeoutTimer != null)
				{
					closeoutTimer.reset();
					closeoutTimer.stop();
					closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
				}
				
				owner.removeNewBillNinja(whichNinja, whichScreen, isSolo);
			}
			
			if(e.target.endTxt.text == " ")
			{
				isEnding = false;
				owner.addText("endClick topx isEnding = " + isEnding);
				owner.addText("top x close");
				if (closeoutTimer != null)
				{
					closeoutTimer.reset();
					closeoutTimer.stop();
					closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
				}
				
				owner.removeNewBillNinja(whichNinja, whichScreen, isSolo);
			}
		}
		
		private function endClickX(e:TuioTouchEvent):void
		{
			owner.addText("endClickX");
			//endClip.restartBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, endClick);
			//endClip.closeBtn.removeEventListener(TuioTouchEvent.TOUCH_DOWN, endClick);

			
			respawnTimer.reset();
			respawnTimer.stop();
			respawnTimer.removeEventListener(TimerEvent.TIMER, repopulate);
			scoreTimer.reset();
			scoreTimer.stop();
			scoreTimer.removeEventListener(TimerEvent.TIMER, countdown);
			buckTimer.reset();
			buckTimer.stop();	
			buckTimer.removeEventListener(TimerEvent.TIMER, repopulateBucks);	
			
			//reset all assets and play again!
			isEnding = false;
			owner.addText("endClickX isEnding = " + isEnding);
			owner.addText("top x close");
			if (closeoutTimer != null)
			{
				closeoutTimer.reset();
				closeoutTimer.stop();
				closeoutTimer.removeEventListener(TimerEvent.TIMER_COMPLETE, timeout);
			}
			
			owner.removeNewBillNinja(whichNinja, whichScreen, isSolo);
		}
		
		private function endOn(e:TuioTouchEvent)
		{
			//owner.addText("endOn");
			e.target.gotoAndStop(2);
		}
		
		private function endOut(e:TuioTouchEvent)
		{
			//owner.addText("endOut");
			e.target.gotoAndStop(1);
		}
		
		private function mDown(e:TuioTouchEvent)
		{
			var goOn:Boolean = false;
			var offset:Number;
			if (whichScreen == 1)
			{
				if (e.stageX < 1080)
				{
					goOn = true;
					offset = 0;
				}
			}
			if (whichScreen == 2)
			{
				if (e.stageX > 1080 && e.stageX <= 2160)
				{
					goOn = true;
					offset = 1080;
				}
			}
			if (whichScreen == 3)
			{
				if (e.stageX > 2160 && e.stageX <= 3240)
				{
					goOn = true;
					offset = 2160;
				}
			}
			if (whichScreen == 4)
			{
				if (e.stageX > 3240)
				{
					goOn = true;
					offset = 3240;
				}
			}
			
			if (goOn)
			{
				//owner.addText("mDown");
				//remove previous slashes
				for(i=0; i<sliceGraphicArray.length; i++) 
				{
					if (sliceGraphicArray[0] != null)
					{
						removeChild(sliceGraphicArray[0]);
						sliceGraphicArray.pop();
					}
				}
				
				//apply blur and other properties to slash
				var filter:BitmapFilter = new BlurFilter(2, 2, BitmapFilterQuality.HIGH);
				var myFilters:Array = new Array(filter);			
				var slasher:Slasher = new Slasher();
				
				begX = e.stageX - offset;
				begY = e.stageY;
				//owner.addText("begX = " + begX + ", begY = " + begY);
				slasher.x = begX;
				slasher.y = begY;
				sliceGraphicArray.push(slasher);
				slasher.visible = false;
				slasher.filters = myFilters;
				addChild(slasher);
				
				stage.addEventListener(TuioTouchEvent.TOUCH_UP, mUp);
				stage.addEventListener(TuioTouchEvent.TOUCH_MOVE, mMove);
			}
		}
		
		private function mMove(e:TuioTouchEvent)
		{
			var goOn:Boolean = false;
			var offset:Number;
			if (whichScreen == 1)
			{
				if (e.stageX < 1080)
				{
					goOn = true;
					offset = 0;
				}
			}
			if (whichScreen == 2)
			{
				if (e.stageX > 1080 && e.stageX <= 2160)
				{
					goOn = true;
					offset = 1080;
				}
			}
			if (whichScreen == 3)
			{
				if (e.stageX > 2160 && e.stageX <= 3240)
				{
					goOn = true;
					offset = 2160;
				}
			}
			if (whichScreen == 4)
			{
				if (e.stageX > 3240)
				{
					goOn = true;
					offset = 3240;
				}
			}
			
			if (goOn)
			{
				//owner.addText("mMove");
				var tempX:Number = e.stageX - offset;
				//trace("set tempX");
				var tempY:Number = e.stageY;
				//owner.addText("tempX = " + tempX + ", tempY = " + tempY);
				//trace("set tempY");
				//find the length of the created line
				var startP:Point = new Point(begX, begY);
				//trace("set startP");
				var endP:Point = new Point(tempX, tempY);
				//trace("set endP");
				var dist:Number = Point.distance(startP,endP);
				//trace("set dist");
				
				//create line
				laserCont.graphics.clear();
				//trace("clear graphics");
				laserCont.graphics.lineStyle(2);
				//trace("set linestyle");
				laserCont.graphics.moveTo(begX, begY);
				//trace("graphics moveTo");
				laserCont.graphics.lineTo(e.stageX - offset, e.stageY);
				//owner.addText("e.stageX = " + e.stageX - offset + ", e.stageY = " + e.stageY);
				//trace("graphics lineTo");
				
				// find out mouse coordinates to find out the angle
				var cy:Number = e.stageY - sliceGraphicArray[0].y; 
				//trace("set cy");
				var cx:Number = e.stageX - offset - sliceGraphicArray[0].x;
				//trace("set cx");
				var Radians:Number = Math.atan2(cy,cx);
				//trace("set radians");
				// convert to degrees to rotate
				var Degrees:Number = Radians * 180 / Math.PI;
				//trace("set degrees");
				// rotate
				sliceGraphicArray[0].rotation = Degrees;
				//trace("set rotation");
				sliceGraphicArray[0].scaleX = dist/250;
				//trace("set scaleX");
				sliceGraphicArray[0].visible = true;
				//trace("set visible");
				sliceGraphicArray[0].alpha = 1;
				//trace("set alpha");
			}
		}
		
		private function mUp(e:TuioTouchEvent)
		{
			var goOn:Boolean = false;
			var offset:Number;
			if (whichScreen == 1)
			{
				if (e.stageX < 1080)
				{
					goOn = true;
					offset = 0;
				}
			}
			if (whichScreen == 2)
			{
				if (e.stageX > 1080 && e.stageX <= 2160)
				{
					goOn = true;
					offset = 1080;
				}
			}
			if (whichScreen == 3)
			{
				if (e.stageX > 2160 && e.stageX <= 3240)
				{
					goOn = true;
					offset = 2160;
				}
			}
			if (whichScreen == 4)
			{
				if (e.stageX > 3240)
				{
					goOn = true;
					offset = 3240;
				}
			}
			
			if (goOn)
			{
				//owner.addText("mUp");
				//remove line, slash and listeners
				mouseReleased = true;
				curX = e.stageX - offset;
				curY = e.stageY;
				//owner.addText("e.stageX = " + e.stageX - offset + ", e.stageY = " + e.stageY);
				lineFade = true;
				laserCont.graphics.clear();
				
				if (sliceGraphicArray[0] != null)
				{
					removeChild(sliceGraphicArray[0]);
					sliceGraphicArray.pop();
				}
				
				stage.removeEventListener(TuioTouchEvent.TOUCH_UP, mUp);
				stage.removeEventListener(TuioTouchEvent.TOUCH_MOVE, mMove);
			}
		}
		
		private function explosion(p1:Number, p2:Number, p3:Number, p4:Number)
		{
			//owner.addText("explosion");
			//find the point halfway between the created slash
			var exploder:Exploder = new Exploder();
			var begPoint:Point = new Point(p1, p2);
			var endPoint:Point = new Point(p3, p4);
			var explosionPoint:Point = halfwayPoint(begPoint, endPoint, .5);
			var tempX:Number;
			var tempY:Number;
			
			//create an explosion at the halfway point
			exploder.scaleX = exploder.scaleY = 0;
			exploder.x = explosionPoint.x*30;
			exploder.y = explosionPoint.y*30;
			tempX = exploder.x;
			tempY = exploder.y;
			addChild(exploder);
			explodeLocArray.push(tempX);
			explodeLocArray.push(tempY);
			
			TweenMax.to(exploder,.2,{alpha:0, scaleX:1, scaleY:1, ease:Linear.easeNone, onComplete:explodeFinish, onCompleteParams:[exploder]});
		}
		
		private function explodeFinish(explosion:Exploder)
		{
			//owner.addText("explodeFinish");
			//remove explosion and clear location arrays
			if (explosion != null)
			{
				removeChild(explosion);
			}
			explodeLocArray.pop();
			explodeLocArray.pop();
		}
		
		private function explodeText(p1:Number, p2:Number, buckType:Boolean, amount:int)
		{
			//owner.addText("explodeText");
			//create the explosion text at the halfway point on slash
			var plusText:PlusText = new PlusText();
			plusText.alpha = 0;
			plusText.x = p1;
			plusText.y = p2;
			addChild(plusText);
			plusText.scoreText.mouseEnabled = false;
			//plusText.scoreText.mouseChildren = false;
			plusText.mouseEnabled = false;
			plusText.mouseChildren = false;
			
			//change amount = to the type slashed
			if(buckType == true)
			{
				plusText.scoreText.text = "Ow! Paper Cut!"
			}
			if(buckType == false)
			{
				if (amount <= 30)
				{
					plusText.scoreText.text = "Nice Slice!"
				}
				if (amount > 30 && amount <= 60)
				{
					plusText.scoreText.text = "Severe Shear!"
				}
				if (amount > 60 && amount <= 100)
				{
					plusText.scoreText.text = "Chop Champ!"
				}
				if (amount > 100)
				{
					plusText.scoreText.text = "Hack Attack!"
				}
			}
			
			TweenMax.to(plusText,1,{alpha:1, y:plusText.y-50, ease:Strong.easeOut, onComplete:explodeTextFinish, onCompleteParams:[plusText]});
		}
		
		private function explodeTextFinish(plusText:PlusText)
		{
			if (plusText != null)
			{
				removeChild(plusText);
			}
		}
		
		private function halfwayPoint(point1:Point, point2:Point, amount:Number):Point
		{
			//owner.addText("halfwayPoint");
			//find the halfway mark of the slash
			var newPoint:Point;
			newPoint = Point.interpolate(point1, point2, amount);
			return newPoint;
		}
		
		public function update(e:Event):void 
		{		
			//owner.addText("update");
			//change the text of the scoreboard and timer
			scoreboard.scoreTimer.text = String(cDown);
			scoreboard.scoreText.text = commaCoder(score);
			scoreboard.scoreText.mouseEnabled = false;
			//scoreboard.scoreText.mouseChildren = false;
			scoreboard.scoreTimer.mouseEnabled = false;
			//scoreboard.scoreTimer.mouseChildren = false;
			
			//fade out the slash. When at 0, remove it
			/*if(lineFade == true){
				sliceGraphicArray[0].alpha -= .2;
				if(sliceGraphicArray[0].alpha <= 0){
					removeChild(sliceGraphicArray[0]);
					sliceGraphicArray.pop();
					lineFade = false;
				}
			}*/
			
			//create the explosion text depending on the type
			if(explodeBuckText == 1){
				explodeText(explodeLocArray[0], explodeLocArray[1], true, tempScore);
				explodeBuckText = 3;
			}
			
			if(explodeBuckText == 2){
				explodeText(explodeLocArray[0], explodeLocArray[1], false, tempScore);
				explodeBuckText = 3;
			}
			
			if(mouseReleased)
			{
				//trace("update mouseReleased");
				// Here I use the world.RayCast() method (I use it twice, see why in the comments in the intersection() method below) to get the intersection points between the line you just drew and all bodies in the Box2D world.
				endX = curX;
				endY = curY;
				
				var p1:b2Vec2 = new b2Vec2(begX/30, begY/30);
				var p2:b2Vec2 = new b2Vec2(endX/30, endY/30);
				
				world.RayCast(intersection, p1, p2);
				world.RayCast(intersection, p2, p1);
				enterPointsVec = new Vector.<b2Vec2>(numEnterPoints);				
				
				mouseReleased = false;
			}
			
			gravityStep = 2 + (score*.0001)/10;
			newGravity = world.GetGravity();
			newGravity.y = gravityStep;
			world.SetGravity(newGravity);
			world.Step(1/24, 90, 90);			
			world.ClearForces();
		
			// Here all the bodies' Sprite equivalents are synchronized to the bodies themselves.
			var p:b2Body;
			var spr:Sprite;
			for(p = world.GetBodyList(); p; p = p.GetNext())
			{
				spr = p.GetUserData();
				if(spr)
				{
					spr.x = p.GetPosition().x*30;
					spr.y = p.GetPosition().y*30;
					spr.rotation = p.GetAngle()*180/Math.PI;
					spr.name = p.GetUserData().objectName;
					
					destroyMultipleBodies(2150, spr, p);
				}
			}
		}
		
		private function intersection(fixture:b2Fixture, point:b2Vec2, normal:b2Vec2, fraction:Number):Number
		{
			//owner.addText("intersection");
			var spr:Sprite = fixture.GetBody().GetUserData();
			// Throughout this whole code I use only one global vector, and that is enterPointsVec. Why do I need it you ask? 
			// Well, the problem is that the world.RayCast() method calls this function only when it sees that a given line gets into the body - it doesnt see when the line gets out of it.
			// I must have 2 intersection points with a body so that it can be sliced, thats why I use world.RayCast() again, but this time from B to A - that way the point, at which BA enters the body is the point at which AB leaves it!
			// For that reason, I use a vector enterPointsVec, where I store the points, at which AB enters the body. And later on, if I see that BA enters a body, which has been entered already by AB, I fire the splitObj() function!
			// I need a unique ID for each body, in order to know where its corresponding enter point is - I store that id in the userData of each body.
			
			if(spr is userData)
			{
				var userD:userData = spr as userData;
				
				if(enterPointsVec[userD.id])
				{					
					// If this body has already had an intersection point, then it now has two intersection points, thus it must be split in two - thats where the splitObj() method comes in.
					// Before calling splitObj() however, I first draw the two intersection points - the blue one is the enter point and the red one is the end point.
					splitObj(fixture.GetBody(), enterPointsVec[userD.id], point.Copy());
				}
				else enterPointsVec[userD.id] = point;
			}
			
			return 1;
		}
		
		private function splitObj(sliceBody:b2Body, A:b2Vec2, B:b2Vec2):void
		{
			//owner.addText("splitObj");
			var origFixture:b2Fixture = sliceBody.GetFixtureList();
			var poly:b2PolygonShape = origFixture.GetShape() as b2PolygonShape;
			var verticesVec:Vector.<b2Vec2> = poly.GetVertices();
			var numVertices:int = poly.GetVertexCount();
			var shape1Vertices:Vector.<b2Vec2> = new Vector.<b2Vec2>(); 
			var shape2Vertices:Vector.<b2Vec2> = new Vector.<b2Vec2>();
			var origUserData:userData = sliceBody.GetUserData();
			var origUserDataId:int = origUserData.id;
			var d:Number;
			var origName:String = origUserData.objectName;
			var origBoolean:Boolean = origUserData.isBuck;
			var explosionPoint:Point;
			var temporaryScore:int;
			var sliced:int = origUserData.timesSliced;
			
			//Add explosion inbetween the intersection points.
			explosion(A.x, A.y, B.x, B.y);
			
			//for each object in the world, if it gets sliced, record how many times in the slicedArray corresponding to the appropriate object by name
			for(var p:b2Body = world.GetBodyList(); p; p = p.GetNext())
			{
				var spr:Sprite = p.GetUserData();
				if(spr)
				{
					spr.name = p.GetUserData().objectName;
					var nameToIndex:int = Number(spr.name);
					//trace("spr.name = " + spr.name);
					//trace("nameToIndex = " + nameToIndex);
					
					//find the name of the object getting sliced and find it's appropriate index number in the array
					if(origUserData.objectName == spr.name)
					{
						//If it is a buck, subtract 100 * the amount it has been sliced
						if(origUserData.isBuck == true)
						{ 
							slicedArray[nameToIndex]++;
							temporaryScore = temporaryScore + 100;
							tempScore = temporaryScore;
							explodeBuckText = 1;
							score -= tempScore;
							if(score < 0)
							{
								score = 0;
							}
						}
						//If it is a bill, add 10 * the amount it has been sliced
						if(origUserData.isBuck == false)
						{
							//trace("*********************************");
							//trace("before update slicedArray[nameToIndex] = " + slicedArray[nameToIndex]);
							slicedArray[nameToIndex]++;
							//trace("after update slicedArray[nameToIndex] = " + slicedArray[nameToIndex]);
							//trace("before update temporaryScore = " + temporaryScore);
							temporaryScore = temporaryScore + 10;
							//trace("after update temporaryScore = " + temporaryScore);
							tempScore = temporaryScore;
							//trace("tempScore = " + tempScore);
							explodeBuckText = 2;
							//trace("before update score = " + score);
							score += tempScore;
							//trace("after update score = " + score);
						}
					}
				}
			}			
			// First, I destroy the original body and remove its Sprite representation from the childlist.
			world.DestroyBody(sliceBody);
			if (origUserData != null)
			{
				objectsCont.removeChild(origUserData);
			}
			
			// The world.RayCast() method returns points in world coordinates, so I use the b2Body.GetLocalPoint() to convert them to local coordinates.
			A = sliceBody.GetLocalPoint(A);
			B = sliceBody.GetLocalPoint(B);

			// I use shape1Vertices and shape2Vertices to store the vertices of the two new shapes that are about to be created. 
			// Since both point A and B are vertices of the two new shapes, I add them to both vectors.
			shape1Vertices.push(A, B);
			shape2Vertices.push(A, B);

			// I iterate over all vertices of the original body. 
			// I use the function det() ("det" stands for "determinant") to see on which side of AB each point is standing on. The parameters it needs are the coordinates of 3 points:
			// - if it returns a value >0, then the three points are in clockwise order (the point is under AB)
			// - if it returns a value =0, then the three points lie on the same line (the point is on AB)
			// - if it returns a value <0, then the three points are in counter-clockwise order (the point is above AB). 
			for(i=0; i<numVertices; i++) 
			{
				d = det(A.x, A.y, B.x, B.y, verticesVec[i].x, verticesVec[i].y);
				if(d>0) shape1Vertices.push(verticesVec[i]);
				else shape2Vertices.push(verticesVec[i]);
			}			

			// In order to be able to create the two new shapes, I need to have the vertices arranged in clockwise order.
			// I call my custom method, arrangeClockwise(), which takes as a parameter a vector, representing the coordinates of the shape's vertices and returns a new vector, with the same points arranged clockwise.
			shape1Vertices = arrangeClockwise(shape1Vertices);
			shape2Vertices = arrangeClockwise(shape2Vertices);

			// setting the properties of the two newly created shapes
			var bodyDef:b2BodyDef = new b2BodyDef();
			bodyDef.type = b2Body.b2_dynamicBody;
			bodyDef.position.SetV(sliceBody.GetPosition());
			fixtureDef = new b2FixtureDef();
			fixtureDef.density = origFixture.GetDensity();
			fixtureDef.friction = origFixture.GetFriction();
			fixtureDef.restitution = origFixture.GetRestitution();
			
			// creating the first shape
			polyShape = new b2PolygonShape();
			polyShape.SetAsVector(shape1Vertices);
			fixtureDef.shape = polyShape;
			
			bodyDef.userData = new userData(origUserDataId, shape1Vertices, origUserData.texture, origName, origBoolean);
			objectsCont.addChild(bodyDef.userData);
			enterPointsVec[origUserDataId] = null;
			
			var body:b2Body;
			body = world.CreateBody(bodyDef);
			body.SetAngle(sliceBody.GetAngle());
			body.CreateFixture(fixtureDef);
			body.SetBullet(true);
			body.SetAngularVelocity(5);
			
			// creating the second shape
			polyShape = new b2PolygonShape();
			polyShape.SetAsVector(shape2Vertices);
			fixtureDef.shape = polyShape;
		
			bodyDef.userData = new userData(numEnterPoints, shape2Vertices, origUserData.texture, origName, origBoolean);
			objectsCont.addChild(bodyDef.userData);
			enterPointsVec.push(null);
			numEnterPoints++;
			
			body = world.CreateBody(bodyDef);
			body.SetAngle(sliceBody.GetAngle());
			body.CreateFixture(fixtureDef);
			body.SetBullet(true);		
		}
		
		private function arrangeClockwise(vec:Vector.<b2Vec2>):Vector.<b2Vec2>
		{
			//owner.addText("arrangeClockwise");
			// The algorithm is simple: 
			// First, it arranges all given points in ascending order, according to their x-coordinate.
			// Secondly, it takes the leftmost and rightmost points (lets call them C and D), and creates tempVec, where the points arranged in clockwise order will be stored.
			// Then, it iterates over the vertices vector, and uses the det() method I talked about earlier. It starts putting the points above CD from the beginning of the vector, and the points below CD from the end of the vector. 
			var n:int = vec.length, d:Number, i1:int = 1, i2:int = n-1;
			var tempVec:Vector.<b2Vec2> = new Vector.<b2Vec2>(n), C:b2Vec2, D:b2Vec2;

			vec.sort(comp1);			
			tempVec[0] = vec[0];
			C = vec[0];
			D = vec[n-1];
			
			for(i=1; i<n-1; i++)
			{
				d = det(C.x, C.y, D.x, D.y, vec[i].x, vec[i].y);
				if(d<0) tempVec[i1++] = vec[i];
				else tempVec[i2--] = vec[i];
			}
			
			tempVec[i1] = vec[n-1];
			return tempVec;
		}
		
		function commaCoder(yourNum):String 
		{
			//owner.addText("commaCoder");
			//Adds a comma every 3 characters
    		var numtoString:String = new String();
    		var numLength:Number = yourNum.toString().length;
    		numtoString = "";

    		for (var i=0; i<numLength; i++) { 
    			if ((numLength-i)%3 == 0 && i != 0) {
    				numtoString += ",";
    			}
    			numtoString += yourNum.toString().charAt(i);
    		}
    		return numtoString;
		}
		
		private function comp1(a:b2Vec2, b:b2Vec2):Number
		{
			//owner.addText("comp1");
			// This is a compare function, used in the arrangeClockwise() method - a fast way to arrange the points in ascending order, according to their x-coordinate.
			if(a.x>b.x) return 1;
			else if(a.x<b.x) return -1;
			return 0;
		}
		
		private function det(x1:Number, y1:Number, x2:Number, y2:Number, x3:Number, y3:Number):Number
		{
			//owner.addText("det");
			// This is a function which finds the determinant of a 3x3 matrix.
			// If you studied matrices, you'd know that it returns a positive number if three given points are in clockwise order, negative if they are in anti-clockwise order and zero if they lie on the same line.
			// Another useful thing about determinants is that their absolute value is two times the face of the triangle, formed by the three given points.
			return x1*y2+x2*y3+x3*y1-y1*x2-y2*x3-y3*x1;    
		}
	}
}
