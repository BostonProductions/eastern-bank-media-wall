package com.transcendingdigital.data
{
	import com.technogumbo.utils.NumberUtils;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	
	/**
	 * This is a generic class for file caching reguardless of the media type.
	 * It downloads things and places them in the local file system.
	 * 
	 * It also will check the local file system for a file with the name and size specified
	 * before preforming a download operation.
	 */
	public class genericFileCaching extends EventDispatcher
	{
		public static const LOADED:String = "com.transcendingdigital.data.genericFileCaching.LOADED";
		public static const ERROR:String = "com.transcendingdigital.data.genericFileCaching.ERROR";
		
		private var _pathToDL:String = "";
		private var _loadedPath:String = "";
		private var _loadedByteSize:Number = 0;
		private var _expectedBSize:Number = 0;
		private var _subCacheDir:String = "";
		
		private var remoteFileLoader:URLLoader;
		
		public function genericFileCaching(target:IEventDispatcher=null)
		{
			super(target);
		}
		
		public function init(_mediaPath:String, _expectedByteSize:Number = 0, _subCacheDirectory:String = ""):void {
			_pathToDL = _mediaPath;
			_expectedBSize = _expectedByteSize;
			_subCacheDir = _subCacheDirectory;
			
			doLocalFilesystemCheck();
		}
		
		public function getOriginalDownloadLink():String {
			return _pathToDL;
		}
		public function getFinalDownloadPath():String {
			return _loadedPath;
		}
		public function getFinalByteSize():Number {
			return _loadedByteSize;
		}
		public function destroyInternals():void {
			destroyRemoteFileLoader();
		}
		private function doLocalFilesystemCheck():void {
			var cachePrefix:String = "app-storage:/";
			var doDownload:Boolean = false;
			
			if(GlobalDataStorage['cacheDirectoryPrefix'] != undefined) {
				cachePrefix = GlobalDataStorage['cacheDirectoryPrefix'];
			}
			if(_subCacheDir != "") {
				cachePrefix += _subCacheDir + "/";
			}
			
			var fileNameOnly:String = NumberUtils.getFileNameOnly(_pathToDL);
			
			var tmpFile:File = new File(cachePrefix + fileNameOnly);
			if(tmpFile.exists) {
				if(tmpFile.size != _expectedBSize) {
					doDownload = true;
					// Set the path remote
					_loadedPath = _pathToDL;
				} else {
					// Set the path local
					_loadedPath = cachePrefix + fileNameOnly;
					_loadedByteSize = tmpFile.size;
				}
			} else {
				doDownload = true;
				// Set the path remote
				_loadedPath = _pathToDL;
			}
			
			
			if(doDownload == true) {
				createRemoteFileLoader();
			} else {
				dispatchEvent( new Event(genericFileCaching.LOADED, false, true) );
			}
			
		}
		
		/*
		* @private
		* Begins the process of loading a remote file then writing a copy of that file into the local cache
		*/
		private function createRemoteFileLoader():void {
			if(remoteFileLoader == null) 
			{
				remoteFileLoader = new URLLoader();
				remoteFileLoader.addEventListener(Event.COMPLETE, handleRemoteLoadCoplete);
				remoteFileLoader.addEventListener(IOErrorEvent.IO_ERROR, handleRemoteLoadError, false, 0, true);
				remoteFileLoader.dataFormat = URLLoaderDataFormat.BINARY;
				var request:URLRequest = new URLRequest(_loadedPath);
				request.cacheResponse = false;
				
				remoteFileLoader.load( request );
			} else {
				destroyRemoteFileLoader(true);
			}
		}
		
		/*
		* @private
		* Gets rid of the remote file loader
		*/
		private function destroyRemoteFileLoader(_OptionalCallback:Boolean = false):void {
			if (remoteFileLoader != null) {
				try {
					remoteFileLoader.close();
				} catch (e:Error) {
					// No big deal, no load was in progress.  Just sink it
				}
				
				remoteFileLoader.removeEventListener(Event.COMPLETE, handleRemoteLoadCoplete);
				remoteFileLoader.removeEventListener(IOErrorEvent.IO_ERROR, handleRemoteLoadError);
				remoteFileLoader = null;
				
				if (_OptionalCallback == true) {
					createRemoteFileLoader();
				}
			}
		}
		
		/*
		* @private
		* Event for handling the end of file loading
		* on this projects CMS (UTES Football). The CMS
		* has problems where it generates blank files for xml
		* we need to NOT cache if thats the case.
		*/
		private function handleRemoteLoadCoplete(e:Event):void {
			
			var justFileName:String = NumberUtils.getFileNameOnly( _loadedPath );
			var cachePrefix:String = "app-storage:/";
			
			if(GlobalDataStorage['cacheDirectoryPrefix'] != undefined) {
				cachePrefix = GlobalDataStorage['cacheDirectoryPrefix'];
			}
			if(_subCacheDir != "") {
				cachePrefix += _subCacheDir + "/";
			}
			
			var newCacheFile:File  = new File( cachePrefix + justFileName);
			
			if(remoteFileLoader.bytesLoaded > 0) {
				try {
					var stream:FileStream = new FileStream();
					stream.open(newCacheFile, FileMode.WRITE);
					stream.writeBytes(remoteFileLoader.data);
					stream.close();
					stream = null;
				} catch (e:Error) {
					// Dont do anything.  Its possible the cached file was open and we tried to over-wirte it.
					// Thats fine because we'll just allow loading of the previous cached file
				}
			}
			destroyRemoteFileLoader();
			
			// Point the loaded file at the local cache
			if(newCacheFile.exists) {
				_loadedPath = newCacheFile.nativePath;
				_loadedByteSize = newCacheFile.size;
			}
			
			dispatchEvent( new Event(genericFileCaching.LOADED, false, true) );
		}
		
		/*
		* @private
		* Event for handling file load error
		*/
		private function handleRemoteLoadError(e:IOErrorEvent):void {
			destroyRemoteFileLoader();
			// Bummer. We need to check if there is an older version of this file in
			// cache; if so, we use that.
			var justFileName:String = NumberUtils.getFileNameOnly( _loadedPath );
			var cachePrefix:String = "app-storage:/";
			
			if(GlobalDataStorage['cacheDirectoryPrefix'] != undefined) {
				cachePrefix = GlobalDataStorage['cacheDirectoryPrefix'];
			}
			if(_subCacheDir != "") {
				cachePrefix += _subCacheDir + "/";
			}
			
			var tmpFile:File = new File(cachePrefix + justFileName);
			if(tmpFile.exists) {
				_loadedPath = tmpFile.nativePath;
				_loadedByteSize = tmpFile.size;
				dispatchEvent( new Event(genericFileCaching.LOADED, false, true) );
			} else {
				dispatchEvent( new Event(genericFileCaching.ERROR, false, true) );
			}
		}

	}
}