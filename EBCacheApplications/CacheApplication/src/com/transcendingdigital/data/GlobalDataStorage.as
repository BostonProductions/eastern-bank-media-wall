package com.transcendingdigital.data
{
	import com.transcendingdigital.data.vo.cacheFileData;
	import com.transcendingdigital.data.vo.cacheFileSegment;
	import com.transcendingdigital.data.vo.cacheMediaWallGameData;
	import com.transcendingdigital.data.vo.cacheMediaWallPageData;
	import com.transcendingdigital.data.vo.xmlCacheVO;
	
	import flash.geom.Point;

	/**
	 * Class is used for application global data
	 */
	public class GlobalDataStorage
	{
		// -----------------------------------
		// DATA
		// -----------------------------------
		public static var configXML:XML;
		public static var uiSize:Point;
		
		public static var mostRecentCMSPullTime:Date;
		public static var guideXMLFiles:Vector.<xmlCacheVO> = null;
		
		/**
		 * Determines where our local cache resides
		 */
		public static var cacheDirectoryPrefix:String = "app-storage:/";
		
		// These data structures are volitle structures used as we download
		// data from the CMS.
		public static var currentCachingText:cacheFileSegment = null;
		public static var currentCachingPhotos:cacheFileSegment = null;
		public static var currentCachingVideos:cacheFileSegment = null;
		public static var currentCachingMediaWallGameData:cacheMediaWallGameData = null;
		public static var currentCachingMediaWallData:Vector.<cacheMediaWallPageData> = null;
		
		// -----------------------------------
		// SECTION CONSTS
		// -----------------------------------
		public static const SECTION_INITIAL_LOAD:String = "SECTION_INITIAL_LOAD";
		// -----------------------------------
		// GLOBAL EVENT CONSTS
		// -----------------------------------
		public static const INITIAL_CACHE_COMPLETE:String = "com.transcendingdigital.data.GlobalDataStorage.INITIAL_CACHE_COMPLETE";
		public static const REQUEST_SECTION:String = "com.transcendingdigital.data.GlobalDataStorage.REQUEST_SECTION";
		public static const REQUEST_TRANSITION_OFF:String = "com.transcendingdigital.data.GlobalDataStorage.REQUEST_TRANSITION_OFF";
	}
}