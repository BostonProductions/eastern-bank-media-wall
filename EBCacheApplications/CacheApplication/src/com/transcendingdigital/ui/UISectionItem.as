package com.transcendingdigital.ui
{
	import com.bostonproductions.sound.soundBase;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	public class UISectionItem extends soundBase
	{
		public var view:MovieClip;
		
		public function UISectionItem()
		{
			super();
		}
		public function init():void {
			
		}
		
		public function destroyInternals():void {
			this.destroySoundFX();
		}
	}
	
}