package com.technogumbo.utils
{
	import flash.geom.Point;
	
	public class NumberUtils
	{
		public function NumberUtils()
		{
		}
		
		public static function getRandNumberInRange(_Low:int, _High:int):int {
			var returnNum:int = 0;
			
			returnNum =  Math.floor( ( Math.random() * (_High - _Low) ) + _Low);

			return returnNum;
		}
		
		public static function getRandTenthValue():Number {
			var returnNum:Number = 0.1;
			var selections:Array = new Array(0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95);
			
			var targetIndex:int = NumberUtils.getRandNumberInRange(0, selections.length - 1);
			
			returnNum =  selections[targetIndex];
			
			return returnNum;
		}
		
		/**
		 * Returns a randomly generated string of random length
		 * @param	_Low
		 * @param	_Hight
		 * @return
		 */
		public static function getRandomStringOfLength(_Low:int, _High:int):String {
			var returnString:String = "";
			var finLen:int = getRandNumberInRange(_Low, _High);
			var alphabet:Array = new Array("a", "A","b", "B","c", "C","d", "D","e", "E","f", "F","g", "G","h", "H","i", "I","j", "J","k", "K","l", "L","m", "M","n", "N","o", "O","p", "P","q", "Q","r", "R","s", "S","t", "T","u", "U","v", "V","w", "W","x", "X","y", "Y","z", "Z","/",":","&",".");
			
			for (var i:int = 0; i < finLen; i++) {
				returnString = returnString.concat( alphabet[ getRandNumberInRange(0, alphabet.length) ] );
			}
			
			return returnString;
		}
		
		/**
		 * Generates a random filename that includes the date
		 */
		public static function generateRandomizedDateIncludedFileNameString(_Prepend:String = ""):String {
			var returnString:String = "";
			
			var CurrentDate:Date = new Date();
			var FourDigYear:uint = CurrentDate.getFullYear();
			var Month:uint = CurrentDate.getMonth() + 1;
			var Day:uint = CurrentDate.getDay();
			var Hour:uint = CurrentDate.getHours();
			var Minutes:uint = CurrentDate.getMinutes();
			var Seconds:uint = CurrentDate.getSeconds();
			var Milliseconds:uint = CurrentDate.getMilliseconds();
			
			returnString = _Prepend + String(FourDigYear) + "_" + String(Month) + "_" + String(Day) + "_" + String(Hour) + String(Minutes) + String(Seconds) + String(Milliseconds) + getRandNumberInRange(0,999) + getRandNumberInRange(0,999) + getRandNumberInRange(0,999);
			
			return returnString;
		}
		
		/**
		 * Used for resizing a display object reference and maintaining its aspect ratio.
		 * @param	itemH
		 * @param   itemW
		 * @param	maxHeight
		 * @param	maxWidth
		 */
		public static function ResizeItem(itemH:Number, itemW:Number, maxHeight:Number, maxWidth:Number):Point
		{
			var returnPnt:Point = new Point(itemH, itemW);
			
			// record the original height and width which is needed to determine a ratio
			var origHeight:Number = itemH;
			var origWidth:Number = itemW;
			
			// if the maxWidth is greater than the maxHeight, set the height to the max height and the width to the percentage of width to height
			
			if(origHeight > origWidth)
			{
				returnPnt.y = maxHeight;
				returnPnt.x = maxHeight * (origWidth / origHeight);
				
				if(returnPnt.x > maxWidth)
				{
					var tempWidth:Number = returnPnt.x;
					returnPnt.x = maxWidth;
					returnPnt.y = maxWidth * (returnPnt.y/tempWidth);
				}
			}
			else if(origHeight < origWidth)
			{
				returnPnt.x = maxWidth;
				returnPnt.y = maxWidth * (origHeight / origWidth);
				if(returnPnt.y > maxHeight)
				{
					var tempHeight:Number = returnPnt.y;
					returnPnt.y = maxHeight;
					returnPnt.x = maxHeight * (returnPnt.x/tempHeight);
				}
			}
				
			else
			{
				returnPnt.y = maxHeight;
				returnPnt.x = maxWidth;
			}
			
			return returnPnt;
		}
		
		/*
		* @private
		* Takes a string of a path like http://192.168.0.10/drupal/sites/default/files/factory_Cam/videos/Sequence1-TURKEYHILL-MPEG-4.mov
		* or videos/Sequence1-TURKEYHILL-MPEG-4.mov
		* or \\ENTERPRISE\Shared_Documents\Sequence1-TURKEYHILL-MPEG-4.mov
		* or Sequence1-TURKEYHILL-MPEG-4.mov
		* and returns everything after the last slash, or just everything if there is no slash
		*/
		public static function getFileNameOnly(_FullPath:String):String {
			var returnString:String = "";
			
			// Find the last slash
			var lastSlashPos:int = -1;
			
			// Are they back slashes?
			lastSlashPos = _FullPath.lastIndexOf("/");
			
			// Ok, no dice try the other direction
			if (lastSlashPos == -1) {
				lastSlashPos = _FullPath.lastIndexOf("\\");
			}
			
			// If we got a match on something, shorten the string
			if (lastSlashPos != -1) {
				returnString = _FullPath.substr((lastSlashPos + 1), _FullPath.length - lastSlashPos);
			}
			
			return returnString;
		}
	}
}