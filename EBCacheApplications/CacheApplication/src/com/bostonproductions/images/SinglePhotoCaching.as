﻿package com.bostonproductions.images {
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Loader;
	import flash.display.Sprite;
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;

	// ----------------- End For File Caching ---------------
	
	public class SinglePhotoCaching extends Sprite {
		
		public static var PHOTOLOADED:String = "SinglePhotoCaching_PHOTOLOADED";
		public static var LOADPROGRESS:String = "SinglePhotoCaching_LOADPROGRESS";
		public static var IOERROR:String = "SinglePhotoCaching_IOERROR";
		
		public static const ALIGN_LEFT:String = "ALIGN_LEFT";
		public static const ALIGN_RIGHT:String = "ALIGN_RIGHT";
		public static const ALIGN_CENTER:String = "ALIGN_CENTER";
		
		private var url:URLRequest;
		private var pathToFile:String = "";
	    private var photoLoader:Loader;
		private var Photo:Bitmap;
		public var rawBitmapData:BitmapData;
		private var alignment:String = ALIGN_LEFT;
		
		private var OptionalDataStorageObject:Object = new Object;
		private var justGetBitmapData:Boolean; //DN update in case you just want BitmapData
		
		public var resizedWidth:Number = 0;
		public var resizedHeight:Number = 0;
		
		// ---------------------- For File Caching --------------
		
		private var expectedFileSize:Number = 0;
		private var remoteFileLoader:URLLoader;
		private var forcedCacheLocation:String = "";
		
		// ---------------------- End For File Caching -------------
		
		public function SinglePhotoCaching(_PathToPhoto:String, _ExpectedFileSize:Number, _StraightData:BitmapData = null, _alignment:String = SinglePhotoCaching.ALIGN_LEFT):void {
				  rawBitmapData = _StraightData;
				  pathToFile = _PathToPhoto;
				  expectedFileSize = _ExpectedFileSize;
				  alignment = _alignment;
				/*  // If it's raw bitmap data, ignore caching opeartions
				  if (rawBitmapData != null) {
					  initiateNormalLoadOperation();
				  } else {
				  // Start caching operations
					  initiateCacheOperations();
				  }*/
		}
		public function loadSinglePhoto(_justGetBitmapData:Boolean = false, _manualCacheAbsPath:String = ""):void
		{
			  justGetBitmapData = _justGetBitmapData; // DN optional Boolean to pass in if all you want is BitmapData and don't want any display objects added
			  forcedCacheLocation = _manualCacheAbsPath;
			  
			  // If it's raw bitmap data, ignore caching opeartions
			  if (rawBitmapData != null || expectedFileSize == 0) {
				  initiateNormalLoadOperation();
			  } else {
			  // Start caching operations
				  initiateCacheOperations();
			  }
		}
		public function getLoadedFileNameWithExtension():String {
			return getFileNameOnly(pathToFile);
		}
		private function initiateNormalLoadOperation():void {
		  if (rawBitmapData == null) {
			  //trace("init normal load operation");
			  photoLoader = new Loader();
			  photoLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, photoLoaded);
			  photoLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			  url = new URLRequest(pathToFile);
			  photoLoader.load(url);
		  } else {
			  //trace("Fakintines");
			  photoLoaded(new Event("Fakintines"));
		  }
		}
		
		//
		// Event from a URL Request
		public function photoLoaded(e:Event):void {
			//trace("photo loaded event??");
			var tempPhoto:BitmapData;
			if(rawBitmapData == null) {
			// Remove event listener
			   e.currentTarget.removeEventListener(Event.COMPLETE, photoLoaded);
			// Remove the IO Error listener
			   e.currentTarget.removeEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			   
			// Render the image and insert it into our array
			   rawBitmapData = Bitmap(e.target.content).bitmapData;
			   
			   //tempPhoto = Bitmap(e.target.content).bitmapData; // DN commenting this out because it doesn't do anything, Charles probably wrote this at 3am.... 
			} else {
				tempPhoto = rawBitmapData;
			}
			
			  // DN if all we want is bitmapData don't create a bitmap.. Makes sense right? 
			  if(! justGetBitmapData)
			  {
			  	Photo = new Bitmap(rawBitmapData);
			   
				// Turn smoothing on
			  	Photo.smoothing = true;
			   
			  	addChild(Photo);
				
				// Do alignment. YAY! - For left we do nothing
				if(alignment == SinglePhotoCaching.ALIGN_CENTER) {
					Photo.x = -(Photo.width / 2);
					Photo.y = -(Photo.height / 2);
				} else if(alignment == SinglePhotoCaching.ALIGN_RIGHT) {
					Photo.x = -Photo.width;
					Photo.y = -Photo.height;
				}
				
			  }

			  
			// Re-dispatch the event
			   //trace("dispatched");
			   dispatchEvent(new Event(SinglePhotoCaching.PHOTOLOADED)); 
		}
		
		public function getRawData():BitmapData {
			return rawBitmapData.clone(); //DN clone's the bitmapData so there isnt a reference to an instance of the BitmapData loaded and we can use .dispose() to clean it up later.
		}
		
		// Returns an object with properties .width and .height int values
		public function getWidthHeight():Object {
			var tmpObj:Object = new Object();
			tmpObj.width = photoLoader.width;
			tmpObj.height = photoLoader.height;
			
			return tmpObj;
		}
		
		public function getLoadedMediaPath():String {
			return url.url;
		}
		
		// Need a way to store and retrieve optional information inside the single photo
		public function getOptionalStorageRef():Object {
			return OptionalDataStorageObject;
		}
		
		public function updateOptionalStorageRef(_UpdatedObject:Object):void {
		    OptionalDataStorageObject = _UpdatedObject;	
		}
		
		// Need to handle IO Errors Gracefully
		// 11/9/2009
		private function handleIOError(e:IOErrorEvent):void {
			// Remove event listener
			   photoLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, photoLoaded);
			// Remove the IO Error listener
			   photoLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			// Trace
			  // trace("SinglePhotoCaching IO Error Loading ---" + url.url);
			// Re-dispatch the IO Error
			dispatchEvent(new IOErrorEvent(SinglePhotoCaching.IOERROR, false, true, e.text));
		}
		
		public function destroyInternals():void {
			destroyRemoteFileLoader();
			
			if( Photo != null ) {
			  this.removeChild(Photo);
			  Photo = null;
			}
			
			if(photoLoader != null) {
			photoLoader.unload();
			// Remove event listener
			   photoLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, photoLoaded);
			// Remove the IO Error listener
			   photoLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, handleIOError);
			}
			if(remoteFileLoader)
			{
				destroyRemoteFileLoader();
			}
			//BN dispose of the Bitmap Data so it doesn't have to hang around as long as we do.....
			if(rawBitmapData)
			{
				rawBitmapData.dispose();
				rawBitmapData = null;
			}
			OptionalDataStorageObject = null;
			photoLoader = null;
			url = null;
		}
		
		/*
		 * ------------------------ AIR ONLY -----------------------------------------------
		 * ------------------ Operations Added for Caching Behavior ------------------------
		 */
		
		/*
		 * @private
		 * All paths passed in will be full URLS like  http://192.168.0.10/drupal/sites/default/files/factory_Cam/images/Room1.jpg
		 * 
		 * We take just the filename off of this path and determine if their is already the same file with same size inside the
		 * AIR app-storage /imageCache folder.
		 * 
		 * If that's the case, we simply re-direct the path to the local stored copy.
		 * 
		 * If NOT, we have to do a lot of other operations explained below
		 */
		private function initiateCacheOperations():void 
		{
			var fileNameOnly:String = getFileNameOnly(pathToFile);
			var tmpFile:File;
			
			//trace("Trying to check for file: " + "app-storage:" + File.separator + "imageCache" + File.separator + fileNameOnly);
			// Check the video cache directory - THINK...cross platform you have to use backslashes here.  I get argument exception on windows using File.separator or \
			if(forcedCacheLocation == "") {
			 	tmpFile = new File("app-storage:/imageCache/" + fileNameOnly);
			} else {
				tmpFile = new File(forcedCacheLocation + fileNameOnly);
			}
			var tmpFileSize:int = 0;
			
			// Get the file size of the file if it exists
			if (tmpFile.exists) 
			{
				tmpFileSize = tmpFile.size;
			}
			
			if (tmpFile.exists && (tmpFileSize == expectedFileSize) ) 
			{
				// Same file as remote - load the local file
				pathToFile = tmpFile.url;
				
				//trace("File exists in cache loading: " + pathToFile);
				initiateNormalLoadOperation();
			} else {
				// Get on loading the remote file
				startLoadingRemoteFileForCache();
			}
		}
		
		/*
		 * @private
		 * Takes a string of a path like http://192.168.0.10/drupal/sites/default/files/factory_Cam/videos/Sequence1-TURKEYHILL-MPEG-4.mov
		 * or videos/Sequence1-TURKEYHILL-MPEG-4.mov
		 * or \\ENTERPRISE\Shared_Documents\Sequence1-TURKEYHILL-MPEG-4.mov
		 * or Sequence1-TURKEYHILL-MPEG-4.mov
		 * and returns everything after the last slash, or just everything if there is no slash
		 */
		private function getFileNameOnly(_FullPath:String):String {
			var returnString:String = "";
			
			// Find the last slash
			var lastSlashPos:int = -1;
			
			// Are they back slashes?
			lastSlashPos = _FullPath.lastIndexOf("/");

			// Ok, no dice try the other direction
			if (lastSlashPos == -1) {
				lastSlashPos = _FullPath.lastIndexOf("\\");
			}

			// If we got a match on something, shorten the string
			if (lastSlashPos != -1) {
				returnString = _FullPath.substr((lastSlashPos + 1), _FullPath.length - lastSlashPos);
			}
			
			return returnString;
		}
		
		/*
		 * @private
		 * Begins the process of loading a remote file then writing a copy of that file into the local cache
		 */
		private function startLoadingRemoteFileForCache():void {
			createRemoteFileLoader();
		}
		
		/*
		 * @private
		 * Begins the process of loading a remote file then writing a copy of that file into the local cache
		 */
		private function createRemoteFileLoader():void {
			if(remoteFileLoader == null) 
			{
				//trace("creating remote file loader");
				remoteFileLoader = new URLLoader();
				remoteFileLoader.addEventListener(Event.COMPLETE, handleRemoteLoadCoplete);
				remoteFileLoader.addEventListener(ProgressEvent.PROGRESS, handleRemoteLoadProgress);
				remoteFileLoader.addEventListener(IOErrorEvent.IO_ERROR, handleRemoteLoadError, false, 0, true);
				remoteFileLoader.dataFormat = URLLoaderDataFormat.BINARY;
				remoteFileLoader.load(new URLRequest(pathToFile));
			} else {
				destroyRemoteFileLoader(true);
			}
		}
		
		/*
		 * @private
		 * Gets rid of the remote file loader
		 */
		private function destroyRemoteFileLoader(_OptionalCallback:Boolean = false):void {
			if (remoteFileLoader != null) {
				try {
					remoteFileLoader.close();
				} catch (e:Error) {
					// No big deal, no load was in progress.  Just sink it
				}
				
				remoteFileLoader.removeEventListener(Event.COMPLETE, handleRemoteLoadCoplete);
				remoteFileLoader.removeEventListener(ProgressEvent.PROGRESS, handleRemoteLoadProgress);
				remoteFileLoader.removeEventListener(IOErrorEvent.IO_ERROR, handleRemoteLoadError);
				remoteFileLoader = null;
				
				if (_OptionalCallback == true) {
					createRemoteFileLoader();
				}
			}
		}
		
		/*
		 * @private
		 * Event for handling the end of file loading
		 */
		private function handleRemoteLoadCoplete(e:Event):void {
//trace("SinglePhotoCaching - Remote load complete");

			var justFileName:String = getFileNameOnly(pathToFile);
			var newCacheFile:File;
			if(forcedCacheLocation != "") {
				newCacheFile = new File(forcedCacheLocation + justFileName);
			} else {
				newCacheFile = new File("app-storage:/imageCache/" + justFileName);
			}
			
			try {
				var stream:FileStream = new FileStream();
				stream.open(newCacheFile, FileMode.WRITE);
				stream.writeBytes(remoteFileLoader.data);
				stream.close();
				stream = null;
			} catch (e:Error) {
				// Dont do anything.  Its possible the cached file was open and we tried to over-wirte it.
				// Thats fine because we'll just allow loading of the previous cached file
			}
			
			destroyRemoteFileLoader();
			
			// Cache file has been created.
			// Have the video player load the cache file
			pathToFile = newCacheFile.url;
			
			// Initiate normal load operations
			//trace("calling noremal load operation");
			initiateNormalLoadOperation();
		}
		
		/*
		 * @private
		 * Event for handling file load progress
		 */
		private function handleRemoteLoadProgress(e:ProgressEvent):void {
			dispatchEvent(new ProgressEvent(SinglePhotoCaching.LOADPROGRESS, false, true, e.bytesLoaded, e.bytesTotal));
		}
		
		/*
		 * @private
		 * Event for handling file load error
		 */
		private function handleRemoteLoadError(e:IOErrorEvent):void {
			destroyRemoteFileLoader();
			// Re-dispatch the IO Error
			dispatchEvent(new IOErrorEvent(SinglePhotoCaching.IOERROR, false, true, e.text));
		}
		
	}	
}