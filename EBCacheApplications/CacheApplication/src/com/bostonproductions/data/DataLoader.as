﻿package com.bostonproductions.data{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.FileFilter;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
    
	// AIR STUFF
	//import flash.filesystem.*;
	
	import com.bostonproductions.events.TextDataEvent;
/* DataLoader - Handles the parsing of xml so that it can be input into a flash project easily
*/

   public class DataLoader extends EventDispatcher{
  
  private var XMLPath:String = "";
  private var XMLOrText:String;
  private var XMLIndex:int;
  private var RawXMLData:XML;
  private var ThePath:String;
  private var removeNamespace:Boolean = false;
  private var continualRetry:Boolean = false;
  
  // Create a data loader
  private var AbstractDataLoader:URLLoader;		 
  public static var DATAREADY:String = "Data Ready";
  
  // Class constructor
  public function DataLoader(_PathToData:String, _XMLOrText:String, _XMLIndex:int,_Mode:String = "actionscript", _RemoveNamespace:Boolean = false, _ContinualRetry:Boolean = false):void
  {

	  ThePath = _PathToData;
	  removeNamespace = _RemoveNamespace;
	  continualRetry = _ContinualRetry;
		  
	  // Swaps any \ slashes with / slashes
	  //_PathToData = _PathToData.replace(/\\/g, "/");
		 
	  // Set the type of file
	  XMLOrText = _XMLOrText;
	  // Set the XML Index
	  XMLIndex = _XMLIndex;
	  
	  preformLoadActions();
  }
  
  private function preformLoadActions():void {
	  	  // Remove any events
	  if (AbstractDataLoader != null) {
		AbstractDataLoader.removeEventListener(Event.COMPLETE, HandleData);
		AbstractDataLoader.removeEventListener(IOErrorEvent.IO_ERROR, ReportError);
		AbstractDataLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, ReportSecurityError);
		AbstractDataLoader = null;
	  }
	  
	  AbstractDataLoader = new URLLoader();
	  // Add Events
	  AbstractDataLoader.addEventListener(Event.COMPLETE, HandleData, false, 0, true);
	  AbstractDataLoader.addEventListener(IOErrorEvent.IO_ERROR, ReportError, false, 0, true);
	  AbstractDataLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, ReportSecurityError, false, 0, true);
	  
	  // Load the xml/text file
	  AbstractDataLoader.load(new URLRequest(ThePath));
  }
  
  private function HandleData(e:Event):void {
	  e.currentTarget.removeEventListener(Event.COMPLETE, HandleData);
	  if(XMLOrText == "xml") {
	    XML.ignoreWhitespace = true;
		
		// If remove namespace is set to true, we need to pull it out before we cast to xml
		if (removeNamespace == true) {
			var xmlnsPattern:RegExp = new RegExp("xmlns[^\"]*\"[^\"]*\"", "gi");
			e.target.data = e.target.data.replace(xmlnsPattern, "");
		}
		
	    RawXMLData = new XML(e.target.data);

		/*
		 for each(var placemarkData:XML in RawXMLData.children() ) {
			 trace("Inside placemark name:" + placemarkData.name() + " local name:" + placemarkData.localName());
		 }
		*/
		 
		dispatchEvent( new TextDataEvent(DataLoader.DATAREADY, false, false, RawXMLData) );
	  } else {
	    var notXML:String = e.target.data;

		dispatchEvent( new TextDataEvent(DataLoader.DATAREADY, false, false, null, notXML) );
	  }
	  
  }
  
  // If we get an IO error, still throw DATAREADY but send null instead of xml
  private function ReportError(e:IOErrorEvent):void {
	  trace("DataLoader IO Error Trying to load: " + ThePath);
	  if (continualRetry == true) {
		  preformLoadActions();
	  } else {
		  var evt:TextDataEvent = new TextDataEvent(DataLoader.DATAREADY, false, false, null);
		  dispatchEvent(evt);
	  }
  }
  
  private function ReportSecurityError(e:SecurityError):void {
	  trace("DataLoader Security Error");
	  var evt:TextDataEvent = new TextDataEvent(DataLoader.DATAREADY, false, false, null);
	  dispatchEvent(evt);
  }
  
  public function destroyInternals():void {
	  AbstractDataLoader.removeEventListener(Event.COMPLETE, HandleData);
	  AbstractDataLoader.removeEventListener(IOErrorEvent.IO_ERROR, ReportError);
	  AbstractDataLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, ReportSecurityError);
	  RawXMLData = null;
  }
  
 }
}