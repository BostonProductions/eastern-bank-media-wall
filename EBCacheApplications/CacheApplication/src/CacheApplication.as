package
{
	import caurina.transitions.Tweener;
	
	import com.bostonproductions.data.commandAPI;
	import com.technogumbo.data.XMLLoader;
	import com.technogumbo.net.StringSocketServer;
	import com.technogumbo.utils.NumberUtils;
	import com.transcendingdigital.data.CMSCache;
	import com.transcendingdigital.data.GlobalDataStorage;
	import com.transcendingdigital.data.vo.xmlCacheVO;
	import com.transcendingdigital.events.UIMessageEvent;
	import com.transcendingdigital.ui.iMainSectionInterface;
	import com.transcendingdigital.ui.startupManagerUI;
	
	import flash.display.Sprite;
	import flash.display.StageDisplayState;
	import flash.display.StageScaleMode;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.ui.Mouse;
	import flash.utils.Timer;
	
	/***
	 * This connects with a Drupal 7 CMS via Services 3 REST
	 * server. We only use the REST server for login and logout
	 * when special permissions are needed to acess CMS files.
	 * 
	 * Main caching operations are handled by polling of small xml timestamp
	 * files.  We compare their unix timestamps to our cached versions.
	 * If they change, we download an accompanying full data file
	 * and re-populate our own cache.
	 * 
	 * Also includes a tcp socket server for interfacing
	 * with other local filesystem applications.
	 * 
	 * Was implemented as TCP for connections to other applications
	 * for maximum flexibility..may communicate with more than
	 * just apps on this machine..just know that on network shares the
	 * AIR FILE class in NOT RELIABLE ON NETWORK SHARES. I AM WARNING YOU BIG TIME. 
	 * DO NOT RELY ON AIR the AIR FILE CLASS TO BE ABLE TO WRITE TO NETWORK
	 * FILE SHARES IN HEAVY, MODERATE, OR LIGHT LOAD, IT WILL FAIL.
	 * 
	 * It can also cause blocking as a network share re-connects freezing
	 * or causing this application to stop responding, so do not plan on
	 * caching files to a network share using this application
	 */
	[SWF(width="640", height="480", backgroundColor="#FFFFFF", frameRate="24")]
	public class CacheApplication extends Sprite
	{
		private var _configLoader:XMLLoader;
		private var _startupView:startupManagerUI;
		private var _interactiveTimeout:Timer;
		private var _socketServer:StringSocketServer;
		private var _commandParser:commandAPI;
		/**
		 * After initial start up, this is used to silently re-pull 
		 * CMS content after a configuration specified time.
		 */
		private var _backgroundCache:CMSCache;
		
		public function CacheApplication()
		{
			GlobalDataStorage.uiSize = new Point(640,480);
			
			// Set Fullscreen - Hide Mouse
			// Go fullscreen
			//stage.scaleMode = StageScaleMode.EXACT_FIT;
			//stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
			// Hide Mouse
			//Mouse.hide();
			
			createConfigLoader();
		}
		
	private function createConfigLoader():void {
		if(_configLoader == null) {
			_configLoader = new XMLLoader("xml/applicationConfiguration.xml");
			_configLoader.addEventListener(XMLLoader.DATALOADED, handleConfigLoaded, false, 0, true);
			_configLoader.addEventListener(IOErrorEvent.IO_ERROR, handleConfigIOError, false, 0, true);
			_configLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, handleConfigSecurityError, false, 0, true);
			_configLoader.LoadXMLData();
		} else {
			destroyConfigLoader(true);
		}
	}
	private function destroyConfigLoader(_optionalCallback:Boolean = false):void {
		if(_configLoader != null) {
			_configLoader.removeEventListener(XMLLoader.DATALOADED, handleConfigLoaded);
			_configLoader.removeEventListener(IOErrorEvent.IO_ERROR, handleConfigIOError);
			_configLoader.removeEventListener(SecurityErrorEvent.SECURITY_ERROR, handleConfigSecurityError);
			_configLoader.destroyInternals();
			_configLoader = null;
			if(_optionalCallback == true) {
				createConfigLoader();
			}
		}
	}
	private function handleConfigIOError(e:IOErrorEvent):void {
		trace("main - config load IO Error: " + e);
	}
	private function handleConfigSecurityError(e:SecurityErrorEvent):void {
		trace("main - config load Security Error: " + e);
	}
	private function handleConfigLoaded(e:Event):void {
		if(_configLoader.getLoadedXML() != null) {
			GlobalDataStorage.configXML = _configLoader.getLoadedXML();
		}
		destroyConfigLoader();
		
		// Parse the items to check into a global vector
		GlobalDataStorage.guideXMLFiles = new Vector.<xmlCacheVO>();
		for each( var targetNode:XML in GlobalDataStorage.configXML.config.CacheGuideFiles.CacheEntry ) {
			var newVO:xmlCacheVO = new xmlCacheVO();
			newVO.timeStampPath = targetNode.TimestampFile.text()[0];
			newVO.fullPath = targetNode.FullDataFile.text()[0];
			newVO.broadcastContstant = targetNode.BroadcastConstant.text()[0];
			// OPTIONAL ELEMENTS FOR USER AUTHENTICATION TO ACCESS PRIVATE FILES
			// SOME DATA MAY HAVE IT, SOME MAY NOT
			if( targetNode.CMSBasePath != undefined ) {
				newVO.drupalCMSBasePath = targetNode.CMSBasePath.text()[0];
			}
			if( targetNode.CMSDrupalEndpoint != undefined ) {
				newVO.drupalCMSEndpointName = targetNode.CMSDrupalEndpoint.text()[0];
			}
			if( targetNode.CMSUserDrupalName != undefined ) {
				newVO.drupalCMSUserName = targetNode.CMSUserDrupalName.text()[0];
			}
			if( targetNode.CMSUserDrupalPassword != undefined ) {
				newVO.drupalCMSPassword = targetNode.CMSUserDrupalPassword.text()[0];
			}
			
			GlobalDataStorage.guideXMLFiles.push( newVO );
		}
		
		// Set the cache location
		GlobalDataStorage.cacheDirectoryPrefix = GlobalDataStorage.configXML.config.LocalCacheDirectory.text()[0];
		
		// Initialize the socket server
		createSocketServer();
		
		createSection(GlobalDataStorage.SECTION_INITIAL_LOAD);
		
	}
	private function createSection(_GlobalSectionConst:String):void {
		var createdSection:iMainSectionInterface = null;
		
		switch(_GlobalSectionConst) {
			case GlobalDataStorage.SECTION_INITIAL_LOAD:
				_startupView = new startupManagerUI();
				createdSection = _startupView;
				break;
		}
		
		// Add events/init
		if(createdSection != null) {
			addChild(Sprite(createdSection));
			
			Sprite(createdSection).addEventListener(GlobalDataStorage.REQUEST_SECTION, handleSectionRequest, false, 0, true);
			Sprite(createdSection).addEventListener(GlobalDataStorage.REQUEST_TRANSITION_OFF, handleSelectionTransitionOffRequest, false, 0, true);
			Sprite(createdSection).addEventListener(GlobalDataStorage.INITIAL_CACHE_COMPLETE, handleInitialCacheComplete, false, 0, true);
			Sprite(createdSection).addEventListener(startupManagerUI.NEW_CACHE_DATA, handleNewCacheData, false, 0, true);
			createdSection.init();
		}
	}
	private function handleSectionRequest(e:UIMessageEvent):void {
		createSection( String(e.data) );
	}
	private function handleSelectionTransitionOffRequest(e:Event):void {
		// Ended up doing individual specialized transitions per section so we
		// can immediately destroy
		destroySection( iMainSectionInterface( e.target ) );
	}
	private function handleInitialCacheComplete(e:Event):void {
		
		// Notify other applications
		if(_socketServer != null) {
			_socketServer.setInitializationTrue();
		}
	}
	private function destroySection(targetSection:iMainSectionInterface):void {
		
		Tweener.removeTweens(targetSection);
		if(contains( Sprite(targetSection) )) {
			removeChild( Sprite(targetSection) );
		}
		// Remove any listeners that could be active
		Sprite(targetSection).removeEventListener(GlobalDataStorage.REQUEST_TRANSITION_OFF, handleSelectionTransitionOffRequest);
		Sprite(targetSection).removeEventListener(GlobalDataStorage.REQUEST_SECTION, handleSectionRequest);
		Sprite(targetSection).removeEventListener(GlobalDataStorage.INITIAL_CACHE_COMPLETE, handleInitialCacheComplete);
		Sprite(targetSection).removeEventListener(startupManagerUI.NEW_CACHE_DATA, handleNewCacheData);
		
		// Destroy it
		targetSection.destroyInternals();
		
		// We have to set the named references null
		 if(targetSection == _startupView) {
			_startupView = null;
		}
	}
	
	/**
	 * Each email station in this experience runs its own socket server.
	 * The photo stations all connect to each of the email stations
	 * in order to send information about new photos as they are
	 * created
	 */
	private function createSocketServer():void {
		if(_socketServer == null) {
			_socketServer = new StringSocketServer(int(GlobalDataStorage.configXML.config.TCPServerPort.text()[0]), "|");
			_socketServer.addEventListener(StringSocketServer.PARSEDDATA, handleSocketData);
			_socketServer.addEventListener(StringSocketServer.ERROR, handleSocketError);
		} else {
			destroySocketServer(true);
		}
	}
	private function destroySocketServer(_OptionalCallback:Boolean = false):void {
		if(_socketServer != null) {
			_socketServer.removeEventListener(StringSocketServer.PARSEDDATA, handleSocketData);
			_socketServer.removeEventListener(StringSocketServer.ERROR, handleSocketError);
			_socketServer.destroyInternals();
			_socketServer = null;
			if(_OptionalCallback == true) createSocketServer();
		}
	}
	private function handleSocketData(e:DataEvent):void {
		trace("MAIN - handleSocketData - " + e.data);
		createCommandParser();
		_commandParser.parseCommand( e.data );
	}
	private function handleSocketError(e:DataEvent):void {
		trace("MAIN - handleSocketError - " + e.data);
	}
	
    private function createCommandParser():void {
		if(_commandParser == null) {
			_commandParser = new commandAPI();
			_commandParser.addEventListener(commandAPI.DATA_READY, handleCommandParserDataReady, false, 0, true);
		}
	}
	
	private function handleCommandParserDataReady(e:Event):void {
		trace("MAIN - handleCOmmandParserDataReady - " + _commandParser.getTransmittableFinalData() );
		_socketServer.sendAllClientsString( _commandParser.getTransmittableFinalData() );
	}
	
	private function handleNewCacheData(e:DataEvent):void {
		trace('MAIN - handleNewCacheData - KEY: ' + e.data);
		// Somewhat counter intuitive but send it back into the command parser for 
		// prep then broadcast back out to the clients
		createCommandParser();
		_commandParser.parseCommand("EVENT_" + e.data + "_ALL", false);
	}
	
  }
}