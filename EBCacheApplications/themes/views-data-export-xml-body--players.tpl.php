<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $rows: An array of row items. Each row is an array of content
 *   keyed by field ID.
 * - $header: an array of headers(labels) for fields.
 * - $themed_rows: a array of rows with themed fields.
 */
$group_values = list_allowed_values(field_info_field('field_group')); 
if (!function_exists ('_parse_item')) {
  function _parse_item($content, $main, $ident='') {
    if (count($content)==0) {
      $content = array(0 => array('raw' => array('uri'=>'')));
    } 
    foreach ($content as $c_item) {
      _one_item($c_item, $main, $ident);
    }
  }
  function _one_item($c_item, $main, $ident='') {
    if (isset($c_item['raw'])) {
      $size = trim($c_item['raw']['uri'])==''?'':filesize($c_item['raw']['uri']);
      $c_item['raw']['uri'] = trim($c_item['raw']['uri'])==''?'':file_create_url(trim($c_item['raw']['uri']));
      if ($main == 'video') {
        echo "    {$ident}<{$main}>\n";
        echo "    {$ident}{$ident}<{$main}_file>".$c_item['raw']['uri']."</{$main}_file>\n";
        echo "    {$ident}{$ident}<{$main}_size>".$size."</{$main}_size>\n";
        echo "    {$ident}</{$main}>\n";
      } else {
        echo "    {$ident}<{$main}>".$c_item['raw']['uri']."</{$main}>\n";
      }
    }
  }  
  function empty_other() {
    echo "      <other_team>\n";
    echo "        <team_name></team_name>\n";
    echo "        <join_date></join_date>\n";
    echo "        <end_date></end_date>\n";
    //echo "        <team_logo></team_logo>\n";
    //echo "        <team_helmet></team_helmet>\n";
    //echo "        <team_group></team_group>\n";
    echo "      </other_team>\n";                    
  }
} 
if ($themed_rows) {
print '<?xml version="1.0" encoding="UTF-8" ?>';
?>
<players>
<?php foreach ($themed_rows as $count => $row): ?>
  <player>
<?php foreach ($row as $field => $content) {
      $content = trim($content);
      switch ($field) {
        case 'field_date_of_birth':
          echo "    <dob>";
          if (isset($rows[$count]->field_field_date_of_birth[0]['raw']) && 
                isset($rows[$count]->field_field_date_of_birth[0]['raw']['value'])) {
            $date = explode(' ',$rows[$count]->field_field_date_of_birth[0]['raw']['value']);
            echo $date[0];
          }
          echo "</dob>\n";
        break;
        case 'field_feet':
          echo "    <height>\n";
          echo "      <{$xml_tag[$field]}>{$content}</{$xml_tag[$field]}>\n";
        break;
        case 'field_inche':
          echo "      <{$xml_tag[$field]}>{$content}</{$xml_tag[$field]}>\n";
          echo "    </height>\n";
        break;
        case 'field_photo_1':
        break;
        case 'field_photo':
          echo "    <photos>\n";
            if(is_array($rows[$count]->field_field_photo) && count($rows[$count]->field_field_photo)>0) {
            foreach ($rows[$count]->field_field_photo as $key=>$c_item) {
              echo "      <photo>\n";
              _one_item($c_item, 'original', '    ');
              $thumb = $rows[$count]->field_field_photo_1[$key]['rendered'];
              $size = trim($c_item['raw']['uri'])==''?'':filesize($c_item['raw']['uri']);
              echo "        <file_size>{$size}</file_size>\n";
              $tsize = trim($thumb['#item']['uri'])==''?'':filesize(image_style_path($thumb['#image_style'], $thumb['#item']['uri']));
              $thumb = image_style_url($thumb['#image_style'], $thumb['#item']['uri']);
              echo "        <thumb>{$thumb}</thumb>\n";
              echo "        <thumb_size>{$tsize}</thumb_size>\n";
              echo "      </photo>\n";
            }        
          } else {
              echo "      <photo>\n";
              echo "        <original></original>\n";
              echo "        <file_size></file_size>\n";
              echo "        <thumb></thumb>\n";
              echo "        <thumb_size></thumb_size>\n";
              echo "      </photo>\n";
          }
          echo "    </photos>\n";
          break;
        case 'field_video':
          echo "    <videos>\n";
          _parse_item($rows[$count]->field_field_video, 'video', '  ');
          echo "    </videos>\n";
        break;
        case 'field_team_name':
          echo "    <primary_team>\n";
          echo "      <team_name>{$content}</team_name>\n";
        break;
        case 'field_time_frame':
          $date = $date2 = '';
          if (isset($rows[$count]->field_field_time_frame[0]['raw']) && 
                isset($rows[$count]->field_field_time_frame[0]['raw']['value'])) {
            $date = explode(' ',$rows[$count]->field_field_time_frame[0]['raw']['value']);
            $date = $date[0];
          }
          if (isset($rows[$count]->field_field_time_frame[0]['raw']) && 
                isset($rows[$count]->field_field_time_frame[0]['raw']['value2'])) {
            $date2 = explode(' ',$rows[$count]->field_field_time_frame[0]['raw']['value2']);
            $date2 = $date2[0];
          }
          echo "      <join_date>{$date}</join_date>\n";
		  if ($date2 == $date){
			  $date2 = '';}
          echo "      <end_date>{$date2}</end_date>\n";
        break;
        case 'field_position':
          echo "      <position>{$content}</position>\n";
          //$team = isset($rows[$count]->field_field_team_name[0]['raw']['entity'])?$rows[$count]->field_field_team_name[0]['raw']['entity']:'';
          //$teamlogo = isset($team->field_team_logo['und'][0]['uri'])?file_create_url($team->field_team_logo['und'][0]['uri']):'';
         // echo "      <team_logo>{$teamlogo}</team_logo>\n";
          //$teamhelmet = isset($team->field_team_helmet['und'][0]['uri'])?file_create_url($team->field_team_helmet['und'][0]['uri']):'';
          //echo "      <team_helmet>{$teamhelmet}</team_helmet>\n";
          //$group = isset($team->field_group['und'][0]['value'])?trim($group_values[$team->field_group['und'][0]['value']]):'';
         // echo "      <team_group>{$group}</team_group>\n";
          echo "    </primary_team>\n";
        break;
        case 'field_other_teams':
          echo "    <other_teams>\n";
            if (is_array($rows[$count]->field_field_other_teams) and count($rows[$count]->field_field_other_teams)>0) {
              $exit = false;
              foreach ($rows[$count]->field_field_other_teams as $item) {
                if (isset($item['rendered']['entity']['field_collection_item']) && 
                     is_array($item['rendered']['entity']['field_collection_item']) &&
                     count($item['rendered']['entity']['field_collection_item'])>0) {
                  foreach ($item['rendered']['entity']['field_collection_item'] as $item2) {
                    if (isset($item2['field_team_names'])) {
                      $team = $item2['field_team_names']['#items'][0]['entity'];
                      $date = explode(' ',$item2['field_time_frames']['#items'][0]['value']);
                      $date = $date[0];
                      $date2 = explode(' ',$item2['field_time_frames']['#items'][0]['value2']);
                      $date2 = $date2[0];
                      echo "      <other_team>\n";
                      echo "        <team_name>{$team->title}</team_name>\n";
                      echo "        <join_date>{$date}</join_date>\n";
                      echo "        <end_date>{$date2}</end_date>\n";
                      $teamlogo = isset($team->field_team_logo['und'][0]['uri'])?file_create_url($team->field_team_logo['und'][0]['uri']):'';
                     // echo "        <team_logo>{$teamlogo}</team_logo>\n";
                      //$teamhelmet = isset($team->field_team_helmet['und'][0]['uri'])?file_create_url($team->field_team_helmet['und'][0]['uri']):'';
                      //echo "        <team_helmet>{$teamhelmet}</team_helmet>\n";
                      //$group = isset($team->field_group['und'][0]['value'])?trim($group_values[$team->field_group['und'][0]['value']]):'';
                      //echo "        <team_group>{$group}</team_group>\n";
                      echo "      </other_team>\n";                    
                    } else {
                      empty_other();
                      $exit = true;
                      break;
                    }
                  }
                  if ($exit) {
                    break;
                  }
                } else {
                  empty_other();
                  break;
                }
              }
            } else {
              empty_other();
            }  
          echo "    </other_teams>\n";
        break;
        default:
          echo "    <{$xml_tag[$field]}>{$content}</{$xml_tag[$field]}>\n";
      }
    }
    ?>
  </player>
<?php endforeach; ?>
</players>
<?php
}
?>