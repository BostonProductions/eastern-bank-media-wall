<?php
/**
 * @file views-view-table.tpl.php
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $rows: An array of row items. Each row is an array of content
 *   keyed by field ID.
 * - $header: an array of headers(labels) for fields.
 * - $themed_rows: a array of rows with themed fields.
 */
if (!function_exists ('_parse_item')) {
  function _parse_item($content, $main) {
    if (count($content)==0) {
      $content = array(0 => array('raw' => array('uri'=>'')));
    } 
    foreach ($content as $c_item) {
      if (isset($c_item['raw'])) {
        $size = trim($c_item['raw']['uri'])==''?'':filesize($c_item['raw']['uri']);
        $c_item['raw']['uri'] = trim($c_item['raw']['uri'])==''?'':file_create_url(trim($c_item['raw']['uri']));
        echo "    <{$main}>".$c_item['raw']['uri']."</{$main}>\n";
        echo "    <{$main}_size>".$size."</{$main}_size>";
      }
    }
  }
} 
if ($themed_rows) {
print '<?xml version="1.0" encoding="UTF-8" ?>';
?>
<teams>
<?php foreach ($themed_rows as $count => $row): ?>
  <team>
<?php foreach ($row as $field => $content): ?>
<?php
      $content = trim($content);
      switch ($field) {
        case 'field_team_logo':
        case 'field_team_helmet':
          $ff = 'field_'.$field;
          _parse_item($rows[$count]->{$ff}, $xml_tag[$field]);
          break;
        default:
          echo "    <{$xml_tag[$field]}>{$content}</{$xml_tag[$field]}>";
      }
    ?>  
<?php endforeach; ?>
  </team>
<?php endforeach; ?>
</teams>
<?php
}
?>