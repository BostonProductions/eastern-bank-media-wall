﻿package 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	public class MortgageCalculator extends MovieClip
	{
		private var intRepayments:int = 0;
		private var sliderArray:Array = new Array();
		private var keypadArray:Array = new Array();
		private var owner:Document;
		private var isSliding:Boolean = false;
		private var tempDecimal:int = 0;
		private var tempComma:int = 0;
		
		public function MortgageCalculator(myOwner:Document)
		{
			//create assets and add event listeners. Also creates the 3 keypads.
			owner = myOwner;
			weekly.bTitle.text = "Weekly";
			biweekly.bTitle.text = "Biweekly";
			monthly.bTitle.text = "Monthly";
			weekly.mouseChildren = false;
			biweekly.mouseChildren = false;
			monthly.mouseChildren = false;
			loanX.mouseChildren = false;
			princX.mouseChildren = false;
			intX.mouseChildren = false;
			
			weekly.addEventListener(MouseEvent.MOUSE_UP,btnClick);
			biweekly.addEventListener(MouseEvent.MOUSE_UP,btnClick);
			monthly.addEventListener(MouseEvent.MOUSE_UP,btnClick);
			princBtn.addEventListener(MouseEvent.MOUSE_UP,enableKeys);
			interestBtn.addEventListener(MouseEvent.MOUSE_UP,enableKeys);
			yearBtn.addEventListener(MouseEvent.MOUSE_UP,enableKeys);
			
			this.addEventListener(Event.ENTER_FRAME, init);
			
			createKeypad(125, 50, false);
			createKeypad(108, 135, true);
			createKeypad(125, 220, false);
		}
		
		private function init(event:Event):void 
		{
			//show final tally if all fields filled out
			calculateRepayment();
			
			//show X buttons if there is a value input into each area
			if(intPrincipal.text != "$")
			{
				princX.visible = true;
				princX.addEventListener(MouseEvent.MOUSE_UP,btnClick);
			}
			else
			{
				princX.visible = false;
				princX.removeEventListener(MouseEvent.MOUSE_UP,btnClick);
			}
			if(floInterest.text != "%")
			{
				intX.visible = true;
				intX.addEventListener(MouseEvent.MOUSE_UP,btnClick);
			}
			else
			{
				intX.visible = false;
				intX.removeEventListener(MouseEvent.MOUSE_UP,btnClick);
			}
			if(intTerm.text != "")
			{
				loanX.visible = true;
				loanX.addEventListener(MouseEvent.MOUSE_UP,btnClick);
			}
			else
			{
				loanX.visible = false;
				loanX.removeEventListener(MouseEvent.MOUSE_UP,btnClick);
			}
		}
		
		private function calculateRepayment() 
		{
			//Show validation text and add instructions
			if (intPrincipal.text == "$" && floInterest.text == "%" && intTerm.text <= "0") {
				validationMessage.text = "Click on a box to input a value";
			}
			else if (intPrincipal.text == "$") {
				validationMessage.text = "Principal must be greater than 0.";
			}
			else if (floInterest.text == "%") {
				validationMessage.text = "Annual interest must be between .125 and 25%.";
			}
			else if (intTerm.text <= "0") {
				validationMessage.text = "Loan term must be greater than 0.";
			}
			else if (intRepayments <= 0) {
				validationMessage.text = "Please select a repayment period.";
			}
			else {
				validationMessage.text = "";
				calculate();
			}
		}
		
		private function createKeypad(xPos:Number, yPos:Number, fullBtns:Boolean) 
		{
			//Create the keypads. give them locations and add the listeners for each number
			var keypad:Keypad = new Keypad();
			keypad.x = xPos;
			keypad.y = yPos;
			addChild(keypad);
			keypadArray.push(keypad);
			keypad.visible = false;
			
			//hide the keypads initially. Show . button if needed
			if(fullBtns == true)
			{
				keypad.btnDot.visible = true;
			}
			if(fullBtns == false)
			{
				keypad.btnDot.visible = false;
			}
			
			for (var i=0; i<keypadArray.length; i++)
  			{
   				keypadArray[i].addEventListener(MouseEvent.MOUSE_UP,btnClick);
  			}
		}
		
		private function btnClick(e:MouseEvent) 
		{
			var tempValue:Number;
			var blank:String = "";
						
			//if an X button is clicked, reset textfield
			if(e.target.name == "princX")
			{
				intPrincipal.text = "$";
				totRepayment.text = "";
				keypadArray[1].visible = false;
				keypadArray[2].visible = false;
			}
			if(e.target.name == "intX")
			{
				floInterest.text = "%";
				tempDecimal = 0;
				totRepayment.text = "";
				keypadArray[0].visible = false;
				keypadArray[2].visible = false;
			}
			if(e.target.name == "loanX")
			{
				intTerm.text = "";
				totRepayment.text = "";
				keypadArray[0].visible = false;
				keypadArray[1].visible = false;
			}
			
			//change the payment plan. Divide the total by number of weeks			
			if(e.target.name == "weekly"){
				intRepayments = 52;
				weekly.innerBox.gotoAndStop(2);
				biweekly.innerBox.gotoAndStop(1);
				monthly.innerBox.gotoAndStop(1);
				keypadArray[0].visible = false;
				keypadArray[1].visible = false;
				keypadArray[2].visible = false;
			}
			if(e.target.name == "biweekly"){
				intRepayments = 26;
				weekly.innerBox.gotoAndStop(1);
				biweekly.innerBox.gotoAndStop(2);
				monthly.innerBox.gotoAndStop(1);
				keypadArray[0].visible = false;
				keypadArray[1].visible = false;
				keypadArray[2].visible = false;
			}
			if(e.target.name == "monthly"){
				intRepayments = 12;
				weekly.innerBox.gotoAndStop(1);
				biweekly.innerBox.gotoAndStop(1);
				monthly.innerBox.gotoAndStop(2);
				keypadArray[0].visible = false;
				keypadArray[1].visible = false;
				keypadArray[2].visible = false;
			}
			
			if(keypadArray[0].visible == true)
			{
				//if a number is clicked add it to string. Apply the commaCoder to final number
				if(intPrincipal.text.length <13)
				{
					if(e.target.name == "btn1")
					{
						intPrincipal.appendText("1");
						intPrincipal.replaceText(0,1,blank);
						tempValue = Number(intPrincipal.text.split(",").join(""));
						intPrincipal.text = "$"+commaCoder(tempValue);						
					}
					if(e.target.name == "btn2")
					{
						intPrincipal.appendText("2");
						intPrincipal.replaceText(0,1,blank);
						tempValue = Number(intPrincipal.text.split(",").join(""));
						intPrincipal.text = "$"+commaCoder(tempValue);
					}
					if(e.target.name == "btn3")
					{
						intPrincipal.appendText("3");
						intPrincipal.replaceText(0,1,blank);
						tempValue = Number(intPrincipal.text.split(",").join(""));
						intPrincipal.text = "$"+commaCoder(tempValue);
					}
					if(e.target.name == "btn4")
					{
						intPrincipal.appendText("4");
						intPrincipal.replaceText(0,1,blank);
						tempValue = Number(intPrincipal.text.split(",").join(""));
						intPrincipal.text = "$"+commaCoder(tempValue);
					}
					if(e.target.name == "btn5")
					{
						intPrincipal.appendText("5");
						intPrincipal.replaceText(0,1,blank);
						tempValue = Number(intPrincipal.text.split(",").join(""));
						intPrincipal.text = "$"+commaCoder(tempValue);
					}
					if(e.target.name == "btn6")
					{
						intPrincipal.appendText("6");
						intPrincipal.replaceText(0,1,blank);
						tempValue = Number(intPrincipal.text.split(",").join(""));
						intPrincipal.text = "$"+commaCoder(tempValue);
					}
					if(e.target.name == "btn7")
					{
						intPrincipal.appendText("7");
						intPrincipal.replaceText(0,1,blank);
						tempValue = Number(intPrincipal.text.split(",").join(""));
						intPrincipal.text = "$"+commaCoder(tempValue);
					}
					if(e.target.name == "btn8")
					{
						intPrincipal.appendText("8");
						intPrincipal.replaceText(0,1,blank);
						tempValue = Number(intPrincipal.text.split(",").join(""));
						intPrincipal.text = "$"+commaCoder(tempValue);
					}
					if(e.target.name == "btn9")
					{
						intPrincipal.appendText("9");
						intPrincipal.replaceText(0,1,blank);
						tempValue = Number(intPrincipal.text.split(",").join(""));
						intPrincipal.text = "$"+commaCoder(tempValue);
					}
					if(e.target.name == "btn0")
					{
						intPrincipal.appendText("0");
						intPrincipal.replaceText(0,1,blank);
						tempValue = Number(intPrincipal.text.split(",").join(""));
						intPrincipal.text = "$"+commaCoder(tempValue);
					}
				}
			}
			
			else if(keypadArray[1].visible == true)
			{
				//when a number is clicked, add it before the %
				if(floInterest.text.length <7)
				{
					if(e.target.name == "btn1")
					{
						floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,"1");
					}
					if(e.target.name == "btn2")
					{
						floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,"2");
					}
					if(e.target.name == "btn3")
					{
						floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,"3");
					}
					if(e.target.name == "btn4")
					{
						floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,"4");
					}
					if(e.target.name == "btn5")
					{
						floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,"5");
					}
					if(e.target.name == "btn6")
					{
						floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,"6");
					}
					if(e.target.name == "btn7")
					{
						floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,"7");
					}
					if(e.target.name == "btn8")
					{
						floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,"8");
					}
					if(e.target.name == "btn9")
					{
						floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,"9");
					}
					if(e.target.name == "btn0")
					{
						floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,"0");
					}
					if(e.target.name == "btnDot")
					{
						//if there is no . add one if there isn't one already made.
						if(floInterest.text.length == 1)
						{
							floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,".");
							tempDecimal++
						}
						else if(tempDecimal < 1)
						{
							floInterest.replaceText(floInterest.text.length-1,floInterest.text.length-1,".");
							tempDecimal++
						}
					}
				}
			}
		
			else if(keypadArray[2].visible == true)
			{
				//When a number is clicked for the year, add to the intTerm string
				if(intTerm.text.length <2)
				{
					if(e.target.name == "btn1")
					{
						intTerm.appendText("1");
					}
					if(e.target.name == "btn2")
					{
						intTerm.appendText("2");
					}
					if(e.target.name == "btn3")
					{
						intTerm.appendText("3");
					}
					if(e.target.name == "btn4")
					{
						intTerm.appendText("4");
					}
					if(e.target.name == "btn5")
					{
						intTerm.appendText("5");
					}
					if(e.target.name == "btn6")
					{
						intTerm.appendText("6");
					}
					if(e.target.name == "btn7")
					{
						intTerm.appendText("7");
					}
					if(e.target.name == "btn8")
					{
						intTerm.appendText("8");
					}
					if(e.target.name == "btn9")
					{
						intTerm.appendText("9");
					}
					if(e.target.name == "btn0")
					{
						intTerm.appendText("0");
					}
				}
			}
		}
		
		private function enableKeys(e:MouseEvent) 
		{
			//When a section is clicked, show related keypad and hide the others
			if(e.target.name == "princBtn")
			{
				keypadArray[0].visible = true;
				keypadArray[1].visible = false;
				keypadArray[2].visible = false;
			}
			if(e.target.name == "interestBtn")
			{
				keypadArray[0].visible = false;
				keypadArray[1].visible = true;
				keypadArray[2].visible = false;
			}
			if(e.target.name == "yearBtn")
			{
				keypadArray[0].visible = false;
				keypadArray[1].visible = false;
				keypadArray[2].visible = true;
			}
		}

		private function calculate() 
		{
			//Remove any special characters
			var tempPrincipal:String = intPrincipal.text.split(",").join("");
			var fixedPrincipal:String = tempPrincipal.split("$").join("");
			var fixedInterest:String = floInterest.text.split("%").join("");
			
			//calculate interest rate applicable to period
			var periodicInterestRate = Number(fixedInterest)/intRepayments/100;

			//length in periods
			var totPeriods = Number(intTerm.text) * intRepayments;

			//periodic interest
			var periodicInterest = Number(fixedPrincipal) * periodicInterestRate;
			var base = 1;
			var mbase = 1 + periodicInterestRate;
			
			for (var i=0; i<totPeriods; i++)
  			{
   			 base = base * mbase;
  			}
			
			//calculate Total Repayment
			totRepayment.text = "$" + NumberFormat(floor(Number(fixedPrincipal) * periodicInterestRate / ( 1 - (1/base))));
		}
		
		function commaCoder(yourNum):String 
		{
			//Adds a comma every 3 characters
    		var numtoString:String = new String();
    		var numLength:Number = yourNum.toString().length;
    		numtoString = "";

    		for (var i=0; i<numLength; i++) { 
    			if ((numLength-i)%3 == 0 && i != 0) {
    				numtoString += ",";
    			}
    			numtoString += yourNum.toString().charAt(i);
    		}
			
			tempComma++;
    		return numtoString;
		}
		
		function decimalCoder(yourNum):String 
		{
			//Adds a decimal 3 characters in
    		var numtoString:String = new String();
    		var numLength:Number = yourNum.toString().length;
    		numtoString = "";

    		for (var i=0; i<numLength; i++) { 
    			if ((numLength-i)%3 == 0 && i >= 0) {
    				numtoString += ".";
    			}
    			numtoString += yourNum.toString().charAt(i);
    		}
    		return numtoString;
		}
		
		function NumberFormat(theNumber):String
		{
    		var myArray:Array;
    		var numberPart:String;
    		var decPart:String;
    		var result:String = '';
    		var numString:String = theNumber.toString();
			
			//if there are . in the input number, split it into 2 sections
    		if(numString.indexOf('.') > 0)
    		{
        		myArray = numString.split('.');
        		numberPart = myArray[0];
        		decPart = myArray[1];
    		}
    		else
    		{
        		numberPart = numString;
    		}
			
			//if the string is greater than 3, add a comma
    		while (numberPart.length > 3)
    		{
        		var chunk:String = numberPart.substr(-3);
        		numberPart = numberPart.substr(0, numberPart.length - 3);
        		result = ',' + chunk + result;
    		}   
    		if (numberPart.length > 0)
    		{
        		result = numberPart + result;
    		}   

    		if(numString.indexOf('.') > 0)
    		{
        		result = result + '.' + decPart;
    		}
    		return result;
		}

		private function floor(number):Number 
		{
			//function rounds to two decimal places
 			return Math.floor(number*Math.pow(10,2))/Math.pow(10,2);
		}
	}
}