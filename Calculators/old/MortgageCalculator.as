﻿package 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import fl.controls.Slider; 
	import fl.events.SliderEvent; 
	
	public class MortgageCalculator extends MovieClip
	{
		private var intRepayments:int = 0;

		public function MortgageCalculator()
		{
			weekly.bTitle.text = "Weekly";
			biweekly.bTitle.text = "Biweekly";
			monthly.bTitle.text = "Monthly";
			weekly.mouseChildren = false;
			biweekly.mouseChildren = false;
			monthly.mouseChildren = false;
			
			weekly.addEventListener(MouseEvent.CLICK,setRepaymentTerm);
			biweekly.addEventListener(MouseEvent.CLICK,setRepaymentTerm);
			monthly.addEventListener(MouseEvent.CLICK,setRepaymentTerm);
			this.addEventListener(Event.ENTER_FRAME, init);
		}
		
		private function init(event:Event):void 
		{
			calculateRepayment();
			
			//sets info boxes = to the slider value and formats appropriately
			intPrincipal.text = "$" + commaCoder(principalSlider.value);
			floInterest.text = decimalCoder(interestSlider.value) + "%";
			intTerm.text = String(termSlider.value);
		}
		
		private function calculateRepayment() 
		{
			//do validation
			if (intPrincipal.text == "$0") {
				validationMessage.text = "Principal must be greater than 0.";
			}
			else if (floInterest.text == "0%") {
				validationMessage.text = "Annual interest must be between .125 and 25%.";
			}
			else if (intTerm.text <= "0") {
				validationMessage.text = "Loan term must be greater than 0.";
			}
			else if (intRepayments <= 0) {
				validationMessage.text = "Please select a repayment period.";
			}
			else {
				validationMessage.text = "";
				calculate();
			}
		}

		private function setRepaymentTerm(e:MouseEvent) 
		{
			weekly.innerBox.gotoAndStop(1);
			biweekly.innerBox.gotoAndStop(1);
			monthly.innerBox.gotoAndStop(1);
			
			e.target.innerBox.gotoAndStop(2);
			
			if(e.target.name == "weekly"){
				intRepayments = 52;
			}
			if(e.target.name == "biweekly"){
				intRepayments = 26;
			}
			if(e.target.name == "monthly"){
				intRepayments = 12;
			}
		}

		private function calculate() 
		{
			//Remove any special characters
			var tempPrincipal:String = intPrincipal.text.split(",").join("");
			var fixedPrincipal:String = tempPrincipal.split("$").join("");
			var fixedInterest:String = floInterest.text.split("%").join("");
			
			//calculate interest rate applicable to period
			var periodicInterestRate = Number(fixedInterest)/intRepayments/100;

			//length in periods
			var totPeriods = Number(intTerm.text) * intRepayments;

			//periodic interest
			var periodicInterest = Number(fixedPrincipal) * periodicInterestRate;
			var base = 1;
			var mbase = 1 + periodicInterestRate;
			
			for (var i=0; i<totPeriods; i++)
  			{
   			 base = base * mbase;
  			}
			
			//calculate Total Repayment
			totRepayment.text = "$" + NumberFormat(floor(Number(fixedPrincipal) * periodicInterestRate / ( 1 - (1/base))));
		}
		
		function commaCoder(yourNum):String 
		{
			//Adds a comma every 3 characters
    		var numtoString:String = new String();
    		var numLength:Number = yourNum.toString().length;
    		numtoString = "";

    		for (var i=0; i<numLength; i++) { 
    			if ((numLength-i)%3 == 0 && i != 0) {
    				numtoString += ",";
    			}
    			numtoString += yourNum.toString().charAt(i);
    		}
    		return numtoString;
		}
		
		function decimalCoder(yourNum):String 
		{
			//Adds a decimal 3 characters in
    		var numtoString:String = new String();
    		var numLength:Number = yourNum.toString().length;
    		numtoString = "";

    		for (var i=0; i<numLength; i++) { 
    			if ((numLength-i)%3 == 0 && i >= 0) {
    				numtoString += ".";
    			}
    			numtoString += yourNum.toString().charAt(i);
    		}
    		return numtoString;
		}
		
		function NumberFormat(theNumber):String
		{
    		var myArray:Array;
    		var numberPart:String;
    		var decPart:String;
    		var result:String = '';
    		var numString:String = theNumber.toString();

    		if(numString.indexOf('.') > 0)
    		{
        		myArray = numString.split('.');
        		numberPart = myArray[0];
        		decPart = myArray[1];
    		}
    		else
    		{
        		numberPart = numString;
    		}

    		while (numberPart.length > 3)
    		{
        		var chunk:String = numberPart.substr(-3);
        		numberPart = numberPart.substr(0, numberPart.length - 3);
        		result = ',' + chunk + result;
    		}   
    		if (numberPart.length > 0)
    		{
        		result = numberPart + result;
    		}   

    		if(numString.indexOf('.') > 0)
    		{
        		result = result + '.' + decPart;
    		}
    		return result;
		}

		private function floor(number):Number 
		{
			//function rounds to two decimal places
 			return Math.floor(number*Math.pow(10,2))/Math.pow(10,2);
		}
	}
}