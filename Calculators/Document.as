﻿package 
{
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	
	public class Document extends MovieClip
	{
		private var calculator:MortgageCalculator;

		public function Document()
		{
			calculator = new MortgageCalculator(this);
			calculator.x = 111;
			calculator.y = 98.4;
			addChild(calculator);
		}
	}
}